package graphics;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class NonProcessiveInputTest {

	public static void main(final String[] arg) {
		final JFrame frame = new JFrame("Non Processive Input Test");
		final NonProcessiveInput p = new NonProcessiveInput(null);
		frame.getContentPane().add(p);
		frame.pack();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent event) {
				System.exit(0);
			}
		});
		frame.setVisible(true);
	}
}
