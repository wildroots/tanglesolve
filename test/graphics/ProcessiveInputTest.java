package graphics;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

// TODO Check if this test is correct.
public class ProcessiveInputTest {
	public static void main(final String[] arg) {
		final JFrame frame = new JFrame("Processive Input Test");
		final ProcessiveOrderKnown processive = new ProcessiveOrderKnown(null);
		frame.getContentPane().add(processive);
		frame.pack();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent event) {
				System.exit(0);
			}
		});
		frame.setVisible(true);
	}
}
