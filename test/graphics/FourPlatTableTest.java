package graphics;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;

// TODO The Four Plat Table does not close entirely when you exit the program.
// TODO Check if this test is correct.
public class FourPlatTableTest extends JDialog {
	private static final long serialVersionUID = 1L;

	public static void main(final String[] arg) {
		final JFrame frame = new JFrame("Four Plat table");
		final FourPlatTable fourPlatTable = new FourPlatTable(frame, "Fplat table");
		fourPlatTable.setVisible(true);
		fourPlatTable.pack();
		fourPlatTable.setVisible(true);
		fourPlatTable.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent event) {
				System.exit(0);
			}
		});
	}
}
