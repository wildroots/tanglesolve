package graphics;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class ProcessiveOrderKnownTest {

	public static void main(final String[] arg) {
		final JFrame frame = new JFrame("sample");
		final ProcessiveOrderKnown processiveKnown = new ProcessiveOrderKnown(null);
		frame.getContentPane().add(processiveKnown);
		frame.pack();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent event) {
				System.exit(0);
			}
		});
		frame.setVisible(true);
	}
}
