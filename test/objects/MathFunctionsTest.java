package objects;

import static org.junit.Assert.*;

import javax.vecmath.Point2d;

import junit.framework.Assert;

import org.junit.Test;

public class MathFunctionsTest {

	private static final double DELTA = 0.00001;

	@Test
	public void testEvenPositive() {
		final long a = 2;
		assertEquals(true, MathFunctions.even(a));
	}

	@Test
	public void testEvenNegative() {
		final long a = -2;
		assertEquals(true, MathFunctions.even(a));
	}

	@Test
	public void testOddPositive() {
		final long b = 3;
		assertEquals(true, MathFunctions.odd(b));
	}

	@Test
	public void testOddNegative() {
		final long b = -3;
		assertEquals(true, MathFunctions.odd(b));
	}

	@Test
	public void testMod() {
		final long c = 10;
		final long d = 4;
		assertEquals(2, MathFunctions.mod(c, d));
		assertEquals(4, MathFunctions.mod(d, c));
	}

	@Test
	public void testModuloInverse1() {
		final long c = 7;
		final long d = 3;
		final long e = 4;
		final long f = 5;
		assertEquals(1, MathFunctions.moduloInverse1(c, d));
		assertEquals(4, MathFunctions.moduloInverse1(e, f));
		// assertEquals(0, MathFunctions.moduloInverse1(g, h)); // TODO
	}

	// TODO Check cases for inverse modulo 
	@Test(expected = IllegalArgumentException.class)
	public void testModuleInverseIllegal() {
		MathFunctions.moduloInverse1(5, 0);
	}

	@Test
	public void testSign() {
		final float a = 5;
		final float b = -5;
		assertEquals(1, MathFunctions.sign(a));
		assertEquals(-1, MathFunctions.sign(b));
	}

	@Test
	public void testCalculateEllipse() {
		final Point2d actual = MathFunctions.calculateEllipse(new Point2d(-1, 3), new Point2d(1, 3), new Point2d(-1, 4));

		Assert.assertEquals(2, actual.x, DELTA);
		Assert.assertEquals(1, actual.y, DELTA);
	}

	@Test
	public void testCalculateEllipseWithVerticalPoints() {
		final Point2d actual = MathFunctions.calculateEllipse(new Point2d(0, 0), new Point2d(0, 1), new Point2d(0, -1));

		Assert.assertEquals(1, actual.x, DELTA);
		Assert.assertEquals(1, actual.y, DELTA);
	}
}
