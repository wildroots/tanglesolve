package objects;
import static org.junit.Assert.*;

import org.junit.Test;


public class KauffmanUnknotTheoremTest {

	@Test
	public void testIsUnknotTangle0Tangle0() throws Exception { 
		Rational tangle1 = new Rational(0, 1);
		Rational tangle2 = new Rational(0, 1);
		assertEquals(false, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}

	@Test
	public void testIsUnknotTangle1Tangle0() throws Exception {
		Rational tangle1 = new Rational(1, 1);
		Rational tangle2 = new Rational(0, 1);
		assertEquals(true, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
	@Test
	public void testIsUnknotTangleNegative1Tangle0() throws Exception {
		Rational tangle1 = new Rational(-1, 1);
		Rational tangle2 = new Rational(0, 1);
		assertEquals(true, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
	@Test
	public void testIsUnknotTangle0Tangle1() throws Exception {
		Rational tangle1 = new Rational(0, 1);
		Rational tangle2 = new Rational(1, 1);
		assertEquals(true, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
	@Test
	public void testIsUnknotTangle0TangleNegative1() throws Exception {
		Rational tangle1 = new Rational(0, 1);
		Rational tangle2 = new Rational(-1, 1);
		assertEquals(true, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
	@Test
	public void testIsUnknotVerticalTangle5Tangle0() throws Exception {
		Rational tangle1 = new Rational(1, 5);
		Rational tangle2 = new Rational(0, 1);
		assertEquals(true, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
	@Test
	public void testIsUnknotTangle0VerticalTangle5() throws Exception {
		Rational tangle1 = new Rational(0, 1);
		Rational tangle2 = new Rational(1, 5);
		assertEquals(true, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
	@Test
	public void testIsUnknotVerticalTangleNegative5Tangle0() throws Exception {
		Rational tangle1 = new Rational(-1, 5);
		Rational tangle2 = new Rational(0, 1);
		assertEquals(true, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
	@Test
	public void testIsUnknotVerticalTangle5Tangle0Special() throws Exception {
		Rational tangle1 = new Rational(1, 5);
		Rational tangle2 = new Rational(0, 5);
		assertEquals(true, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
	@Test
	public void testIsUnknotTangle1TangleNegative4_3() throws Exception {
		Rational tangle1 = new Rational(1, 1);
		Rational tangle2 = new Rational(-4, 3);
		assertEquals(true, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
	@Test
	public void testIsUnknotTangle5Tangle0() throws Exception {
		Rational tangle1 = new Rational(5, 1);
		Rational tangle2 = new Rational(0, 1);
		assertEquals(false, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
	@Test
	public void testIsUnknotTangle5VerticalTangle5() throws Exception {
		Rational tangle1 = new Rational(5, 1);
		Rational tangle2 = new Rational(1, 5);
		assertEquals(false, KauffmanUnknotTheorem.unknotTest(tangle1, tangle2));
	}
	
}
