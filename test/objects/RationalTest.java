package objects;

import static org.junit.Assert.*;

import org.junit.Test;

public class RationalTest {
	
	@Test
	public void testInteger() {
		final Rational a = new Rational("-15/3");
		assertEquals(true, a.isInteger());
	}
	
	@Test
	public void testRationalString() {
		assertFalse(Rational.isRational("//"));
	}
	
	@Test
	public void testReduce() {
		final Rational a = new Rational("15/3");
		assertEquals(5, a.getNumerator());
		assertEquals(1, a.getDenominator());
	}

	@Test
	public void testReduceWithNegativeNumerator() {
		final Rational a = new Rational("-15/3");
		assertEquals(-5, a.getNumerator());
		assertEquals(1, a.getDenominator());
	}

	@Test
	public void testReduceWithZeroDenominator() {
		final Rational a = new Rational("1/0");
		assertEquals(1, a.getNumerator());
		assertEquals(0, a.getDenominator());
	}
	
	@Test
	public void testContinuedFraction() {
		final Rational a = new Rational("675/1000");
		assertEquals("(0,1,2,13)", a.continuedFraction().toString());
	}
	
	@Test
	public void testReciprocal() {
		final Rational a = new Rational("-15/3");
		final Rational b = new Rational("-3/15");
		assertEquals(b, a.getReciprocal());
	}
	
	@Test
	public void testGetNumerator() {
		final Rational a = new Rational("15/3");
		final int numerator = 5;
		assertEquals(numerator, a.getNumerator());
	}
	
	@Test
	public void testGetDenominator() {
		final Rational a = new Rational("15/3");
		final int denominator = 1;
		assertEquals(denominator, a.getDenominator());
	}
	
	@Test
	public void testGetNegative() {
		final Rational a = new Rational("-15/3");
		final Rational b = new Rational("15/3");
		assertEquals(b, a.getNegative());
	}
	
	@Test
	public void testNegativeInfinity() {
		final Rational a = new Rational("0/0");
		assertEquals(a, a.getNegative());
	}
	
	@Test
	public void testIsOne() {
		Rational a = new Rational("15/4");
		assertEquals(false, a.isOne());
	}
	
	@Test
	public void testIsOneInteger() {
		Rational a = new Rational("1");
		assertEquals(true, a.isOne());
	}
	
	@Test
	public void testIsZero() {
		Rational a = new Rational("0");
		assertEquals(true, a.isZero());
	}
	
	@Test
	public void testIsZeroInfinity() {
		Rational a = new Rational("0/0");
		assertEquals(false, a.isZero());
	}
	
	@Test
	public void testIsZeroRational() {
		Rational a = new Rational("0/3");
		assertEquals(true, a.isZero());
	}
	
	@Test
	public void testIsNegativeOneRational() {
		Rational a = new Rational("-1/7");
		assertEquals(false, a.isNegativeOne());
	}
	
	@Test
	public void testIsNegativeOneInteger() {
		Rational a = new Rational("-1");
		assertEquals(true, a.isNegativeOne());
	}
	
	@Test
	public void testIsNegativeOneInfinity() {
		Rational a = new Rational("-1/0");
		assertEquals(false, a.isNegativeOne());
	}
	
	// TODO Check if this test is correct.
	@Test 
	public void testInfinity() {
		final Rational a = new Rational("-15/3");
		final Rational l = new Rational("0/0");
		final Rational r = new Rational("0");
		// By definition set in code
		assertEquals(1, l.getNumerator());
		assertEquals(0, l.getDenominator());
		assertEquals(r, l.getReciprocal());
		assertEquals(l, l.add(a));
		assertEquals(l, a.add(l));
	}
	
	@Test
	public void testAddRationals() {
		final Rational a = new Rational("-15/3");
		final Rational b = new Rational("3/5");
		final Rational c = new Rational("-22/5");
		assertEquals(c, a.add(b));
	}
	
	@Test
	public void testAddRationalLong() {
		final Rational a = new Rational("-15/3");
		final long b = 234;
		final Rational c = new Rational("229");
		assertEquals(c, a.add(b));
	}

	@Test 
	public void testSubtract() {
		final Rational a = new Rational("-15/3");
		final Rational b = new Rational("3/5");
		final Rational c = new Rational("-28/5");
		assertEquals(c, a.subtract(b));
	}
	
	@Test
	public void testDivide() {
		final Rational a = new Rational("-15/3");
		final Rational b = new Rational("3/5");
		final Rational c = new Rational("-25/3");
		assertEquals(c, a.divide(b));		
	}
	
	@Test 
	public void testMultiply() {
		final Rational a = new Rational("-15/3");
		final Rational b = new Rational("3/5");
		final Rational c = new Rational("-3");
		assertEquals(c, a.multiply(b));
	}
	
	// TODO
/*	public void testInvMod() {
		final Rational a = new Rational("-15/3");
		System.out.println(a.invMod());
	}*/
	@Test
	// TODO
	public void test() {
		// Big number
		//final Rational k = new Rational("12345678912/34523457771");
	}

}
