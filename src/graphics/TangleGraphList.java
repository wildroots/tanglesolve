package graphics;

import graphics.Tangle3DTetrahedronDialog.Options;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import objects.FourPlat;
import objects.Rational;
import objects.Tangle;
import objects.Tangle3D;
import objects.Tangle3DUtils;

public class TangleGraphList extends JList {
	private static final Vector3f NUMERATOR_BOTTOM_OFFSET = new Vector3f(0, 0, 2f);
	private static final Vector3f NUMERATOR_TOP_OFFSET = new Vector3f(0, 0, -2f);

	private static final int SCREENSHOT_OUTPUT_SCALE = 5;
	private static final int POSTER_OUTPUT_SCALE = 40;

	public static enum Quality {
		SCREENSHOT(SCREENSHOT_OUTPUT_SCALE),
		POSTER(POSTER_OUTPUT_SCALE);

		private int scale;

		private Quality(final int scale) {
			this.scale = scale;
		}

		public int getScale() {
			return this.scale;
		}
	}

	// constants
	private static final long serialVersionUID = -520704524629284362L;
	private static final int HEIGHT_ADJUSTMENT = 20; // adds height to icon height for solution graph output
	private static final String KNOT_VERTICES_FILE_NAME = "knot.txt";
	private static final String LINK_COMPONENT_ONE_FILE_NAME = "link_component_1.txt";
	private static final String LINK_COMPONENT_TWO_FILE_NAME = "link_component_2.txt";
	private static final String LINK_COMPONENT_THREE_FILE_NAME = "link_component_3.txt";

	private static final float EXPORT_KNOTPLOT_SHIFT_SCALE = 7.5f;
	private static final float EXPORT_KNOTPLOT_SCALE = 2;
	private static final float EXPORT_KNOTPLOT_SITE_SCALE = 0.5f;
	private static final int EXPORT_KNOTPLOT_CROSSING_RESOLUTION = 2;
	private static final float EXPORT_KNOTPLOT_CROSSING_SIZE = 20;
	private static final float EXPORT_KNOTPLOT_CROSSING_HELIX_RADIUS = 0.4f;

	private JFrame frame;

	// constructor
	public TangleGraphList(final JFrame frame) {
		this.frame = frame;
		this.addMouseListener(mouseAdapter);
	}

	private MouseListener mouseAdapter = new MouseAdapter() {
		public void mousePressed(MouseEvent e) {
			check(e);
		}

		public void mouseReleased(MouseEvent e) {
			check(e);
		}

		public void check(MouseEvent e) {
			if (e.isPopupTrigger()) { // if the event shows the menu
				showPopup(e);
			}
		}
	};

	private void showPopup(MouseEvent event) {
		final int selectedIndex = this.locationToIndex(event.getPoint());
		this.setSelectedIndex(selectedIndex); // select the item

		JPopupMenu popup = new JPopupMenu();

		// Show in 3D menu items
		JMenu screenshotMenu = new JMenu("Save Image As...");

		// Save Image in SCREENSHOT QUALITY
		ActionListener saveScreenshotQualityListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				final JFileChooser imageFileChooser = new JFileChooser();
				imageFileChooser.setMultiSelectionEnabled(false);
				imageFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

				final int result = imageFileChooser.showSaveDialog(null);

				if (result == JFileChooser.APPROVE_OPTION) {
					export(selectedIndex, imageFileChooser.getSelectedFile(), Quality.SCREENSHOT);
				}
			}
		};
		JMenuItem saveScreenshotQualityMenuItem = new JMenuItem("Screenshot Quality...", null);
		saveScreenshotQualityMenuItem.addActionListener(saveScreenshotQualityListener);
		screenshotMenu.add(saveScreenshotQualityMenuItem);

		// Save Image in POSTER QUALITY
		ActionListener savePosterQualityListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				final JFileChooser imageFileChooser = new JFileChooser();
				imageFileChooser.setMultiSelectionEnabled(false);

				final int result = imageFileChooser.showSaveDialog(null);

				if (result == JFileChooser.APPROVE_OPTION) {
					export(selectedIndex, imageFileChooser.getSelectedFile(), Quality.POSTER);
				}
			}
		};
		JMenuItem savePosterQualityMenuItem = new JMenuItem("Poster Quality...", null);
		savePosterQualityMenuItem.addActionListener(savePosterQualityListener);
		screenshotMenu.add(savePosterQualityMenuItem);

		popup.add(screenshotMenu);

		// Show in 3D menu items
		JMenu showIn3DMenu = new JMenu("Show In 3D...");

		// Of Tangle
		ActionListener showOfTangleIn3DListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				final SolutionGraph selectedSolutionGraph = getSolutionGraph(selectedIndex);
				showIn3D(selectedSolutionGraph.getEquationGraph(), Options.TANGLE_OF);
			}
		};
		JMenuItem showOfTangleIn3DMenuItem = new JMenuItem("Show 'Of' Tangle In 3D...", null);
		showOfTangleIn3DMenuItem.addActionListener(showOfTangleIn3DListener);
		showIn3DMenu.add(showOfTangleIn3DMenuItem);

		// Ob Tangle
		ActionListener showObTangleIn3DListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				final SolutionGraph selectedSolutionGraph = getSolutionGraph(selectedIndex);
				showIn3D(selectedSolutionGraph.getEquationGraph(), Options.TANGLE_OB);
			}
		};
		JMenuItem showObTangleIn3DMenuItem = new JMenuItem("Show 'Ob' Tangle In 3D...", null);
		showObTangleIn3DMenuItem.addActionListener(showObTangleIn3DListener);
		showIn3DMenu.add(showObTangleIn3DMenuItem);

		// k Tangle
		ActionListener showKTangleIn3DListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				final SolutionGraph selectedSolutionGraph = getSolutionGraph(selectedIndex);
				showIn3D(selectedSolutionGraph.getEquationGraph(), Options.TANGLE_R);
			}
		};
		JMenuItem showKTangleIn3DMenuItem = new JMenuItem("Show 'R' Tangle In 3D...", null);
		showKTangleIn3DMenuItem.addActionListener(showKTangleIn3DListener);
		showIn3DMenu.add(showKTangleIn3DMenuItem);

		// Of and Ob Tangles
		ActionListener showOfObTangleIn3DListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				final SolutionGraph selectedSolutionGraph = getSolutionGraph(selectedIndex);
				showIn3D(selectedSolutionGraph.getEquationGraph(), Options.TANGLES_OF_OB);
			}
		};
		JMenuItem showOfObTangleIn3DMenuItem = new JMenuItem("Show 'Of' and 'Ob' Tangle In 3D...", null);
		showOfObTangleIn3DMenuItem.addActionListener(showOfObTangleIn3DListener);
		showIn3DMenu.add(showOfObTangleIn3DMenuItem);

		// Ob and R Tangles
		ActionListener showObRTangleIn3DListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				final SolutionGraph selectedSolutionGraph = getSolutionGraph(selectedIndex);
				showIn3D(selectedSolutionGraph.getEquationGraph(), Options.TANGLES_OB_R);
			}
		};
		JMenuItem showObRTangleIn3DMenuItem = new JMenuItem("Show 'Ob', and 'R' Tangle In 3D...", null);
		showObRTangleIn3DMenuItem.addActionListener(showObRTangleIn3DListener);
		showIn3DMenu.add(showObRTangleIn3DMenuItem);

		// Of, Ob and R Tangles
		ActionListener showOfObRTangleIn3DListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				final SolutionGraph selectedSolutionGraph = getSolutionGraph(selectedIndex);
				showIn3D(selectedSolutionGraph.getEquationGraph(), Options.TANGLES_OF_OB_R);
			}
		};
		JMenuItem showOfObRTangleIn3DMenuItem = new JMenuItem("Show 'Of', 'Ob', and 'R' Tangle In 3D...", null);
		showOfObRTangleIn3DMenuItem.addActionListener(showOfObRTangleIn3DListener);
		showIn3DMenu.add(showOfObRTangleIn3DMenuItem);

		popup.add(showIn3DMenu);
		

		// Show in 3D menu items
		JMenu exportMenu = new JMenu("Export...");

		// Of Tangle
		ActionListener exportToKnotPlotListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				final JFileChooser exportDirectoryChooser = new JFileChooser();
				exportDirectoryChooser.setMultiSelectionEnabled(false);
				exportDirectoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

				final int result = exportDirectoryChooser.showSaveDialog(null);

				if (result == JFileChooser.APPROVE_OPTION) {
					final SolutionGraph selectedSolutionGraph = getSolutionGraph(selectedIndex);
					try {
						final File exportDirectory = exportDirectoryChooser.getSelectedFile();
						if (!exportDirectory.exists()) {
							exportDirectory.mkdirs();
						}

						exportToKnotPlot(selectedSolutionGraph.getEquationGraph(), Options.TANGLES_OF_OB_R, exportDirectory);
					} catch (final FileNotFoundException pFileNotFoundException) {
						// Oh noz...
						pFileNotFoundException.printStackTrace();
					}
				}
			}
		};
		JMenuItem exportToKnotPlotMenuItem = new JMenuItem("Export vertices for KnotPlot...", null);
		exportToKnotPlotMenuItem.addActionListener(exportToKnotPlotListener);
		exportMenu.add(exportToKnotPlotMenuItem);

		popup.add(exportMenu);

		// and show the menu
		popup.show(this, event.getX(), event.getY());
	}

	protected void showIn3D(final EquationGraph equationGraph, final Options options) {
		final Tangle3DTetrahedronDialog tangle3dTetrahedronDialog = new Tangle3DTetrahedronDialog(frame, equationGraph, options);

		tangle3dTetrahedronDialog.setVisible(true);
	}

	private void export(final int selectedIndex, final File selectedFile, final Quality quality) {
		final SolutionGraph selectedSolutionGraph = getSolutionGraph(selectedIndex);
		Tangle of = selectedSolutionGraph.getOfTangle();
		Tangle ob = selectedSolutionGraph.getObTangle();
		Tangle r = selectedSolutionGraph.getRTangle();
		FourPlat fp = selectedSolutionGraph.getFourPlat();
		int rnd = selectedSolutionGraph.getRound();
		final SolutionGraph biggerSolutionGraph = new SolutionGraph(of, ob, r, fp, rnd, quality.getScale(), quality.getScale());
		final EquationGraph biggerEquationGraph = biggerSolutionGraph.getEquationGraph();

		final int width = biggerEquationGraph.getIconWidth();
		final int height = biggerEquationGraph.getIconHeight() + HEIGHT_ADJUSTMENT;

		try {
			final BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			final Graphics2D graphics2D = (Graphics2D) img.getGraphics();

			graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			biggerEquationGraph.paintIcon(null, graphics2D, 0, 0);

			graphics2D.dispose();

			try {
				ImageIO.write(img, "PNG", selectedFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (OutOfMemoryError e) {
			JOptionPane.showMessageDialog(null, "Sorry, the exported image is too big!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	private SolutionGraph getSolutionGraph(final int selectedIndex) {
		final SolutionGraph selectedSolutionGraph = (SolutionGraph) this.getModel().getElementAt(selectedIndex);
		return selectedSolutionGraph;
	}

	private void exportToKnotPlot(final EquationGraph equationGraph, final Options options, final File directory) throws FileNotFoundException {
		if (options != Options.TANGLES_OF_OB_R) {
			throw new IllegalArgumentException("Only: '" + Options.TANGLES_OF_OB_R + "' is currently supported.");
		}

		final Tangle3D ofTangle3D = new Tangle3D(equationGraph.getOfTangleGraph().tangle, EXPORT_KNOTPLOT_CROSSING_RESOLUTION, EXPORT_KNOTPLOT_CROSSING_SIZE, EXPORT_KNOTPLOT_CROSSING_HELIX_RADIUS, EXPORT_KNOTPLOT_SCALE);
		final Tangle3D obTangle3D = new Tangle3D(equationGraph.getObTangleGraph().tangle, EXPORT_KNOTPLOT_CROSSING_RESOLUTION, EXPORT_KNOTPLOT_CROSSING_SIZE, EXPORT_KNOTPLOT_CROSSING_HELIX_RADIUS, EXPORT_KNOTPLOT_SCALE);
		final Tangle3D rTangle3D = new Tangle3D(equationGraph.getRTangleGraph()[0].tangle, EXPORT_KNOTPLOT_CROSSING_RESOLUTION, EXPORT_KNOTPLOT_CROSSING_SIZE, EXPORT_KNOTPLOT_CROSSING_HELIX_RADIUS, EXPORT_KNOTPLOT_SITE_SCALE);

		final Rational parity1 = equationGraph.getOfTangleGraph().tangle.parity();
		final Rational parity2 = equationGraph.getObTangleGraph().tangle.parity();
		final Rational parity3 = equationGraph.getRTangleGraph()[0].tangle.parity();

		// adjust shift for KnotPlot export
		Vector3f shiftTangleOf = new Vector3f(options.getShiftTangleOf());
		Vector3f shiftTangleOb = new Vector3f(options.getShiftTangleOb());
		Vector3f shiftTangleR = new Vector3f(options.getShiftTangleR());
		shiftTangleOf.scale(EXPORT_KNOTPLOT_SHIFT_SCALE);
		shiftTangleOb.scale(EXPORT_KNOTPLOT_SHIFT_SCALE);
		shiftTangleR.scale(EXPORT_KNOTPLOT_SHIFT_SCALE);
		
		// top numerator from tangle Of to R
		final Point3f tangleOfCornerA = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_A);
		tangleOfCornerA.scale(EXPORT_KNOTPLOT_SCALE);
		tangleOfCornerA.add(shiftTangleOf);
		
		final Point3f tangleRCornerD = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_D);
		tangleRCornerD.scale(EXPORT_KNOTPLOT_SITE_SCALE);
		tangleRCornerD.add(shiftTangleR);
		
		final ArrayList<Point3f> numeratorTopVertices = this.getNumeratorTop(tangleOfCornerA, tangleRCornerD);

		// bottom numerator from tangle Of to R
		final Point3f tangleOfCornerB = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_B);
		tangleOfCornerB.scale(EXPORT_KNOTPLOT_SCALE);
		tangleOfCornerB.add(shiftTangleOf);
		
		final Point3f tangleRCornerC = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_C);
		tangleRCornerC.scale(EXPORT_KNOTPLOT_SITE_SCALE);
		tangleRCornerC.add(shiftTangleR);

		final ArrayList<Point3f> numeratorBottomVertices = this.getNumeratorBottom(tangleOfCornerB, tangleRCornerC);

		if (parity1.isZero()) {
			if (parity2.isZero()) {
				if (parity3.isZero()) {
					// N(0 + 0 + 0) produces a link
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); // The one that starts in the top left in the tangle diagram!
					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					linkComponentOne.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));
					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));

					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>(); // The one that starts in the bottom left in the tangle diagram!
					linkComponentTwo.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					linkComponentTwo.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					linkComponentTwo.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					linkComponentTwo.addAll(reverse(numeratorBottomVertices));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));
				} else if (parity3.isOne()) {
					// N(0 + 0 + 1) produces a knot	
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					knot.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					knot.addAll(reverse(numeratorBottomVertices));
					knot.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					knot.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isInfinity()) {
					// N(0 + 0 + infinity) produces a knot	
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					knot.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					knot.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));
					knot.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isOne()) {
				if (parity3.isZero()) {
					// N(0 + 1 + 0) produces a knot	
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorBottomVertices));
					knot.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					knot.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					knot.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isOne()) {
					// N(0 + 1 + 1) produces a link
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); // The one that starts in the top left in the tangle diagram!
					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>(); // The one that starts in the bottom left in the tangle diagram!

					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					linkComponentOne.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));

					linkComponentTwo.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					linkComponentTwo.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					linkComponentTwo.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					linkComponentTwo.addAll(reverse(numeratorBottomVertices));
					
					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));
				} else if (parity3.isInfinity()) {
					// N(0 + 1 + infinity) produces a knot	
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					knot.addAll(shift(reverse(rTangle3D.getFirstStrandVertices()), shiftTangleR));
					knot.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));
					knot.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));

					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isInfinity()) {
				if (parity3.isZero()) {
					// N(0 + infinity + 0) produces a knot	
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					knot.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(rTangle3D.getSecondStrandVertices(), shiftTangleR));
					knot.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					knot.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
					
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isOne()) {
					// N(0 + infinity + 1) produces a knot
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					knot.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(reverse(rTangle3D.getFirstStrandVertices()), shiftTangleR));
					knot.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
					
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isInfinity()) {
					// N(0 + infinity + infinity) produces a link
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); 
					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>();
					
					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					linkComponentOne.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(numeratorBottomVertices);
					linkComponentOne.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));
					
					linkComponentTwo.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));
					linkComponentTwo.addAll(shift(reverse(rTangle3D.getFirstStrandVertices()), shiftTangleR));

					
					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		} else if (parity1.isOne()) {
			if (parity2.isZero()) {
				if (parity3.isZero()) {
					// N(1 + 0 + 0) produces a knot.
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorBottomVertices));
					knot.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					knot.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					knot.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
										
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isOne()) {
					// N(1 + 0 + 1) produces a link
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); 
					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>();
					
					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					linkComponentOne.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));
					
					linkComponentTwo.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					linkComponentTwo.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					linkComponentTwo.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					linkComponentTwo.addAll(reverse(numeratorBottomVertices));

					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));
				} else if (parity3.isInfinity()) {
					// N(1 + 0 + infinity) produces a knot	
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					knot.addAll(shift(reverse(rTangle3D.getFirstStrandVertices()), shiftTangleR));
					knot.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));
					knot.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
															
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isOne()) {
				if (parity3.isZero()) {
					// N(1 + 1 + 0) produces a link
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); 
					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>();
					
					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					linkComponentOne.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));
					
					linkComponentTwo.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					linkComponentTwo.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					linkComponentTwo.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					linkComponentTwo.addAll(reverse(numeratorBottomVertices));

					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));

				} else if (parity3.isOne()) {
					// N(1 + 1 + 1) produces a knot
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					knot.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					knot.addAll(reverse(numeratorBottomVertices));
					knot.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					knot.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
															
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isInfinity()) {
					// N(1 + 1 + infinity) produces a knot
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					knot.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					knot.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));
					knot.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
															
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isInfinity()) {
				if (parity3.isZero()) {
					// N(1 + infinity + 0) produces a knot
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));
					knot.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(rTangle3D.getSecondStrandVertices(), shiftTangleR));
					knot.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					knot.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
															
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isOne()) {
					// N(1 + infinity + 1) produces a knot
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));
					knot.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(reverse(rTangle3D.getFirstStrandVertices()), shiftTangleR));
					knot.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));					
					knot.addAll(reverse(numeratorTopVertices));
															
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isInfinity()) {
					// N(1 + infinity + infinity) produces a link
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); 
					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>();
					
					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));
					linkComponentOne.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(numeratorBottomVertices);
					linkComponentOne.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));

					linkComponentTwo.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));
					linkComponentTwo.addAll(shift(reverse(rTangle3D.getFirstStrandVertices()), shiftTangleR));
					
					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		} else if (parity1.isInfinity()) {
			if (parity2.isZero()) {
				if (parity3.isZero()) {
					// N(infinity + 0 + 0) produces a knot
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(rTangle3D.getSecondStrandVertices(), shiftTangleR));
					knot.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));
					knot.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					knot.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					knot.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));
					
					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isOne()) {
					// N(infinity + 0 + 1) produces a knot
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(reverse(rTangle3D.getFirstStrandVertices()), shiftTangleR));
					knot.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));
					knot.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					knot.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));

					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isInfinity()) {
					// N(infinity + 0 + infinity) produces a link
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); 
					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>();
					
					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(numeratorBottomVertices);
					linkComponentOne.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));
					
					linkComponentTwo.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					linkComponentTwo.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					linkComponentTwo.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					linkComponentTwo.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));

					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isOne()) {
				if (parity3.isZero()) {
					// N(infinity + 1 + 0) produces a knot
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(rTangle3D.getSecondStrandVertices(), shiftTangleR));
					knot.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));
					knot.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					knot.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					knot.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));

					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isOne()) {
					// N(infinity + 1 + 1) produces a knot
					final List<Point3f> knot = new ArrayList<Point3f>();
					knot.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					knot.addAll(numeratorBottomVertices);
					knot.addAll(shift(reverse(rTangle3D.getFirstStrandVertices()), shiftTangleR));
					knot.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));
					knot.addAll(shift(reverse(ofTangle3D.getSecondStrandVertices()), shiftTangleOf));
					knot.addAll(shift(obTangle3D.getFirstStrandVertices(), shiftTangleOb));
					knot.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					knot.addAll(reverse(numeratorTopVertices));

					exportKnotPlotVerticesToFile(knot, new File(directory, KNOT_VERTICES_FILE_NAME));
				} else if (parity3.isInfinity()) {
					// N(infinity + 1 + infinity) produces a link
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); 
					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>();
					
					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(numeratorBottomVertices);
					linkComponentOne.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));
					
					linkComponentTwo.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					linkComponentTwo.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					linkComponentTwo.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					linkComponentTwo.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));

					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isInfinity()) {
				if (parity3.isZero()) {
					// N(infinity + infinity + 0) produces a link
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); 
					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>();
					
					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(numeratorBottomVertices);
					linkComponentOne.addAll(shift(rTangle3D.getSecondStrandVertices(), shiftTangleR));
					linkComponentOne.addAll(shift(reverse(obTangle3D.getSecondStrandVertices()), shiftTangleOb));
					linkComponentOne.addAll(shift(rTangle3D.getFirstStrandVertices(), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));
					
					
					linkComponentTwo.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					linkComponentTwo.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));

					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));
				} else if (parity3.isOne()) {
					// N(infinity + infinity + 1) produces a link
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); 
					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>();
					
					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(numeratorBottomVertices);
					linkComponentOne.addAll(shift(reverse(rTangle3D.getFirstStrandVertices()), shiftTangleR));
					linkComponentOne.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));
					linkComponentOne.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));
					
					linkComponentTwo.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					linkComponentTwo.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));

					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));
				} else if (parity3.isInfinity()) {
					// N(infinity + infinity + infinity)
					final List<Point3f> linkComponentOne = new ArrayList<Point3f>(); 
					final List<Point3f> linkComponentTwo = new ArrayList<Point3f>();
					final List<Point3f> linkComponentThree = new ArrayList<Point3f>();
					
					linkComponentOne.addAll(shift(ofTangle3D.getFirstStrandVertices(), shiftTangleOf));
					linkComponentOne.addAll(numeratorBottomVertices);
					linkComponentOne.addAll(shift(reverse(rTangle3D.getSecondStrandVertices()), shiftTangleR));
					linkComponentOne.addAll(reverse(numeratorTopVertices));
					
					linkComponentTwo.addAll(shift(ofTangle3D.getSecondStrandVertices(), shiftTangleOf));
					linkComponentTwo.addAll(shift(reverse(obTangle3D.getFirstStrandVertices()), shiftTangleOb));
					
					linkComponentThree.addAll(shift(obTangle3D.getSecondStrandVertices(), shiftTangleOb));
					linkComponentThree.addAll(shift(reverse(rTangle3D.getFirstStrandVertices()), shiftTangleR));
					
					exportKnotPlotVerticesToFile(linkComponentOne, new File(directory, LINK_COMPONENT_ONE_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentTwo, new File(directory, LINK_COMPONENT_TWO_FILE_NAME));
					exportKnotPlotVerticesToFile(linkComponentThree, new File(directory, LINK_COMPONENT_THREE_FILE_NAME));
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		} else {
			throw new IllegalArgumentException();
		}

//		for (int i = 1; i < this.round; i++) {
//			if (parity3.isZero()) {
//				this.rTangleGraph[i].colorSources[0] = COLORSOURCE_1;
//				this.rTangleGraph[i].colorSources[1] = COLORSOURCE_2;
//			}
//			this.rTangleGraph[i].colorSources[0] = this.rTangleGraph[i - 1].colorSources[1];
//			this.rTangleGraph[i].colorSources[1] = this.rTangleGraph[i - 1].colorSources[0];
//		}
	}

	private ArrayList<Point3f> getNumeratorTop(final Point3f vertexOne, final Point3f vertexTwo) {
		return this.getNumeratorVertices(vertexOne, vertexTwo, NUMERATOR_TOP_OFFSET);
	}

	private ArrayList<Point3f> getNumeratorBottom(final Point3f vertexOne, final Point3f vertexTwo) {
		return this.getNumeratorVertices(vertexOne, vertexTwo, NUMERATOR_BOTTOM_OFFSET);
	}

	private ArrayList<Point3f> getNumeratorVertices(final Point3f vertexOne, final Point3f vertexTwo, Vector3f offset) {
		final Point3f oneThird = new Point3f();
		oneThird.x = (vertexOne.x * 2f / 3f) + (vertexTwo.x * 1f / 3f); 
		oneThird.y = (vertexOne.y * 2f / 3f) + (vertexTwo.y * 1f / 3f); 
		oneThird.z = (vertexOne.z * 2f / 3f) + (vertexTwo.z * 1f / 3f);

		final Point3f twoThirds = new Point3f();
		twoThirds.x = (vertexOne.x * 1f / 3f) + (vertexTwo.x * 2f / 3f); 
		twoThirds.y = (vertexOne.y * 1f / 3f) + (vertexTwo.y * 2f / 3f); 
		twoThirds.z = (vertexOne.z * 1f / 3f) + (vertexTwo.z * 2f / 3f);

		oneThird.add(offset);
		twoThirds.add(offset);

		final ArrayList<Point3f> numeratorVertices = new ArrayList<Point3f>();
		numeratorVertices.add(oneThird);
		numeratorVertices.add(twoThirds);

		return numeratorVertices;
	}

	private static List<Point3f> shift(final List<Point3f> vertices, final Vector3f shift) {
		final ArrayList<Point3f> shiftedVertices = new ArrayList<Point3f>(vertices.size());

		for (Point3f vertex : vertices) {
			final Point3f shifted = new Point3f(vertex);
			shifted.add(shift);
			shiftedVertices.add(shifted);
		}

		return shiftedVertices;
	}

	private static List<Point3f> reverse(final List<Point3f> vertices) {
		final ArrayList<Point3f> reversedVertices = new ArrayList<Point3f>(vertices.size());

		for (int i = vertices.size() - 1; i >= 0; i--) {
			reversedVertices.add(vertices.get(i));
		}

		return reversedVertices;
	}
	
	private static List<Point3f> filterSameAdjacentVertices(final List<Point3f> vertices, float epsilon) {
		final ArrayList<Point3f> filteredVertices = new ArrayList<Point3f>(vertices.size());

		for (int i = 0; i < vertices.size() - 1; i++) {
			final Point3f current = vertices.get(i);
			final Point3f next = vertices.get(i + 1);
			if (!isSame(current, next, epsilon)) {
				filteredVertices.add(current);
			}
		}
		final Point3f first = vertices.get(0);
		final Point3f last = vertices.get(vertices.size() - 1);
		
		if (!isSame(last, first, epsilon)) {
			filteredVertices.add(last);
		}

		return filteredVertices;
	}

	private static boolean isSame(final Point3f pointA, final Point3f pointB, float epsilon) {
		return Math.abs(pointA.x - pointB.x) < epsilon && Math.abs(pointA.y - pointB.y) < epsilon && Math.abs(pointA.z - pointB.z) < epsilon;
	}

	private void exportKnotPlotVerticesToFile(final List<Point3f> vertices, final File file) throws FileNotFoundException {
		final List<Point3f> filteredVertices = filterSameAdjacentVertices(vertices, 0.00001f);
		final PrintWriter out = new PrintWriter(new FileOutputStream(file));

		for (Point3f vertex : filteredVertices) {
			out.println(vertex.toString());
		}

		out.close();
	}
	
}
