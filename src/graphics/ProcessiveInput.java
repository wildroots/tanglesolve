/**
 * @author Unknown
 * Last Updated : April 13, 2013
 * @edited by Crista Moreno
 */
package graphics;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;

import objects.FourPlat;
import objects.InputData;
import objects.Parser;
import objects.Rational;
import objects.VectorList;

public class ProcessiveInput extends JPanel {

	// constants
	private static final long serialVersionUID = 1L;
	private static final int MAX_EQ = TreeDisplayString.ROUNDS;
	
	// private fields
	// Labels
	private JLabel substratesLabel;
	private JLabel productsLabel;
	private JLabel roundsLabel;
	private JLabel crossings1Label;
	private JLabel crossings2Label;
	// Text
	private JTextField substratesField;
	private JTextField productsField;
	private JTextField crossings1Field;
	private JTextField crossings2Field;
	private JTextField roundsField;
	// Buttons
	private JButton solveButton;
	private JButton fromTable1Button;
	private JButton fromTable2Button;
	private JButton productOrderKnownButton;
	private JButton sampleInputButton;
	// Lists
	private VectorList[] rList;
	private VectorList[] crossingList;
	// Panel 
	private TangleSolvePanel tangleSolvePanel;
	// ??????
	private int rounds = TreeDisplayString.ROUNDS;
	private boolean Debug = false;
	private int numEq;

	private static String FORMAT = "Input: 4-plat canonical vector(s)/rational number(s) separated by space";

	// constructor
	public ProcessiveInput(final TangleSolvePanel tangleSolvePanel) {
		super(true);
		this.tangleSolvePanel = tangleSolvePanel;
		this.setLayout(new GridBagLayout());
		this.setOpaque(true);
		this.setBackground(UIManager.getColor("control"));

		this.roundsLabel = new JLabel("Predict up to round :", SwingConstants.LEFT);
		this.roundsField = new JTextField(3);
		this.roundsField.setText("4");

		this.substratesLabel = new JLabel("List of substrates :", SwingConstants.LEFT);
		this.substratesField = new JTextField(10);
		this.substratesField.setToolTipText(ProcessiveInput.FORMAT);

		this.productsLabel = new JLabel("List of products :", SwingConstants.LEFT);
		this.productsField = new JTextField(10);
		this.productsField.setToolTipText(ProcessiveInput.FORMAT);

		this.crossings1Field = new JTextField(3);
		this.crossings1Label = new JLabel(" crossing #");

		this.crossings2Field = new JTextField(3);
		this.crossings2Label = new JLabel(" crossing #");

		this.fromTable1Button = new JButton("From Table");
		this.fromTable2Button = new JButton("From Table");
		
		this.solveButton = new JButton("Solve");
		this.sampleInputButton = new JButton("Sample Input");
		
		this.productOrderKnownButton = new JButton("Product order known");

		// action listeners
		this.fromTable1Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveInput.this.tangleSolvePanel.getFourPlatTable().setVisible(true);
				final String substrates = ProcessiveInput.this.substratesField.getText();
				String substratesValue;
				if (substrates.equals("")) { // is the first substrate
					substratesValue = substrates + ProcessiveInput.this.tangleSolvePanel.getFourPlatTable().getValue();
				} else { // is not the first substrate
					substratesValue = substrates + ", " + ProcessiveInput.this.tangleSolvePanel.getFourPlatTable().getValue();
				}
				ProcessiveInput.this.substratesField.setText(substratesValue);
			}
		});

		this.fromTable2Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveInput.this.tangleSolvePanel.getFourPlatTable().setVisible(true);
				final String products = ProcessiveInput.this.productsField.getText();
				String productValue;
				if (products.equals("")) { // is the first product
					productValue = products + ProcessiveInput.this.tangleSolvePanel.getFourPlatTable().getValue();
				} else { // is not the first product
					productValue = products + ", " + ProcessiveInput.this.tangleSolvePanel.getFourPlatTable().getValue();
				}
				ProcessiveInput.this.productsField.setText(productValue);
			}
		});

		this.productOrderKnownButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveInput.this.tangleSolvePanel.getSelectPane().setVisible(true);
			}
		});

		this.solveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				VectorList substrateList = null;
				VectorList productList = null;
				try {
					String substrates = ProcessiveInput.this.substratesField.getText();
					if (ProcessiveInput.this.solvable(substrates)) {
						substrates = "fplist(" + substrates + ")";
						substrateList = (VectorList) Parser.evaluate(substrates);
					}

					String products = ProcessiveInput.this.productsField.getText();
					if (ProcessiveInput.this.solvable(products)) {
						products = "fplist(" + products + ")";
						productList = (VectorList) Parser.evaluate(products);
					}

					VectorList substrateCrossingList = null;
					VectorList productCrossingList = null;
					
					String crossings1 = ProcessiveInput.this.crossings1Field.getText();
					if (!crossings1.trim().equals("")) {
						crossings1 = "list(" + crossings1 + ")";
						substrateCrossingList = (VectorList) Parser.evaluate(crossings1);
					}

					String crossings2 = ProcessiveInput.this.crossings2Field.getText();
					if (!crossings2.trim().equals("")) {
						crossings2 = "list(" + crossings2 + ")";
						productCrossingList = (VectorList) Parser.evaluate(crossings2);
					}
					int productCrossingLength = 0;
					if (productCrossingList != null) {
						productCrossingLength = productCrossingList.length();
					}

					int productLength;
					if (productList != null) {
						productList = VectorList.filterSame(productList);
						productLength = ProcessiveInput.this.numberDiffferentCrossing(productList);
					} else {
						productLength = 0;
					}

					ProcessiveInput.this.numEq = Math.min(ProcessiveInput.MAX_EQ, productLength + productCrossingLength);
					ProcessiveInput.this.numEq++;

					ProcessiveInput.this.rList = new VectorList[ProcessiveInput.this.numEq];
					ProcessiveInput.this.crossingList = new VectorList[ProcessiveInput.this.numEq];

					ProcessiveInput.this.crossingList[0] = substrateCrossingList;
					ProcessiveInput.this.rList[0] = substrateList;
					
					for (int i = 1; i < ProcessiveInput.this.numEq; i++) {
						if (productList != null) {
							ProcessiveInput.this.rList[i] = (VectorList) productList.clone();
						}
						ProcessiveInput.this.crossingList[i] = productCrossingList;
					}

					final int d = Integer.parseInt(ProcessiveInput.this.roundsField.getText()) + 1;
					
					if (d > ProcessiveInput.this.rounds) {
						ProcessiveInput.this.rounds = d;
					}

					VectorList list = ProcessiveInput.this.putList(0);

					if (ProcessiveInput.this.crossingList[0] != null) {
						substrateList = new VectorList(ProcessiveInput.this.crossingList[0], substrateList);
					}

					if (ProcessiveInput.this.crossingList[1] != null) {
						productList = new VectorList(ProcessiveInput.this.crossingList[1], productList);
					}

					final DefaultMutableTreeNode pb = new DefaultMutableTreeNode(
							"Substrates: " + VectorList.curlyBraceKnotList(substrateList) + "; Products: " + 
							VectorList.curlyBraceKnotList(productList));
					final DefaultMutableTreeNode pb2 = new DefaultMutableTreeNode(
							"All solutions that use n products from the list");
					pb.add(pb2);
					ProcessiveInput.this.tangleSolvePanel.getTree().insertNode(pb);
					VectorList temp = (VectorList) list.clone();
					
					while (list.length() >= 2) {
						temp = (VectorList) list.clone();
						final InputData inData = new InputData(true, list, ProcessiveInput.this.rounds); 
						
						// list given to inData has
						// the form ((substrates), (products))
						
						final VectorList solutionList = inData.solve();
						if (ProcessiveInput.this.Debug) {
							System.out.println("solution list: " + solutionList);
						}

						ProcessiveInput.this.tangleSolvePanel.getTree().addSolutionList("n =  " + list.length(), solutionList, pb);
						list = VectorList.removeLast(temp);
					}
					ProcessiveInput.this.tangleSolvePanel.getDisplay().clearScreen();
					ProcessiveInput.this.clearTexts();

				} catch (final Exception exception) {
					exception.printStackTrace();
					System.out.println("Error at solve button " + exception);
					JOptionPane.showMessageDialog(ProcessiveInput.this.tangleSolvePanel.getFrame(), "Invalid input");
				}
			}
		});

		this.sampleInputButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveInput.this.substratesField.setText("1, 1/3, b(2,1)");
				ProcessiveInput.this.crossings2Field.setText("4");
			}
		});
		
		// Layout
		final GridBagConstraints constraints= new GridBagConstraints();
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		this.add(this.productOrderKnownButton, constraints);
		constraints.gridy = 1;
		this.add(this.substratesLabel, constraints);
		this.add(this.substratesField, constraints);
		this.add(this.crossings1Label, constraints);
		this.add(this.crossings1Field, constraints);
		this.add(this.fromTable1Button, constraints);
		constraints.gridy = 2;
		this.add(this.productsLabel, constraints);
		this.add(this.productsField, constraints);
		this.add(this.crossings2Label, constraints);
		this.add(this.crossings2Field, constraints);
		this.add(this.fromTable2Button, constraints);
		constraints.gridy = 3;
		this.add(this.roundsLabel, constraints);
		this.add(this.roundsField, constraints);
		this.add(this.solveButton, constraints);
		constraints.gridx = 4;
		this.add(this.sampleInputButton, constraints);
	} // end constructor

	/**
	 * Returns the number of four-plats with different crossing numbers in list
	 * @param list
	 * @return the number of four-plats with different crossing numbers in the given list
	 */
	private int numberDiffferentCrossing(VectorList list) {
		list = this.filterSameCrossing(list);
		return list.length();
	}

	private VectorList filterSameCrossing(final VectorList list) {
		if (list == null) {
			return null;
		} else if (ProcessiveInput.hasSameCrossing((FourPlat) list.head(), list.tail())) {
			return this.filterSameCrossing(list.tail());
		} else {
			return new VectorList(list.head(), this.filterSameCrossing(list.tail()));
		}
	}

	public static boolean hasSameCrossing(final FourPlat element, VectorList list) {

		for (; list != null; list = list.tail()) {
			if (element.getNumberCrossings() == ((FourPlat) list.head()).getNumberCrossings()) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unused")
	private int totalNumberFourPlats(VectorList list) {
		int number = 0;
		for (final VectorList temp = list; list != null; list = list.tail()) {
			final Rational rational = (Rational) list.head();
			final VectorList lst = FourPlat.enumerateFourPlats(rational.getNumerator());
			number += lst.length();
		}
		return number;
	}

	private VectorList putList(final int i) {
		if ((i < this.rList.length) && (i < this.crossingList.length)) {
			if ((this.rList[i] != null) || (this.crossingList[i] != null)) {
				return new VectorList(new VectorList(this.crossingList[i], this.rList[i]),
						this.putList(i + 1));
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private boolean solvable(String s) {
		s = s.trim();
		if (s.equals("")) {
			return false;
		} else {
			return true;
		}
	}

	private void clearTexts() {
		this.substratesField.setText("");
		this.productsField.setText("");
		this.crossings1Field.setText("");
		this.crossings2Field.setText("");
		this.rList = null;
		this.crossingList = null;
	}

}
