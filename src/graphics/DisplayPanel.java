/**
 * Author : Yuki Saka
 * @edited by Crista Moreno
 * Last Updated : 08/24/2013
 */
package graphics;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import objects.TangleNumerator;
import objects.VectorList;

public class DisplayPanel extends JPanel implements TreeSelectionListener {

	// constants
	private static final long serialVersionUID = 2445130781851696449L;
	
	// private fields
	private TangleNumerator tangleNumerator = null;
	private JList tangleGraphList; // holds the tangle graphs to be displayed
	private JComboBox dropDownListTangleEquations; // drop down list of other tangle equations
	private JLabel illustrationsLabel;

	// constructor
	public DisplayPanel(final JFrame frame) {
		super(true);

		this.setLayout(new GridBagLayout());
		
		// Prepare tangle graph list.
		this.tangleGraphList = new TangleGraphList(frame);
		this.tangleGraphList.setToolTipText("Tangle graphs equations and four plats display.");
		
		// Prepare drop down list of tangle equations/minimum representations of 0 tangle
		this.dropDownListTangleEquations = new JComboBox();
		this.dropDownListTangleEquations.setToolTipText("List of different (minimum) representations of O tangle");
		this.dropDownListTangleEquations.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				final int i = DisplayPanel.this.dropDownListTangleEquations.getSelectedIndex();
				if ((DisplayPanel.this.tangleNumerator != null) && (i >= 0)) {
					DisplayPanel.this.tangleNumerator.select(i + 1);
					DisplayPanel.this.tangleGraphList.setModel(DisplayPanel.this.tangleNumerator.selection());
				}
			}
		});
		
		// Prepare illustrations label.
		this.illustrationsLabel = new JLabel();
		this.illustrationsLabel.setText("Illustrations from Knots and Links by Dale Rolfsen (Publish or Perish Press, 1976)");
		
		// Layout 
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridy = 0;
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		this.add(new JScrollPane(this.tangleGraphList), constraints); // place to display tangle graphs
		constraints.weightx = 0;
		constraints.weighty = 0;
		constraints.gridy = 1;
		this.add(this.dropDownListTangleEquations, constraints);
		constraints.gridy = 2;
		this.add(this.illustrationsLabel, constraints);

		
		// Component esthetics 
		
		// drop down list of tangle equations
		this.dropDownListTangleEquations.setOpaque(true); // Did not like how it looked. Not what I expected.
		this.dropDownListTangleEquations.setBackground(Color.CYAN); // Sets background color of drop list 
		this.dropDownListTangleEquations.setFont(this.dropDownListTangleEquations.getFont().deriveFont(18.0f)); // Increases the font size of drop down list.
		
		// tangle graph list
		this.tangleGraphList.setBackground(Color.WHITE); // Sets background color of the tangle graphs display.
		this.tangleGraphList.setFont(this.tangleGraphList.getFont().deriveFont(18.0f));
		
		// illustation's label
		this.setBackground(Color.PINK); // Sets background color of illustrations label.
		this.illustrationsLabel.setFont(this.illustrationsLabel.getFont().deriveFont(14.0f));
	}// end of constructor

	/**
	 * Puts the tangle graphs of Tangle Diagrams in the display panel.
	 * 
	 * @param tangleNumerator
	 * TangleNum
	 */
	public void setTangleGraph(final TangleNumerator tangleNumerator) {
		this.tangleNumerator = tangleNumerator;
		this.tangleGraphList.setModel(this.tangleNumerator.selection());
		this.boxElements(this.tangleNumerator.nameList, this.tangleNumerator.getSelectedIndex());
		this.repaint();
	}

	public void clearScreen() {
		this.tangleGraphList.setModel(new DefaultListModel());
		this.dropDownListTangleEquations.setModel(new DefaultComboBoxModel());
		this.repaint();
	}

	/**
	 * Method of TreeSelection Listener. Called when value of tree selection
	 * changes
	 * 
	 * @param event
	 *            TreeSelectionEvent
	 */
	@Override
	public void valueChanged(final TreeSelectionEvent event) {
		final TreePath sl = event.getPath();
		final DefaultMutableTreeNode node = (DefaultMutableTreeNode) sl.getLastPathComponent();
		if (node.getUserObject() instanceof TangleNumerator) {
			this.tangleNumerator = (TangleNumerator) node.getUserObject();
			this.tangleGraphList.setModel(this.tangleNumerator.selection());
			this.boxElements(this.tangleNumerator.nameList, this.tangleNumerator.getSelectedIndex());
		}
	}

	public void boxElements(final VectorList gList, final int selected) {
		this.dropDownListTangleEquations.removeAllItems();
		if (gList.length() > 1) {
			for (VectorList temp = gList; temp != null; temp = temp.tail()) {
				this.dropDownListTangleEquations.addItem(temp.head());
			}
			this.dropDownListTangleEquations.setSelectedIndex(selected - 1);
		}
	}
}