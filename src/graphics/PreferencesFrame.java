package graphics;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JFrame;

import util.Preferences;

public class PreferencesFrame extends JFrame {
	private static final long serialVersionUID = 2212409583096084099L;

	// constructor
	public PreferencesFrame() {
		final Container contentPane = this.getContentPane();

		contentPane.setLayout(new GridBagLayout());

		contentPane.setPreferredSize(new Dimension(200, 200));

		/* Toggle Checkboxes: */
		final JCheckBox showBoxesCheckBox = new JCheckBox("Show/Hide boxes");
		showBoxesCheckBox.setSelected(Preferences.getInstance().isShowBoxes());

		//contentPane.add(showBoxesCheckBox);

		showBoxesCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final boolean checked = showBoxesCheckBox.isSelected();
				Preferences.getInstance().setShowBoxes(checked);
			}
		});

		/* FourPlat color: */
		final JButton fourPlatColorButton = new JButton("Four Plat Color");
		fourPlatColorButton.setOpaque(true);
		fourPlatColorButton.setBackground(Preferences.getInstance().getFourPlatColor1());

		//contentPane.add(fourPlatColorButton);

		fourPlatColorButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final Color initial = Preferences.getInstance().getFourPlatColor1();
				final Color color = JColorChooser.showDialog(PreferencesFrame.this, "Select a FourPlat color...", initial);
				if (color != null) {
					fourPlatColorButton.setBackground(color);
					Preferences.getInstance().setFourPlatColor1(color);
				}
			}
		});

		/* Tangle Strand 1 color: */
		final JButton tangleColor1Button = new JButton("Strand 1 Color");
		tangleColor1Button.setOpaque(true);
		tangleColor1Button.setBackground(Preferences.getInstance().getTangleColor1());

		//contentPane.add(tangleColor1Button);

		tangleColor1Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final Color initial = Preferences.getInstance().getTangleColor1();
				final Color color = JColorChooser.showDialog(PreferencesFrame.this, "Select a Tangle 1 color...", initial);
				if (color != null) {
					tangleColor1Button.setBackground(color);
					Preferences.getInstance().setTangleColor1(color);
				}
			}
		});

		/* Tangle Strand 2 color: */
		final JButton tangleColor2Button = new JButton("Strand 2 Color");
		tangleColor2Button.setOpaque(true);
		tangleColor2Button.setBackground(Preferences.getInstance().getTangleColor2());

		//contentPane.add(tangleColor2Button);

		tangleColor2Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final Color initial = Preferences.getInstance().getTangleColor2();
				final Color color = JColorChooser.showDialog(PreferencesFrame.this, "Select a Tangle 2 color...", initial);
				if (color != null) {
					tangleColor2Button.setBackground(color);
					Preferences.getInstance().setTangleColor2(color);
				}
			}
		});

		// Layout
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridwidth = 1;
		constraints.gridheight = 1;

		constraints.gridy = 0;
		constraints.gridx = 0;
		contentPane.add(showBoxesCheckBox, constraints);

		constraints.gridy = 1;
		contentPane.add(fourPlatColorButton, constraints);

		constraints.gridy = 2;
		contentPane.add(tangleColor1Button, constraints);

		constraints.gridy = 3;
		contentPane.add(tangleColor2Button, constraints);

		this.pack();
	}
}
