/**
 * @author Yuki Saka
 * Last Updated : April 14, 2013
 * @edited by Crista Moreno
 */
package graphics;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class SelectDialog extends JDialog {

	// constants
	private static final long serialVersionUID = 8457103851745853940L;

	// constructor
	public SelectDialog(final JFrame frame, final TangleSolvePanel gPanel, final JPanel panel, final String title) {
		super(frame, title, false);
		this.getContentPane().add(panel);
		this.pack();
	}
}