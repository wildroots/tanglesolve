/**
 * Table that shows up when press "From Table" button in input panes
 * @author Yuki Saka
 * @edited by Crista Moreno
 * Last Updated : August 24, 2013
 * 
 */
package graphics;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import objects.FourPlat;
import objects.VectorList;

public class FourPlatTable extends JDialog {

	// constants
	private static final long serialVersionUID = -5293945469983080741L;
	private static final int COLUMNS = 2;
	private static final int MAX_CROSSING = 11;
	private static final int COLUMN_WIDTH = 200;
	
	// private fields
	private DefaultTableModel[] table;
	private JTable fourPlatTable;
	private VectorList fourPlatList = null;
	private JTextField textField;
	private JComboBox crossingsDropDownList;
	private JButton okButton;
	private JLabel illustrationLabel;

	// constructor
	@SuppressWarnings("serial")
	public FourPlatTable(final JFrame frame, final String title) {
		super(frame, title, true);
		try {
			this.setSize(600, 500);
			this.getContentPane().setLayout(new GridBagLayout());
			this.textField = new JTextField();
			
			this.crossingsDropDownList = new JComboBox();
			// Changes the font size to 17.0f
			this.crossingsDropDownList.setFont(this.crossingsDropDownList.getFont().deriveFont(17.0f));
			for (int i = 0; i < FourPlatTable.MAX_CROSSING; i++) {
				this.crossingsDropDownList.addItem(i + "-crossings");
			}
			this.crossingsDropDownList.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(final ActionEvent event) {
					if (FourPlatTable.this.fourPlatTable != null) {
						final int i = FourPlatTable.this.crossingsDropDownList.getSelectedIndex();
						FourPlatTable.this.fourPlatTable.setModel(FourPlatTable.this.table[i]);
						final IconRenderer renderer = new IconRenderer();
						
						for (int k = 0; k < FourPlatTable.COLUMNS; k++) {
							FourPlatTable.this.fourPlatTable.getColumnModel().getColumn(k)
									.setCellRenderer(renderer);
							FourPlatTable.this.fourPlatTable.getColumnModel().getColumn(k)
									.setWidth(FourPlatTable.COLUMN_WIDTH);
						}
					}
				}
			});

			// button
			this.okButton = new JButton("OK");
			// Sets the okButton label font size to 15.0f.
			this.okButton.setFont(this.okButton.getFont().deriveFont(15.0f));
			this.okButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(final ActionEvent event) {
					FourPlatTable.this.setVisible(false);
				}
			});

			// create four plat table from 3-crossings to 10-crossings
			this.table = new DefaultTableModel[FourPlatTable.MAX_CROSSING];
			final String[] colh = new String[FourPlatTable.COLUMNS];
			
			for (int i = 0; i < FourPlatTable.COLUMNS; i++) {
				colh[i] = "" + i;
			}
			
			for (int i = 0; i < FourPlatTable.MAX_CROSSING; i++) {
				this.table[i] = new DefaultTableModel(FourPlatTable.fourPlatTable(i), colh) {
					@Override
					public boolean isCellEditable(final int rowIndex, final int columnIndex) {
						return false;
					}
				};
			}
			this.fourPlatTable = new JTable(this.table[0]);
			this.fourPlatTable.setToolTipText("Double-click on the diagram to select knots/links from the table");
			this.fourPlatTable.setPreferredScrollableViewportSize(new Dimension(640, 400));
			//this.fourPlatTable.setBackground(Color.GREEN); Not needed.
			final JScrollPane scrollpane = new JScrollPane(this.fourPlatTable);

			// jtb.getTableHeader().setSize(new Dimension(650, 400));
			// scrollpane.setSize(new Dimension(700, 500));
			// selection
			this.fourPlatTable.setCellSelectionEnabled(true);
			this.fourPlatTable.setRowSelectionAllowed(false);
			this.fourPlatTable.setColumnSelectionAllowed(false);
			this.fourPlatTable.setRowHeight(90);
			final IconRenderer renderer = new IconRenderer();
			
			for (int i = 0; i < FourPlatTable.COLUMNS; i++) {
				this.fourPlatTable.getColumnModel().getColumn(i).setCellRenderer(renderer);
				this.fourPlatTable.getColumnModel().getColumn(i).setWidth(FourPlatTable.COLUMN_WIDTH);
			}

			// Selection detection
			this.fourPlatTable.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(final MouseEvent event) {
					if (event.getClickCount() == 2) {
						final Point point = event.getPoint();
						final int i = FourPlatTable.this.fourPlatTable.rowAtPoint(point);
						final int j = FourPlatTable.this.fourPlatTable.columnAtPoint(point);
						if ((i >= 0) && (j >= 0)) {
							if ((FourPlatGraph) FourPlatTable.this.fourPlatTable.getModel().getValueAt(i, j) != null) {
								final FourPlat fp = ((FourPlatGraph) FourPlatTable.this.fourPlatTable
										.getModel().getValueAt(i, j)).getFourPlat();
								if (!VectorList.member(fp, FourPlatTable.this.fourPlatList)) {
									FourPlatTable.this.fourPlatList = new VectorList(fp, FourPlatTable.this.fourPlatList);
									FourPlatTable.this.textField.setText(FourPlatTable.this.fourPlatList.toString());
								}
							}
						}
					}
				}
			});

			this.illustrationLabel = new JLabel();
			this.illustrationLabel.setText("Illustrations from Knots and Links by Dale Rolfsen (Publish or Perish Press, 1976)");
			// Sets the font size of the illustration label to 15.0f.
			this.illustrationLabel.setFont(this.illustrationLabel.getFont().deriveFont(14.0f));
			
			// Layout
			final GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridx = 0;
			constraints.gridwidth = 2;
			constraints.gridy = 0;
			constraints.fill = GridBagConstraints.BOTH;
			constraints.weighty = 1;
			this.getContentPane().add(scrollpane, constraints);
			constraints.gridwidth = 1;
			constraints.weighty = 0;
			constraints.gridy = 1;
			this.getContentPane().add(this.crossingsDropDownList, constraints);
			constraints.gridx = 1;
			this.getContentPane().add(this.textField, constraints);
			constraints.gridx = 0;
			constraints.gridwidth = 2;
			constraints.gridy = 2;
			this.getContentPane().add(this.okButton, constraints);
			constraints.gridx = 0;
			constraints.gridy = 3;
			constraints.gridwidth = 2;
			this.getContentPane().add(this.illustrationLabel, constraints);
			this.setSize(700, 550);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	} // end constructor

	// methods
	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			this.fourPlatList = null;
			this.textField.setText("");
		}
		super.setVisible(visible);
	}

	// Gets value from Knot / Link Table and puts it into text field
	public String getValue() {
		String s = "";
		for (VectorList fourPlatList = this.fourPlatList; fourPlatList != null; fourPlatList = fourPlatList.tail()) {
			final VectorList list = ((FourPlat) fourPlatList.head()).getVector();
			if (s.equals("")) { // First substrate
				s = s + list;
			} else { // Not the first substrate.
				s = s + ", " + list;
			}
		}
		return s;
	}

	private static FourPlatGraph[][] fourPlatTable(final int crs) throws Exception {
		final VectorList fourPlatList = FourPlat.enumerateFourPlats(crs);
		final int rows = (fourPlatList.length() / FourPlatTable.COLUMNS) + 1;
		final FourPlatGraph[][] fourPlatGraph = new FourPlatGraph[rows][FourPlatTable.COLUMNS];
		int i = 0;
		int j = 0;
		for (VectorList list = fourPlatList; list != null; list = list.tail(), j++) {
			if (j >= FourPlatTable.COLUMNS) {
				j = j % FourPlatTable.COLUMNS;
				i++;
			}
			fourPlatGraph[i][j] = new FourPlatGraph((FourPlat) list.head(), 1);
		}
		return fourPlatGraph;
	}
}