/** 
 * @author Yuki Saka
 * Last Update : April 13, 2013
 * @edited by Crista Moreno
 */
package graphics;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import objects.FourPlat;
import objects.Rational;
import objects.VectorList;
import util.Preferences;

public class FourPlatGraph implements Icon {
	private static int KT_SIZE = 700;
	private static Icon[][] pics = new Icon[FourPlatGraph.KT_SIZE][FourPlatGraph.KT_SIZE];
	public static boolean img = true;
	
	// private fields
	private FourPlat fourPlat;
	private GeneralPath fourPlatPath;
	private Rectangle Bounds;
	private int iconX;
	private int iconY;
	private Icon pic = null;
	
	// constructors
	public FourPlatGraph() {
		
	}

	public FourPlatGraph(final FourPlat fourPlat, final float scale) {
		this.fourPlat = fourPlat;

		if (FourPlatGraph.img && (fourPlat.getNumberCrossings() <= 10)) {
			this.pic = FourPlatGraph.pics[(int) fourPlat.getRational().getNumerator()][(int) fourPlat.getRational().getDenominator()];
		}
		if (this.pic == null) {
			// System.out.println(fourplat + " has no image");
		}
		this.fourPlatPath = this.getFourPlatPath(scale * 10, scale * 10, scale * 50, fourPlat);
	}
	
	// ----------------------ACCESSOR METHODS------------------------//
	/**
	 * Returns the four plat path.
	 * 
	 * @param twistSpace
	 * @param coilWidth
	 * @param fourPlatHeight
	 * @param fourPlat
	 * @return 
	 */
	public GeneralPath getFourPlatPath(final float twistSpace, final float coilWidth, final float fourPlatHeight, final FourPlat fourPlat) {
		// The xc position variable does change.
		float xc = 0;
		
		// These values do not change.
		final float yc = 0;
		final float yc2 = fourPlatHeight / 3.0f;
		final float yc3 = (2.0f / 3.0f) * fourPlatHeight;
		
		VectorList fourPlatList = fourPlat.getVector();
		
		final GeneralPath fourPlatPath = new GeneralPath();
		final int[] cn1 = { 1, 1, 0, 0 };  // What does this array do?
		final int[] cn2 = { 0, 0, 1, 1 }; // What does this array do?
		
		for (int i = 1; fourPlatList != null; fourPlatList = fourPlatList.tail(), i++) {
			final long numberTwists = ((Rational) fourPlatList.head()).getNumerator();
			
			if ((i % 2) == 1) { // Odd entries in four plat list are placed in middle row of four plat.
				// negative horizontal twists 
				final GeneralPath[] twistPath = TwistBoxUtil.twistBox(xc, yc2, yc2, coilWidth, twistSpace, (int) numberTwists, TwistBoxUtil.NEGATIVE_HORIZONTAL, cn1);
				fourPlatPath.append(twistPath[0], false);
				fourPlatPath.append(twistPath[1], false);
				
				// top row lines of four plat
				fourPlatPath.moveTo(xc, yc);
				fourPlatPath.lineTo(xc + (2 * twistSpace) + (numberTwists * coilWidth), yc);
				
			} else { // Even entries in four plat list are placed in top row of four plat.
				// positive horizontal twists
				final GeneralPath[] twistPath = TwistBoxUtil.twistBox(xc, yc, yc2, coilWidth, twistSpace, (int) numberTwists, TwistBoxUtil.POSITIVE_HORIZONTAL, cn2);
				fourPlatPath.append(twistPath[0], false);
				fourPlatPath.append(twistPath[1], false);
				
				// middle row lines of four plat
				fourPlatPath.moveTo(xc, yc3);
				fourPlatPath.lineTo(xc + (2 * twistSpace) + (numberTwists * coilWidth), yc3);
			}
			// the bottom line of four plat
			fourPlatPath.moveTo(xc, fourPlatHeight);
			fourPlatPath.lineTo(xc + (2 * twistSpace) + (numberTwists * coilWidth), fourPlatHeight);
			xc += (2 * twistSpace) + (numberTwists * coilWidth);
		}
		// caps
		fourPlatPath.moveTo(0, 0);
		fourPlatPath.quadTo(-2 * coilWidth, 0, 0, yc2);
		fourPlatPath.moveTo(0, yc3);
		fourPlatPath.quadTo(-2 * coilWidth, fourPlatHeight, 0, fourPlatHeight);
		
		// the other end
		fourPlatPath.moveTo(xc, 0);
		fourPlatPath.quadTo(xc + (2 * coilWidth), 0, xc, yc2);
		fourPlatPath.moveTo(xc, yc3);
		fourPlatPath.quadTo(xc + (2 * coilWidth), fourPlatHeight, xc, fourPlatHeight);
	
		final AffineTransform affineTransformation = new AffineTransform();
		if (this.pic == null) {
			affineTransformation.translate(2 * coilWidth, 5);
		} else {
			affineTransformation.translate((2 * coilWidth) + this.pic.getIconWidth(), 5);
		}
		fourPlatPath.transform(affineTransformation);
		this.Bounds = fourPlatPath.getBounds();
		this.Bounds.width += 5;
		this.Bounds.height += 5;
		if (this.pic != null) {
			this.Bounds.width += this.pic.getIconWidth();
			this.Bounds.height = Math.max(this.pic.getIconHeight(), this.Bounds.height);
		}
		return fourPlatPath;
	}

	public FourPlat getFourPlat() {
		return fourPlat;
	}

	public Rectangle getBounds() {
		return this.Bounds;
	}

	@Override
	public int getIconWidth() {
		return this.Bounds.width;
	}

	@Override
	public int getIconHeight() {
		return this.Bounds.height;
	}

	/**
	 * 
	 * @param x
	 * @param y
	 */
	public void translate(final float x, final float y) {
		final AffineTransform affineTransformation = new AffineTransform();
		affineTransformation.translate(x, y);
		this.fourPlatPath.transform(affineTransformation);
		this.iconX = (int) x;
		this.iconY = (int) y;
	}

	public static void loadImages() {
		VectorList fourPlatList = null;
	
		for (int i = 0; i <= 10; i++) {
			fourPlatList = VectorList.append(fourPlatList, FourPlat.enumerateFourPlats(i));
		}
	
		for (; fourPlatList != null; fourPlatList = fourPlatList.tail()) {
			final FourPlat fourPlat = (FourPlat) (fourPlatList.head());
			final Rational rational = fourPlat.getRational();
			final FourPlatGraph dummy = new FourPlatGraph();
			if (FourPlatGraph.img) {
				try {
					FourPlatGraph.pics[(int) rational.getNumerator()][(int) rational.getDenominator()] = new ImageIcon(
							dummy.getClass().getResource("images/" + fourPlat.getNumberCrossings() + "-" + rational.getPair() + ".jpg"));
	
					final Rational rn = rational.invMod();
					FourPlatGraph.pics[(int) rn.getNumerator()][(int) rn.getDenominator()] = new ImageIcon(
							dummy.getClass().getResource("images/" + fourPlat.getNumberCrossings() + "-" + rational.getPair() + ".jpg"));
				} catch (final Exception exception2) {
					// System.out.println("Exception at load image : " +
					// e2.toString());
				}
			}
	
			// System.out.println(pics[(int)rat.num()][(int)rat.denom()]);
		}
	}

	/**
	 * Draw four plat
	 * @param graphics
	 */
	public void draw(final Graphics graphics) {
		final Graphics2D graphics2D = (Graphics2D) graphics;
		graphics2D.setColor(Preferences.getInstance().getFourPlatColor1());
		graphics2D.draw(this.fourPlatPath);
		if (this.pic != null) {
			this.pic.paintIcon(null, graphics, this.iconX, this.iconY);
		}
	}

	@Override
	public void paintIcon(final Component c, final Graphics graphics, final int x, final int y) {
		final Graphics2D graphics2D = (Graphics2D) graphics;
		/*
		 * AffineTransform at = new AffineTransform(0,0,0,0,0,0);
		 * path.transform(at); at.setToTranslation(x,y); path.transform(at);
		 */
		graphics2D.setColor(Preferences.getInstance().getFourPlatColor1());
		graphics2D.draw(this.fourPlatPath);
		if (this.pic != null) {
			this.pic.paintIcon(c, graphics, 0, 0);
		}
	}

	@Override
	public String toString() {
		return this.fourPlat.toString();
	}
}