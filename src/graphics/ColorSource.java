package graphics;

import java.awt.Color;

import javax.vecmath.Color3f;

public interface ColorSource {
	public Color getColor();
	public Color3f getColor3f();
}
