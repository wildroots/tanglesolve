/** 
 * @author Yuki Saka
 * @edited by Crista Moreno
 * Last Updated: April 21, 2013
 * 
 */
package graphics;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;

import javax.swing.Icon;

import objects.Rational;
import objects.Tangle;
import util.TangleColor1ColorSource;
import util.TangleColor2ColorSource;

public class EquationGraph implements Icon {
	// constants
	// tangle strand colors
	public static final ColorSource COLORSOURCE_1 = new TangleColor1ColorSource();
	public static final ColorSource COLORSOURCE_2 = new TangleColor2ColorSource();

	// private fields
	private final int boxWidth;
	private final int coilWidth; // Sets the coilwidth, or the size of the twist
	private final int twistSpace; // twistSpace can be adjusted and the curves match up
	private final int tangleSpace;
	// paths
	private GeneralPath numeratorTopPath;
	private GeneralPath numeratorBottomPath;
	private GeneralPath ofToObTopPath;
	private GeneralPath obToRTopPath;
	private GeneralPath ofToObBottomPath;
	private GeneralPath obToRBottomPath;
	// path colors
	private ColorSource numeratorTopColorSource;
	private ColorSource numeratorBottomColorSource;
	private ColorSource ofToObTopColorSource;
	private ColorSource obToRTopColorSource;
	private ColorSource ofToObBottomColorSource;
	private ColorSource obToRBottomColorSource;
	// tangles
	private Tangle ofTangle;
	private Tangle obTangle;
	private Tangle rTangle;
	private int round;
	// tangle graphs
	private final TangleGraph ofTangleGraph;
	private final TangleGraph obTangleGraph;
	private final TangleGraph rTangleGraph[];
	// Maximum height of all three rectangles.
	private final float maxHeight;
	// Of Tangle 
	private final Rectangle rectangle1;
	// Ob Tangle
	private final Rectangle rectangle2;
	// k Tangle
	private final Rectangle rectangle3;
	private final Rectangle bounds;

	// constructor
	public EquationGraph(final Tangle ofTangle, final Tangle obTangle, final Tangle rTangle, int round, final int scale) {
		this.ofTangle = ofTangle;
		this.obTangle = obTangle;
		this.rTangle = rTangle;

		final int[] cn1 = { 2, 0, 0, 2 };
		final int[] cn2 = { 0, 0, 0, 0 };
		final int[] cn3 = { 0, 2, 2, 0 };

		// TODO Possibly allow the user to change these parameters.
		this.boxWidth = scale * 10;
		this.coilWidth = scale * 5;
		this.twistSpace = scale * 5;
		this.tangleSpace = scale * 10;

		this.ofTangleGraph = new TangleGraph(this.twistSpace, this.coilWidth, this.boxWidth, cn1, ofTangle, scale);
		this.obTangleGraph = new TangleGraph(this.twistSpace, this.coilWidth, this.boxWidth, cn2, obTangle, scale);

		if (round == 0) {
			if (rTangle.getRational().isInfinity()) {
				this.rTangleGraph = new TangleGraph[1];
				this.rTangleGraph[0] = new TangleGraph(this.twistSpace, this.coilWidth, this.boxWidth, cn3, new Tangle(new Rational(1, 0)), scale);
				this.round = round = 1;
			} else {
				this.rTangleGraph = new TangleGraph[1];
				this.rTangleGraph[0] = new TangleGraph(this.twistSpace, this.coilWidth, this.boxWidth, cn3, new Tangle(new Rational(0)), scale);
				this.round = round = 1;
			}
		} else if (rTangle.getRational().isInteger()) {
			this.rTangleGraph = new TangleGraph[1];
			this.rTangle = new Tangle(new Rational(rTangle.getRational().getNumerator() * round));
			this.rTangleGraph[0] = new TangleGraph(this.twistSpace, this.coilWidth, this.boxWidth, cn3, this.rTangle, scale);
			this.round = round = 1;
		} else {
			if (rTangle.getRational().isInfinity()) {
				this.rTangleGraph = new TangleGraph[1];
				this.rTangleGraph[0] = new TangleGraph(this.twistSpace, this.coilWidth, this.boxWidth, cn3, new Tangle(new Rational(0)), scale);
				this.round = round = 1;
			} else {
				this.rTangleGraph = new TangleGraph[round];
				for (int i = 0; i < round; i++) {
					this.rTangleGraph[i] = new TangleGraph(this.twistSpace, this.coilWidth, this.boxWidth, cn2, rTangle, scale);
				}
				this.round = round;
			}
		}
		this.rectangle1 = this.ofTangleGraph.getBounds();
		this.rectangle2 = this.obTangleGraph.getBounds();
		this.rectangle3 = this.rTangleGraph[0].getBounds();

		this.maxHeight = Math.max(this.rectangle1.height, Math.max(this.rectangle2.height, this.rectangle3.height));

		final float y1 = (this.maxHeight / 2.0f) - (this.rectangle1.height / 2.0f);
		final float y2 = (this.maxHeight / 2.0f) - (this.rectangle2.height / 2.0f);
		final float y3 = (this.maxHeight / 2.0f) - (this.rectangle3.height / 2.0f);

		final float x2 = this.rectangle1.width + this.tangleSpace;
		final float x3 = x2 + this.rectangle2.width + this.tangleSpace;

		this.ofTangleGraph.translate(0, y1);
		this.obTangleGraph.translate(x2, y2);
		for (int i = 0; i < round; i++) {
			this.rTangleGraph[i].translate(x3 + (i * this.rTangleGraph[0].getBounds().width), y3);
		}

		this.connect(this.maxHeight, 0, 0, y1, x2, y2, x3, y3, this.rectangle1, this.rectangle2, this.rectangle3);

		final AffineTransform affineTransform = new AffineTransform();
		affineTransform.translate(this.tangleSpace / 2.0f, this.tangleSpace);
		this.ofTangleGraph.translate(this.tangleSpace / 2.0f, this.tangleSpace);
		this.obTangleGraph.translate(this.tangleSpace / 2.0f, this.tangleSpace);

		for (int i = 0; i < round; i++) {
			this.rTangleGraph[i].translate(this.tangleSpace / 2.0f, this.tangleSpace);
		}

		this.numeratorTopPath.transform(affineTransform);
		this.numeratorBottomPath.transform(affineTransform);
		this.ofToObTopPath.transform(affineTransform);
		this.ofToObBottomPath.transform(affineTransform);
		this.obToRTopPath.transform(affineTransform);
		this.obToRBottomPath.transform(affineTransform);

		this.bounds = new Rectangle(0, 0, this.rectangle1.width + this.rectangle2.width + this.rectangle3.width + (4 * this.tangleSpace), (int) (this.maxHeight + (2 * this.tangleSpace)));
	} // end of constructor

	public Tangle getOfTangle() {
		return ofTangle;
	}

	public Tangle getObTangle() {
		return obTangle;
	}

	public Tangle getRTangle() {
		return rTangle;
	}
	
	public ColorSource getNumeratorTopColor() {
		return numeratorTopColorSource;
	}
	
	public ColorSource getNumeratorBottomColor() {
		return numeratorBottomColorSource;
	}
	
	public ColorSource getOfToObTopColor() {
		return ofToObTopColorSource;
	}
	
	public ColorSource getObToRTopColor() {
		return obToRTopColorSource;
	}
	
	public ColorSource getOfToObBottomColor() {
		return ofToObBottomColorSource;
	}
	
	public ColorSource getObToRBottomColor() {
		return obToRBottomColorSource;
	}

	public TangleGraph getOfTangleGraph() {
		return ofTangleGraph;
	}

	public TangleGraph getObTangleGraph() {
		return obTangleGraph;
	}

	public TangleGraph[] getRTangleGraph() {
		return rTangleGraph;
	}

	public Rectangle getBounds() {
		return this.bounds;
	}

	@Override
	public int getIconWidth() {
		return this.bounds.width + 5;
	}

	@Override
	public int getIconHeight() {
		return this.bounds.height + 5;
	}

	private void setKnotColorSource(ColorSource colorSource) {
		this.setNumeratorColorSources(colorSource, colorSource);
		this.setTangleColorSources(colorSource, colorSource, colorSource, colorSource, colorSource, colorSource);
		this.setConnectionColorSources(colorSource, colorSource, colorSource, colorSource);
	}

	/**
	 * Sets the colors of the strands connecting each of the tangles Of, Ob and
	 * k.
	 * 
	 * @param ofTopColor
	 * @param ofBottomColor
	 * @param obTopColor
	 * @param obBottomColor
	 */
	private void setConnectionColorSources(ColorSource ofTopColorSource, ColorSource ofBottomColorSource, ColorSource obTopColorSource, ColorSource obBottomColorSource) {
		this.ofToObTopColorSource = ofTopColorSource;
		this.ofToObBottomColorSource = ofBottomColorSource;

		this.obToRTopColorSource = obTopColorSource;
		this.obToRBottomColorSource = obBottomColorSource;

	}

	/**
	 * Sets the color of the two tangle strands for each tangle Of, Ob and k.
	 * 
	 * @param ofTangleColor1
	 * @param ofTangleColor2
	 * @param obTangleColor1
	 * @param obTangleColor2
	 * @param kTangleColor1
	 * @param kTangleColor2
	 */
	private void setTangleColorSources(ColorSource ofTangleColorSource1, ColorSource ofTangleColorSource2, ColorSource obTangleColorSource1, ColorSource obTangleColorSource2, ColorSource kTangleColorSource1, ColorSource kTangleColorSource2) {
		this.ofTangleGraph.colorSources[0] = ofTangleColorSource1;
		this.ofTangleGraph.colorSources[1] = ofTangleColorSource2;

		this.obTangleGraph.colorSources[0] = obTangleColorSource1;
		this.obTangleGraph.colorSources[1] = obTangleColorSource2;

		this.rTangleGraph[0].colorSources[0] = kTangleColorSource1;
		this.rTangleGraph[0].colorSources[1] = kTangleColorSource2;
	}

	/**
	 * Sets the color of the top and bottom parts of the numerator strands.
	 * 
	 * @param numeratorTopColorSource
	 * @param numeratorBottomColorSource
	 */
	private void setNumeratorColorSources(ColorSource numeratorTopColorSource, ColorSource numeratorBottomColorSource) {
		this.numeratorTopColorSource = numeratorTopColorSource;
		this.numeratorBottomColorSource = numeratorBottomColorSource;
	}

	public void connect(final float r, final float x, final float y, final float y1, final float x2, final float y2, final float x3, final float y3, final Rectangle r1, final Rectangle r2, final Rectangle r3) {

		final Rational parity1 = this.ofTangle.parity();
		final Rational parity2 = this.obTangle.parity();
		final Rational parity3 = this.rTangleGraph[0].tangle.parity();

		// System.out.println(of + " " + of.parity() + " , " +
		// ob + " " + ob.parity() + " , " +
		// kg[0].tangle + " " + kg[0].tangle.parity());
		this.ofToObTopPath = new GeneralPath();
		this.obToRTopPath = new GeneralPath();
		this.ofToObBottomPath = new GeneralPath();
		this.obToRBottomPath = new GeneralPath();
		this.numeratorTopPath = new GeneralPath();
		this.numeratorBottomPath = new GeneralPath();

		// Tangle Of top path connection to Tangle Ob
		this.ofToObTopPath.moveTo(x2 - this.tangleSpace, y1);
		this.ofToObTopPath.quadTo((x2 - this.tangleSpace) + (this.tangleSpace / 4.0f), y1, ((x2 - this.tangleSpace) + x2) / 2.0f, (y1 + y2) / 2.0f);
		this.ofToObTopPath.quadTo(x2 - (this.tangleSpace / 4.0f), y2, x2, y2);

		// Tangle Ob top path connection to Tangle k
		this.obToRTopPath.moveTo(x3 - this.tangleSpace, y2);
		this.obToRTopPath.quadTo((x3 - this.tangleSpace) + (this.tangleSpace / 4.0f), y2, ((x3 - this.tangleSpace) + x3) / 2.0f, (y2 + y3) / 2.0f);
		this.obToRTopPath.quadTo(x3 - (this.tangleSpace / 4.0f), y3, x3, y3);

		// Tangle Of bottom path connection to Tangle Ob
		this.ofToObBottomPath.moveTo(x2 - this.tangleSpace, y1 + r1.height);
		this.ofToObBottomPath.quadTo((x2 - this.tangleSpace) + (this.tangleSpace / 4.0f), y1 + r1.height, ((x2 - this.tangleSpace) + x2) / 2.0f, (y1 + r1.height + y2 + r2.height) / 2.0f);
		this.ofToObBottomPath.quadTo(x2 - (this.tangleSpace / 4.0f), y2 + r2.height, x2, y2 + r2.height);

		// Tangle Ob bottom path connection to Tangle k
		this.obToRBottomPath.moveTo(x3 - this.tangleSpace, y2 + r2.height);
		this.obToRBottomPath.quadTo((x3 - this.tangleSpace) + (this.tangleSpace / 4.0f), y2 + r2.height, ((x3 - this.tangleSpace) + x3) / 2.0f, (y2 + r2.height + y3 + r3.height) / 2.0f);
		this.obToRBottomPath.quadTo(x3 - (this.tangleSpace / 4.0f), y3 + r3.height, x3, y3 + r3.height);

		// Numerator top path
		this.numeratorTopPath.moveTo(x, y1);
		this.numeratorTopPath.lineTo(x, y);
		final float xf = x + r1.width + r2.width + r3.width + (2 * this.tangleSpace);
		this.numeratorTopPath.quadTo(x, y - (this.tangleSpace / 1.0f), xf / 2.0f, y - (this.tangleSpace / 1.0f));
		this.numeratorTopPath.quadTo(xf, y - (this.tangleSpace / 1.0f), xf, y);
		this.numeratorTopPath.lineTo(xf, y3);

		// Numerator bottom path
		this.numeratorBottomPath.moveTo(x, y1 + r1.height);
		this.numeratorBottomPath.lineTo(x, y + r);
		this.numeratorBottomPath.quadTo(x, y + r + (this.tangleSpace / 1.0f), xf / 2.0f, y + r + (this.tangleSpace / 1.0f));
		this.numeratorBottomPath.quadTo(xf, y + r + (this.tangleSpace / 1.0f), xf, y + r);
		this.numeratorBottomPath.lineTo(xf, y3 + r3.height);

		if (parity1.isZero()) {
			if (parity2.isZero()) {
				if (parity3.isZero()) {
					// N(0 + 0 + 0) produces a link
					this.setNumeratorColorSources(COLORSOURCE_1, COLORSOURCE_2);
					this.setTangleColorSources(COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_1, COLORSOURCE_2);
					this.setConnectionColorSources(COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_1, COLORSOURCE_2);
				} else if (parity3.isOne()) {
					// N(0 + 0 + 1) produces a knot	
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isInfinity()) {
					// N(0 + 0 + infinity) produces a knot	
					setKnotColorSource(COLORSOURCE_1);
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isOne()) {
				if (parity3.isZero()) {
					// N(0 + 1 + 0) produces a knot	
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isOne()) {
					// N(0 + 1 + 1) produces a link
					this.setNumeratorColorSources(COLORSOURCE_1, COLORSOURCE_2);
					this.setTangleColorSources(COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_1);
					this.setConnectionColorSources(COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_1);
				} else if (parity3.isInfinity()) {
					// N(0 + 1 + infinity) produces a knot	
					setKnotColorSource(COLORSOURCE_1);
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isInfinity()) {
				if (parity3.isZero()) {
					// N(0 + infinity + 0) produces a knot	
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isOne()) {
					// N(0 + infinity + 1) produces a knot
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isInfinity()) {
					// N(0 + infinity + infinity) produces a link
					this.setNumeratorColorSources(COLORSOURCE_1, COLORSOURCE_1);
					this.setTangleColorSources(COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_1);
					this.setConnectionColorSources(COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2);
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		} else if (parity1.isOne()) {
			if (parity2.isZero()) {
				if (parity3.isZero()) {
					// N(1 + 0 + 0) produces a knot
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isOne()) {
					// N(1 + 0 + 1) produces a link
					this.setNumeratorColorSources(COLORSOURCE_2, COLORSOURCE_1);
					this.setTangleColorSources(COLORSOURCE_2, COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_1, COLORSOURCE_2);
					this.setConnectionColorSources(COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_1, COLORSOURCE_2);
				} else if (parity3.isInfinity()) {
					// N(1 + 0 + infinity) produces a knot	
					setKnotColorSource(COLORSOURCE_1);
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isOne()) {
				if (parity3.isZero()) {
					// N(1 + 1 + 0) produces a link
					this.setNumeratorColorSources(COLORSOURCE_2, COLORSOURCE_1);
					this.setTangleColorSources(COLORSOURCE_2, COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_1);
					this.setConnectionColorSources(COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_1);
				} else if (parity3.isOne()) {
					// N(1 + 1 + 1) produces a knot
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isInfinity()) {
					// N(1 + 1 + infinity) produces a knot
					setKnotColorSource(COLORSOURCE_1);
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isInfinity()) {
				if (parity3.isZero()) {
					// N(1 + infinity + 0) produces a knot
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isOne()) {
					// N(1 + infinity + 1) produces a knot
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isInfinity()) {
					// N(1 + infinity + infinity) produces a link
					this.setNumeratorColorSources(COLORSOURCE_1, COLORSOURCE_1);
					this.setTangleColorSources(COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_1);
					this.setConnectionColorSources(COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2);
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		} else if (parity1.isInfinity()) {
			if (parity2.isZero()) {
				if (parity3.isZero()) {
					// N(infinity + 0 + 0) produces a knot
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isOne()) {
					// N(infinity + 0 + 1) produces a knot
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isInfinity()) {
					// N(infinity + 0 + infinity) produces a link
					this.setNumeratorColorSources(COLORSOURCE_1, COLORSOURCE_1);
					this.setTangleColorSources(COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_1);
					this.setConnectionColorSources(COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2);
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isOne()) {
				if (parity3.isZero()) {
					// N(infinity + 1 + 0) produces a knot
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isOne()) {
					// N(infinity + 1 + 1) produces a knot
					setKnotColorSource(COLORSOURCE_1);
				} else if (parity3.isInfinity()) {
					// N(infinity + 1 + infinity) produces a link
					this.setNumeratorColorSources(COLORSOURCE_1, COLORSOURCE_1);
					this.setTangleColorSources(COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_1);
					this.setConnectionColorSources(COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2);
				} else {
					throw new IllegalArgumentException();
				}
			} else if (parity2.isInfinity()) {
				if (parity3.isZero()) {
					// N(infinity + infinity + 0) produces a link
					this.setNumeratorColorSources(COLORSOURCE_2, COLORSOURCE_2);
					this.setTangleColorSources(COLORSOURCE_2, COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2);
					this.setConnectionColorSources(COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2);
				} else if (parity3.isOne()) {
					// N(infinity + infinity + 1) produces a link
					this.setNumeratorColorSources(COLORSOURCE_2, COLORSOURCE_2);
					this.setTangleColorSources(COLORSOURCE_2, COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2);
					this.setConnectionColorSources(COLORSOURCE_1, COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2);
				} else if (parity3.isInfinity()) {
					// N(infinity + infinity + infinity) produces a link
					this.setNumeratorColorSources(COLORSOURCE_1, COLORSOURCE_1);
					this.setTangleColorSources(COLORSOURCE_1, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_1);
					this.setConnectionColorSources(COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2, COLORSOURCE_2);
				} else {
					throw new IllegalArgumentException();
				}
			} else {
				throw new IllegalArgumentException();
			}
		} else {
			throw new IllegalArgumentException();
		}

		for (int i = 1; i < this.round; i++) {
			if (parity3.isZero()) {
				this.rTangleGraph[i].colorSources[0] = COLORSOURCE_1;
				this.rTangleGraph[i].colorSources[1] = COLORSOURCE_2;
			}
			this.rTangleGraph[i].colorSources[0] = this.rTangleGraph[i - 1].colorSources[1];
			this.rTangleGraph[i].colorSources[1] = this.rTangleGraph[i - 1].colorSources[0];
		}

	}

	public void translate(final float x, final float y) {
		final AffineTransform affineTransformation = new AffineTransform();
		affineTransformation.translate(x, y);
		this.ofTangleGraph.translate(x, y);
		this.obTangleGraph.translate(x, y);

		for (int i = 0; i < this.round; i++) {
			this.rTangleGraph[i].translate(x, y);
		}

		this.numeratorTopPath.transform(affineTransformation);
		this.numeratorBottomPath.transform(affineTransformation);
		this.ofToObTopPath.transform(affineTransformation);
		this.ofToObBottomPath.transform(affineTransformation);
		this.obToRTopPath.transform(affineTransformation);
		this.obToRBottomPath.transform(affineTransformation);
	}

	@Override
	public void paintIcon(final Component component, final Graphics graphics, final int x, final int y) {
		final AffineTransform affineTransform = new AffineTransform();
		affineTransform.translate(x, y);
		final Graphics2D graphics2D = (Graphics2D) graphics;
		this.ofTangleGraph.translate(x, y);
		this.obTangleGraph.translate(x, y);
		this.ofTangleGraph.draw(graphics);
		this.obTangleGraph.draw(graphics);

		for (int i = 0; i < this.round; i++) {
			this.rTangleGraph[i].translate(x, y);
			this.rTangleGraph[i].draw(graphics);
		}
		graphics2D.setColor(this.numeratorTopColorSource.getColor());
		this.numeratorTopPath.transform(affineTransform);
		graphics2D.draw(this.numeratorTopPath);

		graphics2D.setColor(this.numeratorBottomColorSource.getColor());
		this.numeratorBottomPath.transform(affineTransform);
		graphics2D.draw(this.numeratorBottomPath);

		graphics2D.setColor(this.ofToObTopColorSource.getColor());
		this.ofToObTopPath.transform(affineTransform);
		graphics2D.draw(this.ofToObTopPath);

		graphics2D.setColor(this.ofToObBottomColorSource.getColor());
		this.ofToObBottomPath.transform(affineTransform);
		graphics2D.draw(this.ofToObBottomPath);

		graphics2D.setColor(this.obToRTopColorSource.getColor());
		this.obToRTopPath.transform(affineTransform);
		graphics2D.draw(this.obToRTopPath);

		graphics2D.setColor(this.obToRBottomColorSource.getColor());
		this.obToRBottomPath.transform(affineTransform);
		graphics2D.draw(this.obToRBottomPath);
	}

	public void draw(final Graphics graphics) {
		final Graphics2D graphics2D = (Graphics2D) graphics;
		this.ofTangleGraph.draw(graphics);
		this.obTangleGraph.draw(graphics);

		for (int i = 0; i < this.round; i++) {
			this.rTangleGraph[i].draw(graphics);
		}
		graphics2D.setColor(this.numeratorTopColorSource.getColor());
		graphics2D.draw(this.numeratorTopPath);

		graphics2D.setColor(this.numeratorBottomColorSource.getColor());
		graphics2D.draw(this.numeratorBottomPath);

		graphics2D.setColor(this.ofToObTopColorSource.getColor());
		graphics2D.draw(this.ofToObTopPath);

		graphics2D.setColor(this.ofToObBottomColorSource.getColor());
		graphics2D.draw(this.ofToObBottomPath);

		graphics2D.setColor(this.obToRTopColorSource.getColor());
		graphics2D.draw(this.obToRTopPath);

		graphics2D.setColor(this.obToRBottomColorSource.getColor());
		graphics2D.draw(this.obToRBottomPath);
	}
}