/**
 * @author Unknown
 * Last Updated : April 14, 2013
 * @edited by Crista Moreno
 */
package graphics;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import objects.FourPlat;
import objects.Parser;
import objects.Rational;
import objects.Tangle;
import objects.TangleNumerator;
import objects.VectorList;

public class TangleDiagramInput extends JPanel {

	// constants
	private static final long serialVersionUID = 3984555151804505706L;

	// private fields
	private TangleSolvePanel tangleSolvePanel;
	// labels
	private JLabel tangleOfLabel;
	private JLabel tangleObLabel;
	private JLabel recombinantTangleLabel;
	// text fields
	private JTextField tangleOfField;
	private JTextField tangleObField;
	private JTextField recombinantTangleField;
	// buttons
	private JButton displayButton;
	private JButton sampleInputButton;
 
	// constructor
	public TangleDiagramInput(final TangleSolvePanel tangleSolvePanel) {
		super(true);
		this.tangleSolvePanel = tangleSolvePanel;
		this.setLayout(new GridBagLayout());
		this.setOpaque(true);
		this.setBackground(UIManager.getColor("control"));

		this.tangleOfLabel = new JLabel("Tangle Of = ", SwingConstants.RIGHT);
		this.tangleObLabel = new JLabel("Tangle Ob = ", SwingConstants.RIGHT);
		this.recombinantTangleLabel = new JLabel("Recombinant Tangle R =  ", SwingConstants.RIGHT);

		this.tangleOfField = new JTextField(10);
		this.tangleObField = new JTextField(10);
		this.recombinantTangleField = new JTextField(3);

		this.displayButton = new JButton("Display");
		this.sampleInputButton = new JButton("Sample Input");

		// action listeners
		this.displayButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				try {
					String tangleOfInput = TangleDiagramInput.this.tangleOfField.getText();
					String tangleObInput = TangleDiagramInput.this.tangleObField.getText();
					
					final String recombinantTangleInput = TangleDiagramInput.this.recombinantTangleField.getText();

					tangleOfInput = "tglist(" + tangleOfInput + ")";
					tangleObInput = "tglist(" + tangleObInput + ")";

					final VectorList<Tangle> tangleOfList = (VectorList) Parser.evaluate(tangleOfInput);
					final VectorList<Tangle> tangleObList = (VectorList) Parser.evaluate(tangleObInput);
					final Rational recombinantTangle = (Rational) Parser.evaluate(recombinantTangleInput);

					final boolean compress = false;
					final Tangle tangleOf = (Tangle) tangleOfList.head();
					final Tangle tangleOb = (Tangle) tangleObList.head();
					final Tangle tangleR = new Tangle(recombinantTangle);
					
					

					final VectorList prods = new VectorList(new FourPlat(tangleOf,
							tangleOb), new VectorList(new FourPlat(tangleOf,
							tangleOb.addTail(recombinantTangle.getNumerator())), null));

					final TangleNumerator tangleNumerator = new TangleNumerator(prods,
							tangleOf, tangleOb, tangleR, compress);

					TangleDiagramInput.this.tangleSolvePanel.getDisplay().setTangleGraph(tangleNumerator);
				} catch (final Exception exception) {
					System.out.println("Exception at Graphic input " + exception);
				}
			}
		});

		this.sampleInputButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				TangleDiagramInput.this.tangleOfField.setText("(3 2 1)");
				TangleDiagramInput.this.tangleObField.setText("(2)");
				TangleDiagramInput.this.recombinantTangleField.setText("1");
			}
		});

		// Layout GridBag format
		//			gridx = 0 	gridx = 1	gridx = 2 ...
		// gridy = 0
		// gridy = 1
		// gridy = 2
		// .
		// .
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		
		constraints.gridy = 0;
		constraints.gridx = 0;
		this.add(this.sampleInputButton, constraints);
		
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 1;
		this.add(this.tangleOfLabel, constraints);
		this.add(this.tangleOfField, constraints);
		this.add(this.tangleObLabel, constraints);
		this.add(this.tangleObField, constraints);
		
		constraints.gridy = 2;
		this.add(this.recombinantTangleLabel, constraints);
		this.add(this.recombinantTangleField, constraints);
		constraints.gridy = 3;
		constraints.gridx = 0;
		this.add(this.displayButton, constraints);
		
	}// end of constructor
}
