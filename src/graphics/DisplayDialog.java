/** 
 * @author Yuki Saka
 * Last Updated : August 24, 2013
 * @edited by Crista Moreno
 */

package graphics;

import javax.swing.JDialog;
import javax.swing.JFrame;

public class DisplayDialog extends JDialog {

	// constants
	private static final long serialVersionUID = 2678132983794327759L;

	// constructor
	public DisplayDialog(final JFrame frame, final TangleSolvePanel gPanel, final DisplayPanel displayPanel,
			final String title) {
		super(frame, title, false);
		this.setSize(600, 400);
		this.getContentPane().add(displayPanel);
	}
}