/**
 * @author Yuki Saka
 * Last Updated : April 14, 2013
 * @edited by Crista Moreno
 */
package graphics;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.Icon;

import objects.FourPlat;
import objects.Rational;
import objects.Tangle;

public class SolutionGraph implements Icon {
	
	// constants 
	private static final int FOURPLATGRAPH_SCALE = 2; 
	private static final int EQUATIONGRAPH_SCALE = 4; 
	private static final int SPACE = 10;
	
	// private fields
	private final int round;
	// tangles
	private final Tangle ofTangle;
	private final Tangle obTangle;
	private final Tangle rTangle;
	// four plats
	private final FourPlat fourPlat;
	// graphs
	private final EquationGraph equationGraph;
	private final FourPlatGraph fourPlatGraph;
	private final Rectangle rectangle;

	// constructors
	public SolutionGraph(final Tangle ofTangle, final Tangle obTangle, final Tangle rTangle, final FourPlat fourPlat, final int round) {
		this(ofTangle, obTangle, rTangle, fourPlat, round, EQUATIONGRAPH_SCALE, FOURPLATGRAPH_SCALE);
	}

	public SolutionGraph(final Tangle ofTangle, final Tangle obTangle, final Tangle rTangle, final FourPlat fourPlat, final int round, 
			final int equationGraphScale, final int fourPlatScale) {
		this.ofTangle = ofTangle;
		this.obTangle = obTangle;
		this.rTangle = rTangle;
		this.fourPlat = fourPlat;
		this.round = round;

		this.equationGraph = new EquationGraph(ofTangle, obTangle, rTangle, round, equationGraphScale);
			// Scales the size of the four plat graph on the display panel.
		this.fourPlatGraph = new FourPlatGraph(fourPlat, fourPlatScale);
		this.equationGraph.translate(SPACE, SPACE);

		this.fourPlatGraph.translate(this.equationGraph.getBounds().width + (2 * SPACE),
				(SPACE + (this.equationGraph.getBounds().height / 2.0f))
						- (this.fourPlatGraph.getBounds().height / 2.0f));
		
		this.rectangle = new Rectangle();
		this.rectangle.x = 0;
		this.rectangle.y = 0;
		this.rectangle.width = (3 * SPACE) + this.equationGraph.getBounds().width
				+ this.fourPlatGraph.getBounds().width;
		
		this.rectangle.height = Math.max(this.equationGraph.getBounds().height
				+ (4 * SPACE), this.fourPlatGraph.getBounds().height
				+ (4 * SPACE));
	}

	// ----------------------ACCESSOR METHODS------------------------//
	public int getRound() {
		return round;
	}
	
	public Tangle getOfTangle() {
		return this.ofTangle;
	}

	public Tangle getObTangle() {
		return this.obTangle;
	}

	public Tangle getRTangle() {
		return this.rTangle;
	}

	public TangleGraph getOfTangleGraph() {
		return equationGraph.getOfTangleGraph();
	}
	
	public TangleGraph getObTangleGraph() {
		return equationGraph.getObTangleGraph();
	}
	
	public TangleGraph[] getRTangleGraph() {
		return equationGraph.getRTangleGraph();
	}
	
	public FourPlat getFourPlat() {
		return fourPlat;
	}
	
	public EquationGraph getEquationGraph() {
		return equationGraph;
	}
	
	@Override
	public int getIconHeight() {
		return this.rectangle.height;
	}

	@Override
	public int getIconWidth() {
		return this.rectangle.width;
	}

	@Override
	public void paintIcon(final Component component, final Graphics graphics, final int x,
			final int y) {
		final Graphics2D graphics2D = (Graphics2D) graphics;
		// clears the background
	
		graphics2D.setColor(Color.BLACK);
		graphics2D.drawString("=", this.equationGraph.getBounds().width + SPACE, (this.equationGraph.getBounds().height / 2.0f) + SPACE);
		
		if (this.rTangle.getRational().isInfinity()) {
			Rational rational;
			if (this.round == 0) {
				rational = new Rational(0, 0);
			} else {
				rational = new Rational(0);
			}
			graphics2D.drawString("N(" + this.ofTangle + " + " + this.obTangle + " + (" + rational + ")) = " 
					+ this.fourPlat.shortDescription(), 3 * SPACE, this.equationGraph.getBounds().height +
					(3 * SPACE));
		} else {
			graphics2D.drawString("N(" + this.ofTangle + " + " + this.obTangle
							+ " + (" + (this.rTangle.getRational().getNumerator() * this.round)
							+ ")) = " + this.fourPlat.shortDescription(),
					3 * SPACE, this.equationGraph.getBounds().height + (3 * SPACE));
		}
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	
		this.equationGraph.draw(graphics);
		this.fourPlatGraph.draw(graphics);
	}

	@Override
	public String toString() {
		return this.equationGraph.getOfTangle() + " " + this.equationGraph.getObTangle()
				+ " " + this.equationGraph.getRTangle();
	}

}
