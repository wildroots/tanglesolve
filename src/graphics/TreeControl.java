/** 
 * @author Yuki Saka
 * Last Updated : August 24, 2013
 * @edited by Crista Moreno
 */
package graphics;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import objects.EquationSystem;
import objects.FourPlat;
import objects.Tangle;
import objects.TangleNumerator;
import objects.VectorList;

public class TreeControl extends JTree {

	// constants
	private static final long serialVersionUID = -309444056660558462L;
	
	// private fields
	private DefaultTreeModel tree;
	private DefaultMutableTreeNode root;
	private VectorList solutionVector = null;
	private JFrame frame;
	
	// constructor
	public TreeControl(final JFrame frame) {
		this.frame = frame;
		try {

			this.root = new DefaultMutableTreeNode(new String(TreeDisplayString.INITIAL_STRING));
			// fixes the cell row height of the root node
			this.setRowHeight(0);
			this.tree = new DefaultTreeModel(this.root);
			this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
			this.setModel(this.tree);
			// Sets the background color of the solution tree.
			this.setBackground(Color.white);
			// Sets the font size of the solution tree.
			this.setFont(this.getFont().deriveFont(17.0f));

			Icon leafIcon = null;
			leafIcon = new ImageIcon(this.getClass().getResource("leaf.jpg"));
			final Icon openIcon = null;
			final Icon closeIcon = null;

			this.setCellRenderer(new MyTreeCellRenderer(openIcon, closeIcon, leafIcon));

			this.addKeyListener(new KeyListener() {
				@Override
				public void keyTyped(final KeyEvent event) {
				}

				@Override
				public void keyPressed(final KeyEvent event) {
				}

				@Override
				public void keyReleased(final KeyEvent event) {
					final int keyCode = event.getKeyCode();
					if (keyCode == KeyEvent.VK_DELETE) {
						final TreePath sl = TreeControl.this.getSelectionPath();
						if (sl != null) {
							final DefaultMutableTreeNode node = (DefaultMutableTreeNode) sl
									.getLastPathComponent();
							((DefaultTreeModel) TreeControl.this.getModel())
									.removeNodeFromParent(node);
						}
					}
				}
			});
		} catch (final Exception exception) {
			System.out.println("Exception " + exception + " at constructor of tree control");
		}
	}// end of constructor

	public void clearSelected() {
		final TreePath selectionPath = this.getSelectionPath();
		if (selectionPath != null) {
			final DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectionPath.getLastPathComponent();
			((DefaultTreeModel) this.getModel()).removeNodeFromParent(node);
		}
	}

	public void clearAll() {
		this.root.removeAllChildren();
		this.tree.reload(this.root);
	}

	public String solutionString() {
		return this.solutionVector.toString();
	}

	public void insertNode(final DefaultMutableTreeNode node) {
		this.root.add(node);
	}

	public void addSolutionList(final String sname, final VectorList sl) {
		this.addSolutionList(sname, sl, this.root);
	}

	/**
	 * Adds new solutions to the tree in form of node.
	 * 
	 * @param sname
	 * String
	 * 
	 * @param sl
	 * VectorList has each node of the form (EqnSystem solsyst, VectorList
	 * sol_OR)
	 * 
	 * @param node
	 * DefaultMutableTreeNode root of the tree
	 */
	public void addSolutionList(final String sname, final VectorList sl,
			final DefaultMutableTreeNode node) {
		try {
			VectorList knots = null;
			VectorList links = null;
			int numberRationals = 0;
			int numberSumsRationals = 0;

			final DefaultMutableTreeNode nl = new DefaultMutableTreeNode(sname); // Node that holds sname
			node.add(nl);

			// //////////Processing each node of _sl//////////////
			for (VectorList temp = sl; temp != null; temp = temp.tail()) {
				// each node of sl is temp.head(), of type VectorList
				// head of each node of _sl is _ls, has form: (substrate, prod1,
				// prod2, prod3, ..)
				// tail of each node of _sl is _rest, represents solution for O
				// and R but not in presentable form yet.

				final EquationSystem system = (EquationSystem) ((VectorList) temp.head()).head(); // EquationSystem
				// (substrate, product1, prod2, ..))
				final VectorList rest = ((VectorList) temp.head()).tail(); // VectorList solutions
				// final int round = syst.size(); // Not used.

				//// Processing the part with system of equations
				DefaultMutableTreeNode systemNode;
				systemNode = new DefaultMutableTreeNode(system); // node that holds the EquationSystem system

				if (VectorList.allSame(system.getSystem())) {
					JOptionPane.showMessageDialog(this.frame, "infinite number of solutions exist!");
				}

				// put knot/link info
				knots = VectorList.append(TreeControl.knotsList(system.getSystem().tail()), knots);
				links = VectorList.append(TreeControl.linksList(system.getSystem().tail()), links);
				
				if (rest != null) {
					nl.add(systemNode); // add systemNode to tree
				}

				//////// Processing the part with solutions O, R and TangleNum ////////
				VectorList tnList = null;
				for (VectorList t = rest; t != null; t = t.tail()) {
					final VectorList nt = (VectorList) t.head();
					final TangleNumerator tangleNumerator = new TangleNumerator(system.getSystem(), (Tangle) nt.head(),
							(Tangle) nt.getNth(2), (Tangle) nt.getNth(3));
					tnList = new VectorList(tangleNumerator, tnList);
				}

				if (tnList != null) {
					tnList = VectorList.filterSame(tnList);
					numberRationals += TreeControl.numberRationals(tnList);
					numberSumsRationals += tnList.length() - TreeControl.numberRationals(tnList);

					for (VectorList t = tnList; t != null; t = t.tail()) {
						final DefaultMutableTreeNode git = new DefaultMutableTreeNode(
								t.head(), false);
						// each _git is a node of _systNode that holds a
						// TangleNumn

						systemNode.add(git); // add _git as a node of _systNode
					}
				}
				// //////////////////////////////////////////////////////////////

				// //////////Processing each systNode for theorem/////////////
				if (system.hasTheorem()) {
					final DefaultMutableTreeNode claimNode = new DefaultMutableTreeNode(TreeDisplayString.RATIONAL_SOLUTION_NODE);
					systemNode.add(claimNode);
					final DefaultMutableTreeNode theoremNode = new DefaultMutableTreeNode(system.getTheorem());
					claimNode.add(theoremNode);

				}
				// /////////////////////////////////////////////////////
			}
			////////////////End of Processing each node of _sl

			knots = VectorList.filterSame(knots);
			links = VectorList.filterSame(links);
			
			int numberKnots = 0;
			int numberLinks = 0;
			if (knots != null) {
				numberKnots = knots.length();
			}
			if (links != null) {
				numberLinks = links.length();
			}
			// add specifics
			nl.insert(new DefaultMutableTreeNode(this.outputStatisticsTangleSolutions(numberRationals, numberSumsRationals)), 0);
			nl.insert(new DefaultMutableTreeNode(this.outputStatisticsKnotLinkProducts(numberKnots, numberLinks)), 0);
			nl.insert(new DefaultMutableTreeNode(new String("There are " + EquationSystem.getTotalRationalSystems()
					+ " systems whose solutions for O must be Rational. ")), 0);

			this.tree.reload(this.root);
		} catch (final Exception exception) {
			System.out.println("Exception " + exception + " at tree control");
		}
	}// end of addSolutionList

	public static VectorList knotsList(final VectorList knotList) {
		if (knotList == null) {
			return null;
		} else if (((FourPlat) knotList.head()).isKnot()) {
			return new VectorList(knotList.head(), TreeControl.knotsList(knotList.tail()));
		} else {
			return TreeControl.knotsList(knotList.tail());
		}
	}

	public static VectorList linksList(final VectorList linkList) {
		if (linkList == null) {
			return null;
		} else if (((FourPlat) linkList.head()).isKnot()) {
			return TreeControl.linksList(linkList.tail());
		} else {
			return new VectorList(linkList.head(), TreeControl.linksList(linkList.tail()));
		}
	}

	public static int numberRationals(VectorList list) {
		int numberRationals = 0;
		for (; list != null; list = list.tail()) {
			if (((TangleNumerator) list.head()).isRational()) {
				numberRationals++;
			}
		}
		return numberRationals;
	}

	public VectorList writableSolution(final VectorList list) {
		if (list != null) {
			return new VectorList(this.eachSolution((VectorList) list.head()),
					this.writableSolution(list.tail()));
		} else {
			return null;
		}
	}

	private String eachSolution(final VectorList solutionList) {
		try {
			VectorList fourPlatList = (VectorList) solutionList.head();
			VectorList tangleList = solutionList.tail();
			
			String s1 = "fplist(";
			for (; fourPlatList != null; fourPlatList = fourPlatList.tail()) {
				final FourPlat fourPlat = (FourPlat) fourPlatList.head();
				s1 = s1 + fourPlat.getRational() + " ";
			}
			s1 = s1 + ")";
			String s2 = "";
			for (; tangleList != null; tangleList = tangleList.tail()) {
				final String s = ((VectorList) tangleList.head()).toString();
				s2 = s2 + " tglist" + s;
			}
			final String s = "(" + s1 + " " + s2 + ")";
			return s;
		} catch (final Exception exception) {
			System.out.println("Error at writable soln : " + exception);
			return "";
		}
	}

	public String outputStatisticsTangleSolutions(final int oRationals, final int oSumsRationals) {
		final int totalModels = oRationals + oSumsRationals;
		String s = totalModels + " solutions: ";
		s = s + "(" + oRationals + " with O Rational, " + oSumsRationals + " with O Sum of two rationals)";
		return s;
	}

	public String outputStatisticsKnotLinkProducts(final int knots, final int links) {
		final int totalProducts = knots + links;
		String s = "";
		s = s + totalProducts + " different products: ";
		s = s + "(" + knots + " knots, " + links + " links)";
		return s;
	}
}
