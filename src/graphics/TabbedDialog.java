/**
 * @author Yuki Saka
 * Last Updated : August 24, 2013
 * @edited by Crista Moreno
 */
package graphics;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class TabbedDialog extends JDialog {

	// constants
	private static final long serialVersionUID = 1857245126464842016L;

	// constructor
	public TabbedDialog(final JFrame frame, final TangleSolvePanel gPanel, final JTabbedPane tabbedPane, final String title) {
		super(frame, title, false);
		this.setSize(600, 190);
		this.getContentPane().add(tabbedPane);
		this.setVisible(true);
	}
}