/**
 * Author : Unknown
 * Last Updated : August 24, 2013
 * @edited by Crista Moreno
 */

package graphics;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import objects.FourPlat;
import objects.InputData;
import objects.Parser;
import objects.Tangle;
import objects.VectorList;

public class ProcessiveOrderKnown extends JPanel {

	// constants
	private static final long serialVersionUID = 1L;
	private static final int MAX_EQ = 6;
	private static final String FORMAT = "Input: 4-plat canonical vector(s)/rational number(s) separated by space";
	
	// private fields
	// Labels
	private JLabel substratesLabel;
	private JLabel products1Label;
	private JLabel products2Label;
	private JLabel products3Label;
	private JLabel products4Label;
	private JLabel products5Label;
	
	private JLabel crossings1Label;
	private JLabel crossings2Label;
	private JLabel crossings3Label;
	private JLabel crossings4Label;
	private JLabel crossings5Label;
	private JLabel crossings6Label;
	private JLabel roundsLabel;
	
	// Text fields
	private JTextField substratesField;
	private JTextField products1Field;
	private JTextField products2Field;
	private JTextField products3Field;
	private JTextField products4Field;
	private JTextField products5Field;
	
	private JTextField crossings1Field;
	private JTextField crossings2Field;
	private JTextField crossings3Field;
	private JTextField crossings4Field;
	private JTextField crossings5Field;
	private JTextField crossings6Field;
	
	private JTextField roundsField;
	
	// Buttons
	private JButton fromTable1Button;
	private JButton fromTable2Button;
	private JButton fromTable3Button;
	private JButton fromTable4Button;
	private JButton fromTable5Button;
	private JButton fromTable6Button;
	private JButton solveButton;
	
	// Lists
	private VectorList[] rList;
	private VectorList[] crossingsList;
	
	// Panel
	private TangleSolvePanel tangleSolvePanel;
	private int rounds = TreeDisplayString.ROUNDS;

	// constructor
	public ProcessiveOrderKnown(final TangleSolvePanel tangleSolvePanel) {
		super(true);
		this.tangleSolvePanel = tangleSolvePanel;
		this.setLayout(new GridBagLayout());
		this.setOpaque(true);
		this.setBackground(UIManager.getColor("control"));

		this.roundsLabel = new JLabel("# of rounds displayed :");
		this.roundsField = new JTextField(3);
		this.roundsField.setText("4");
		
		this.substratesLabel = new JLabel("Substrate(s): ", SwingConstants.LEFT);
		this.products1Label = new JLabel("Product(s) 1: ", SwingConstants.LEFT);
		this.products2Label = new JLabel("Product(s) 2: ", SwingConstants.LEFT);
		this.products3Label = new JLabel("Product(s) 3: ", SwingConstants.LEFT);
		this.products4Label = new JLabel("Product(s) 4: ", SwingConstants.LEFT);
		this.products5Label = new JLabel("Product(s) 5: ", SwingConstants.LEFT);

		this.crossings1Label = new JLabel("/ crossing # ", SwingConstants.RIGHT);
		this.crossings2Label = new JLabel("/ crossing # ", SwingConstants.RIGHT);
		this.crossings3Label = new JLabel("/ crossing # ", SwingConstants.RIGHT);
		this.crossings4Label = new JLabel("/ crossing # ", SwingConstants.RIGHT);
		this.crossings5Label = new JLabel("/ crossing # ", SwingConstants.RIGHT);
		this.crossings6Label = new JLabel("/ crossing # ", SwingConstants.RIGHT);

		this.substratesField = new JTextField(20);
		this.products1Field = new JTextField(20);
		this.products2Field = new JTextField(20);
		this.products3Field = new JTextField(20);
		this.products4Field = new JTextField(20);
		this.products5Field = new JTextField(20);
		
		this.substratesField.setToolTipText(ProcessiveOrderKnown.FORMAT);
		this.products1Field.setToolTipText(ProcessiveOrderKnown.FORMAT);
		this.products2Field.setToolTipText(ProcessiveOrderKnown.FORMAT);
		this.products3Field.setToolTipText(ProcessiveOrderKnown.FORMAT);
		this.products4Field.setToolTipText(ProcessiveOrderKnown.FORMAT);
		this.products5Field.setToolTipText(ProcessiveOrderKnown.FORMAT);
		
		this.crossings1Field = new JTextField(3);
		this.crossings2Field = new JTextField(3);
		this.crossings3Field = new JTextField(3);
		this.crossings4Field = new JTextField(3);
		this.crossings5Field = new JTextField(3);
		this.crossings6Field = new JTextField(3);
		
		this.fromTable1Button = new JButton("From Table");
		this.fromTable2Button = new JButton("From Table");
		this.fromTable3Button = new JButton("From Table");
		this.fromTable4Button = new JButton("From Table");
		this.fromTable5Button = new JButton("From Table");
		this.fromTable6Button = new JButton("From Table");
		this.solveButton = new JButton("Solve");

		// action listeners
		this.fromTable1Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveOrderKnown.this.tangleSolvePanel.getFourPlatTable().setVisible(true);
				final String substratesString = ProcessiveOrderKnown.this.substratesField.getText();
				ProcessiveOrderKnown.this.substratesField.setText(addCommasBetweenEntries(substratesString));
			}
		});

		this.fromTable2Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveOrderKnown.this.tangleSolvePanel.getFourPlatTable().setVisible(true);
				final String products1String = ProcessiveOrderKnown.this.products1Field.getText();
				ProcessiveOrderKnown.this.products1Field.setText(addCommasBetweenEntries(products1String));
			}
		});

		this.fromTable3Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveOrderKnown.this.tangleSolvePanel.getFourPlatTable().setVisible(true);
				final String products2String = ProcessiveOrderKnown.this.products2Field.getText();
				ProcessiveOrderKnown.this.products2Field.setText(addCommasBetweenEntries(products2String));
			}
		});

		this.fromTable4Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveOrderKnown.this.tangleSolvePanel.getFourPlatTable().setVisible(true);
				final String products3String = ProcessiveOrderKnown.this.products3Field.getText();
				ProcessiveOrderKnown.this.products3Field.setText(addCommasBetweenEntries(products3String));
			}
		});

		this.fromTable5Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveOrderKnown.this.tangleSolvePanel.getFourPlatTable().setVisible(true);
				final String products4String = ProcessiveOrderKnown.this.products4Field.getText();
				ProcessiveOrderKnown.this.products4Field.setText(addCommasBetweenEntries(products4String));
			}
		});

		this.fromTable6Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveOrderKnown.this.tangleSolvePanel.getFourPlatTable().setVisible(true);
				final String products5String = ProcessiveOrderKnown.this.products5Field.getText();
				ProcessiveOrderKnown.this.products5Field.setText(addCommasBetweenEntries(products5String));
			}
		});

		this.solveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ProcessiveOrderKnown.this.rList = new VectorList[6];
				ProcessiveOrderKnown.this.crossingsList = new VectorList[6];
				try {
					String substrates = ProcessiveOrderKnown.this.substratesField.getText();
					if (ProcessiveOrderKnown.this.solvable(substrates)) {
						substrates = "fplist(" + substrates + ")";
						ProcessiveOrderKnown.this.rList[0] = (VectorList) Parser.evaluate(substrates);
					}

					String products1 = ProcessiveOrderKnown.this.products1Field.getText();
					if (ProcessiveOrderKnown.this.solvable(products1)) {
						products1 = "fplist(" + products1 + ")";
						ProcessiveOrderKnown.this.rList[1] = (VectorList) Parser.evaluate(products1);
					}

					String products2 = ProcessiveOrderKnown.this.products2Field.getText();
					if (ProcessiveOrderKnown.this.solvable(products2)) {
						products2 = "fplist(" + products2 + ")";
						ProcessiveOrderKnown.this.rList[2] = (VectorList) Parser.evaluate(products2);
					}

					String products3 = ProcessiveOrderKnown.this.products3Field.getText();
					if (ProcessiveOrderKnown.this.solvable(products3)) {
						products3 = "fplist(" + products3 + ")";
						ProcessiveOrderKnown.this.rList[3] = (VectorList) Parser.evaluate(products3);
					}

					String products4 = ProcessiveOrderKnown.this.products4Field.getText();
					if (ProcessiveOrderKnown.this.solvable(products4)) {
						products4 = "fplist(" + products4 + ")";
						ProcessiveOrderKnown.this.rList[4] = (VectorList) Parser.evaluate(products4);
					}

					String products5 = ProcessiveOrderKnown.this.products5Field.getText();
					if (ProcessiveOrderKnown.this.solvable(products5)) {
						products5 = "fplist(" + products5 + ")";
						ProcessiveOrderKnown.this.rList[5] = (VectorList) Parser.evaluate(products5);
					}

					String crossings1 = ProcessiveOrderKnown.this.crossings1Field.getText();
					if (!crossings1.trim().equals("")) {
						crossings1 = "list(" + crossings1 + ")";
						ProcessiveOrderKnown.this.crossingsList[0] = (VectorList) Parser.evaluate(crossings1);
					}

					String crossings2 = ProcessiveOrderKnown.this.crossings2Field.getText();
					if (!crossings2.trim().equals("")) {
						crossings2 = "list(" + crossings2 + ")";
						ProcessiveOrderKnown.this.crossingsList[1] = (VectorList) Parser.evaluate(crossings2);
					}

					String crossings3 = ProcessiveOrderKnown.this.crossings3Field.getText();
					if (!crossings3.trim().equals("")) {
						crossings3 = "list(" + crossings3 + ")";
						ProcessiveOrderKnown.this.crossingsList[2] = (VectorList) Parser.evaluate(crossings3);
					}

					String crossings4 = ProcessiveOrderKnown.this.crossings4Field.getText();
					if (!crossings4.trim().equals("")) {
						crossings4 = "list(" + crossings4 + ")";
						ProcessiveOrderKnown.this.crossingsList[3] = (VectorList) Parser.evaluate(crossings4);
					}

					String crossings5 = ProcessiveOrderKnown.this.crossings5Field.getText();
					if (!crossings5.trim().equals("")) {
						crossings5 = "list(" + crossings5 + ")";
						ProcessiveOrderKnown.this.crossingsList[4] = (VectorList) Parser.evaluate(crossings5);
					}

					String crossings6 = ProcessiveOrderKnown.this.crossings6Field.getText();
					if (!crossings6.trim().equals("")) {
						crossings6 = "list(" + crossings6 + ")";
						ProcessiveOrderKnown.this.crossingsList[5] = (VectorList) Parser.evaluate(crossings6);
					}

					ProcessiveOrderKnown.this.rounds = Integer.parseInt(ProcessiveOrderKnown.this.roundsField
							.getText()) + 1;

					final VectorList list = ProcessiveOrderKnown.this.putList(0);

					final VectorList temp = (VectorList) list.clone();
					final InputData inData = new InputData(true, list, ProcessiveOrderKnown.this.rounds);
					final VectorList solutionList = inData.solve();

					temp.setString(VectorList.INPUTLS);
					ProcessiveOrderKnown.this.tangleSolvePanel.getTree().addSolutionList(temp.toString(), solutionList);

					ProcessiveOrderKnown.this.tangleSolvePanel.getDisplay().clearScreen();
					ProcessiveOrderKnown.this.clearTexts();
					ProcessiveOrderKnown.this.tangleSolvePanel.getSelectPane().setVisible(false);
				} catch (final Exception exception) {
					JOptionPane.showMessageDialog(ProcessiveOrderKnown.this.tangleSolvePanel.getFrame(),
							"Invalid input");
				}
			}
		});

		// Layout
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		this.add(this.substratesLabel, constraints);
		this.add(this.substratesField, constraints);
		this.add(this.crossings1Label, constraints);
		this.add(this.crossings1Field, constraints);
		this.add(this.fromTable1Button, constraints);
		
		constraints.gridy = 1;
		this.add(this.products1Label, constraints);
		this.add(this.products1Field, constraints);
		this.add(this.crossings2Label, constraints);
		this.add(this.crossings2Field, constraints);
		this.add(this.fromTable2Button, constraints);
		
		constraints.gridy = 2;
		this.add(this.products2Label, constraints);
		this.add(this.products2Field, constraints);
		this.add(this.crossings3Label, constraints);
		this.add(this.crossings3Field, constraints);
		this.add(this.fromTable3Button, constraints);
		
		constraints.gridy = 3;
		this.add(this.products3Label, constraints);
		this.add(this.products3Field, constraints);
		this.add(this.crossings4Label, constraints);
		this.add(this.crossings4Field, constraints);
		this.add(this.fromTable4Button, constraints);
		
		constraints.gridy = 4;
		this.add(this.products4Label, constraints);
		this.add(this.products4Field, constraints);
		this.add(this.crossings5Label, constraints);
		this.add(this.crossings5Field, constraints);
		this.add(this.fromTable5Button, constraints);
		
		constraints.gridy = 5;
		this.add(this.products5Label, constraints);
		this.add(this.products5Field, constraints);
		this.add(this.crossings6Label, constraints);
		this.add(this.crossings6Field, constraints);
		this.add(this.fromTable6Button, constraints);
		
		constraints.gridy = 6;
		this.add(this.roundsLabel, constraints);
		this.add(this.roundsField, constraints);
		this.add(this.solveButton, constraints);
	}// end constructor

	private String addCommasBetweenEntries(String string1) {
		String string2;
		if (string1.equals("")) { // this is the first entry
			string2 = string1 + ProcessiveOrderKnown.this.tangleSolvePanel.getFourPlatTable().getValue();
		} else { // this is not the first entry
			string2 = string1 + ", " + ProcessiveOrderKnown.this.tangleSolvePanel.getFourPlatTable().getValue();
		}
		return string2;
	}
	
	private VectorList putList(final int i) {
		if ((i <= (ProcessiveOrderKnown.MAX_EQ - 1))
				&& ((this.rList[i] != null) || (this.crossingsList[i] != null))) {
			return new VectorList(new VectorList(this.crossingsList[i], this.rList[i]),
					this.putList(i + 1));
		} else {
			return null;
		}
	}

	private boolean solvable(String s) {
		s = s.trim();
		if (s.equals("")) {
			return false;
		} else {
			return true;
		}
	}

	private void clearTexts() {
		this.substratesField.setText("");
		this.products1Field.setText("");
		this.products2Field.setText("");
		this.products3Field.setText("");
		this.products4Field.setText("");
		this.products5Field.setText("");
		this.crossings1Field.setText("");
		this.crossings2Field.setText("");
		this.crossings3Field.setText("");
		this.crossings4Field.setText("");
		this.crossings5Field.setText("");
		this.crossings6Field.setText("");
		this.rList[0] = null;
		this.rList[1] = null;
		this.rList[2] = null;
		this.rList[3] = null;
		this.rList[4] = null;
		this.rList[5] = null;
		this.crossingsList[0] = null;
		this.crossingsList[1] = null;
		this.crossingsList[2] = null;
		this.crossingsList[3] = null;
		this.crossingsList[4] = null;
		this.crossingsList[5] = null;
	}

	@SuppressWarnings("unused")
	private static VectorList addProdRounds(final VectorList sol, final int round) {
		try {
			VectorList returnValue = null;
			for (VectorList temp = sol; temp != null; temp = temp.tail()) {
				final VectorList set = (VectorList) temp.head();
				final VectorList fourPlatList = (VectorList) set.head();
				final VectorList tsol = set.tail();
				final int fourPlatListLength = fourPlatList.length();
				if (round > fourPlatListLength) {
					for (VectorList t2 = tsol; t2 != null; t2 = t2.tail()) {
						VectorList fpadd = null;
						final VectorList tgset = (VectorList) t2.head();
						final Tangle ofTangle = (Tangle) tgset.head();
						final Tangle obTangle = (Tangle) tgset.getNth(2);
						final Tangle kTangle = (Tangle) tgset.getNth(3);
						if (kTangle.getRational().isInteger()) {
							for (int i = fourPlatListLength; i <= round; i++) {
								final FourPlat nf = new FourPlat(ofTangle,
										obTangle.addTail(kTangle.getRational().getNumerator() * i));
								fpadd = new VectorList(nf, fpadd);
							}
							fpadd = fpadd.reverse();
						}
						fpadd = VectorList.append(fourPlatList, fpadd);
						fpadd = new VectorList(fpadd, new VectorList(tgset, null));
						returnValue = new VectorList(fpadd, returnValue);
					}
				} else {
					returnValue = new VectorList(temp.head(), returnValue);
				}
			}
			return returnValue;
		} catch (final Exception exception) {
			System.out.println(exception + " at addProdRounds");
			return null;
		}
	}

}
