/**
 * @author Yuki Saka
 * Last Updated : August 24, 2013
 * @edited by Crista Moreno
 */
package graphics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.RoundRectangle2D;

import javax.swing.Icon;
import javax.vecmath.Point2d;

import objects.MathFunctions;
import objects.Rational;
import objects.Tangle;
import objects.VectorList;
import util.SimpleColorSource;
import util.TangleColor1ColorSource;
import util.TangleColor2ColorSource;

public class TangleGraph implements Icon {
	private final GeneralPath[] generalPath;

	public Tangle tangle;
	private final Color tangleBorderColor = new Color(255, 255, 255, 80);  // alpha was 80
	private Rectangle mBounds;

	public ColorSource[] colorSources;
	public Color twistBoxColor = new Color(255, 255, 255, 0); // alpha was 90 & was 5 // Color c = new Color(red, green, blue, alpha); alpha is opacity
	private float strokeScale; 

	// constructors
	public TangleGraph(final double twistSpace, final double coilWidth, final double boxStartSize, final int[] co, final Tangle tangle) {
		this(twistSpace, coilWidth, boxStartSize, co, tangle, 1);
	}

	public TangleGraph(final double twistSpace, final double coilWidth, final double boxStartSize, final int[] co, final Tangle tangle, final float strokeScale) {
		this.strokeScale = strokeScale;

		this.colorSources = new ColorSource[3];
		this.colorSources[0] = new TangleColor1ColorSource();
		this.colorSources[1] = new TangleColor2ColorSource(); 
		this.colorSources[2] = new SimpleColorSource() { // TODO Extract class too?
			@Override
			public Color getColor() {
				return TangleGraph.this.twistBoxColor; // TODO make depend on Preferences
			}
		}; 

		this.generalPath = this.tanglePath(twistSpace, coilWidth, boxStartSize, co, tangle);
		this.tangle = tangle;
		final GeneralPath whole = new GeneralPath();
		whole.append(this.generalPath[0], false);
		whole.append(this.generalPath[1], false);
		this.mBounds = whole.getBounds();
	}

	/**
	 * Produces the tangle path.
	 * 
	 * @param twistSpace
	 * @param coilWidth
	 * @param boxWidth
	 * @param co // what is co???
	 * @param tangle
	 * @return
	 */
	public GeneralPath[] tanglePath(final double twistSpace, final double coilWidth, final double boxWidth, final int[] co, 
			final Tangle tangle) {
		double xStart = 0;
		double yStart = 0;
		double horizontalTwistLength = 0; 
		double verticalTwistLength = 0; 

		int i = 1; // keeps track of whether the twist is vertical or horizontal.

		VectorList tangleList = tangle.getVector(); 

		final GeneralPath paths[] = new GeneralPath[3];
		paths[0] = new GeneralPath();
		paths[1] = new GeneralPath();
		paths[2] = new GeneralPath();

		final int[] cn = new int[3];
		GeneralPath[] generalPath;
		// for the odd list case

		final int numberTangleTwists = tangleList.length();
		final int firstTwistNumberCrossings = Math.abs(TangleGraph.getNumberCrossings(tangleList)); 
		// First twist
		// First twist is Horizontal. (Last twist in any case if horizontal.)
		if ((numberTangleTwists % 2) == 1) { // Odd number of tangle twists means the tangles starts and ends with horizontal twist.
			cn[0] = 0;
			cn[1] = 1;
			cn[2] = 1;
			// positive horizontal twist.
			if (TangleGraph.getNumberCrossings(tangleList) > 0) {
				generalPath = TwistBoxUtil.twistBox((float) xStart, (float) yStart, (float) boxWidth, (float) coilWidth, (float) twistSpace,
						firstTwistNumberCrossings, TwistBoxUtil.POSITIVE_HORIZONTAL, this.crossingType(numberTangleTwists, 1, true));
			} else { // negative horizontal twist.
				generalPath = TwistBoxUtil.twistBox((float) xStart, (float) yStart, (float) boxWidth, (float) coilWidth, (float) twistSpace,
						firstTwistNumberCrossings, TwistBoxUtil.NEGATIVE_HORIZONTAL, this.crossingType(numberTangleTwists, 1, false));
			}

			addTwistBox(paths, generalPath);

			this.connect(paths, generalPath, cn, TangleGraph.getNumberCrossings(tangleList), true);
			horizontalTwistLength += TangleGraph.getDistance(twistSpace, coilWidth, firstTwistNumberCrossings);
			verticalTwistLength += boxWidth;
			yStart += boxWidth;
			i++;
			tangleList = tangleList.tail(); // next twist
			// First twist is Vertical.
		} else { // Even number of crossings means tangle starts with vertical twist and ends with horizontal twist.
			cn[0] = 1;
			cn[1] = 1;
			cn[2] = 0;
			// negative vertical twist.
			if (TangleGraph.getNumberCrossings(tangleList) > 0) {
				generalPath = TwistBoxUtil.twistBox((float) xStart, (float) yStart, (float) boxWidth, (float) coilWidth, (float) twistSpace,
						firstTwistNumberCrossings, TwistBoxUtil.NEGATIVE_VERTICAL, this.crossingType(numberTangleTwists, 0, true));
			} else { // positive vertical twist.
				generalPath = TwistBoxUtil.twistBox((float) xStart, (float) yStart, (float) boxWidth, (float) coilWidth, (float) twistSpace,
						firstTwistNumberCrossings, TwistBoxUtil.POSITIVE_VERTICAL, this.crossingType(numberTangleTwists, 0, false));
			}

			addTwistBox(paths, generalPath); 

			this.connect(paths, generalPath, cn, TangleGraph.getNumberCrossings(tangleList), false);
			horizontalTwistLength = boxWidth;
			verticalTwistLength += TangleGraph.getDistance(twistSpace, coilWidth, firstTwistNumberCrossings);
			xStart += boxWidth;
			tangleList = tangleList.tail(); // next twist
		}

		// remaining twists of tangle
		for (; tangleList != null; tangleList = tangleList.tail(), i++) {
			int numberCrossings = Math.abs(TangleGraph.getNumberCrossings(tangleList));
			if ((tangleList.tail() == null) && (TangleGraph.getNumberCrossings(tangleList) == 0)) {
				break;
			}
			if ((i % 2) == 0) { // vertical twist
				// negative vertical twist.
				if (TangleGraph.getNumberCrossings(tangleList) > 0) {
					generalPath = TwistBoxUtil.twistBox((float) xStart, (float) yStart, (float) horizontalTwistLength, (float) coilWidth, (float) twistSpace,
							numberCrossings, TwistBoxUtil.NEGATIVE_VERTICAL, this.crossingType(numberTangleTwists, i, true));
				} else { // positive vertical twist.
					generalPath = TwistBoxUtil.twistBox((float) xStart, (float) yStart, (float) horizontalTwistLength, (float) coilWidth, (float) twistSpace,
							numberCrossings, TwistBoxUtil.POSITIVE_VERTICAL, this.crossingType(numberTangleTwists, i, false));
				}

				addTwistBox(paths, generalPath); 

				this.connect(paths, generalPath, cn, TangleGraph.getNumberCrossings(tangleList), false);
				yStart -= verticalTwistLength;
				verticalTwistLength += TangleGraph.getDistance(twistSpace, coilWidth, numberCrossings);
				xStart += horizontalTwistLength;
			} else { // horizontal twist
				// positive horizontal twist
				if (TangleGraph.getNumberCrossings(tangleList) > 0) {
					generalPath = TwistBoxUtil.twistBox((float) xStart, (float) yStart, (float) verticalTwistLength, (float) coilWidth, (float) twistSpace,
							numberCrossings, TwistBoxUtil.POSITIVE_HORIZONTAL, this.crossingType(numberTangleTwists, i, true));

				} else { // negative horizontal twist
					generalPath = TwistBoxUtil.twistBox((float) xStart, (float) yStart, (float) verticalTwistLength, (float) coilWidth, (float) twistSpace,
							numberCrossings, TwistBoxUtil.NEGATIVE_HORIZONTAL, this.crossingType(numberTangleTwists, i, false));
				}

				addTwistBox(paths, generalPath); 

				this.connect(paths, generalPath, cn, TangleGraph.getNumberCrossings(tangleList), true);
				xStart -= horizontalTwistLength;
				horizontalTwistLength += TangleGraph.getDistance(twistSpace, coilWidth, numberCrossings);
				yStart += verticalTwistLength;
			}
		}
		// Give the corners
		final GeneralPath whole = new GeneralPath();
		whole.append(paths[0], false);
		whole.append(paths[1], false);
		final Rectangle r = whole.getBounds();

		if (tangle.parity().isInfinity()) {
			TwistBoxUtil.makeSmooth(paths[0], 0, co[0], -5, -5, 0, 0);
			TwistBoxUtil.makeSmooth(paths[0], 0, co[3], -5, r.height + 5, 0, r.height);
			TwistBoxUtil.makeSmooth(paths[1], 1, co[1], r.width, 0, r.width + 5, -5);
			TwistBoxUtil.makeSmooth(paths[1], 1, co[2], r.width, r.height, r.width + 5, r.height + 5);
		} else if (tangle.parity().isOne()) {
			TwistBoxUtil.makeSmooth(paths[0], 0, co[0], -5, -5, 0, 0);
			TwistBoxUtil.makeSmooth(paths[1], 0, co[3], -5, r.height + 5, 0, r.height);
			TwistBoxUtil.makeSmooth(paths[1], 1, co[1], r.width, 0, r.width + 5, -5);
			TwistBoxUtil.makeSmooth(paths[0], 1, co[2], r.width, r.height, r.width + 5, r.height + 5);
		} else { // parity is equal to zero
			TwistBoxUtil.makeSmooth(paths[0], 0, co[0], -5, -5, 0, 0);
			TwistBoxUtil.makeSmooth(paths[1], 0, co[3], -5, r.height + 5, 0, r.height);
			TwistBoxUtil.makeSmooth(paths[0], 1, co[1], r.width, 0, r.width + 5, -5);
			TwistBoxUtil.makeSmooth(paths[1], 1, co[2], r.width, r.height, r.width + 5, r.height + 5);
		}
		final AffineTransform affineTransform = new AffineTransform();
		affineTransform.translate(5, 5);
		paths[0].transform(affineTransform);
		paths[1].transform(affineTransform);
		paths[2].transform(affineTransform);
		return paths;
	}

	private void addTwistBox(final GeneralPath[] paths, GeneralPath[] generalPath) {
		final GeneralPath twistBoxPath = new GeneralPath();
		twistBoxPath.append(generalPath[0], false);
		twistBoxPath.append(generalPath[1], false);
		final Rectangle r = twistBoxPath.getBounds();
		paths[2].append(r, false);
	}

	// A little confusing but positive here means that the over crossing is supposed to be
	// drawn from top left to bottom right. It does not mean a positive crossing in general.
	private int[] crossingType(final int numberCrossings, final int i, final boolean positiveCrossing) { // was named ctype????

		final int[] cn1 = { 1, 0, 2, 2 }; // positive twist? 
		final int[] cn2 = { 2, 2, 0, 1 }; // negative twist?

		final int[] cn3 = { 0, 2, 0, 0 }; // negative twist? 
		final int[] cn4 = { 0, 0, 2, 0 }; // positive twist? 

		final int[] cn5 = { 1, 1, 1, 1 }; // length = 1

		final int[] cn6 = { 0, 0, 2, 2 }; // negative twist? length = 2 
		final int[] cn7 = { 2, 2, 0, 0 }; // positive twist? length = 2

		final int[] cn8 = { 0, 1, 1, 0 }; // last crossing (horizontal)

		final int[] cn9 = {0, 2, 1, 0 }; // positive twist? n-1th twist 
		final int[] cn10 = { 0, 1, 2, 0 }; // negative twist? n-1th twist

		// i % 2 vertical twists???
		if (((i == 1) && ((numberCrossings % 2) == 1)) || ((i == 0) && ((numberCrossings % 2) == 0))) {
			if (numberCrossings == 1) {
				return cn5;
			} else if (numberCrossings == 2) {
				if (positiveCrossing) {
					return cn7;
				} else {
					return cn6;
				}
			} else {
				if (positiveCrossing) {
					return cn1;
				} else {
					return cn2;
				}
			}
		} else if (i == numberCrossings) {
			// last horizontal
			return cn8;
		} else if (i == (numberCrossings - 1)) {
			if (positiveCrossing) {
				return cn9;
			} else {
				return cn10;
			}
		} else {
			if (positiveCrossing) {
				return cn4;
			} else {
				return cn3;
			}
		}
	}

	/**
	 * Connects the two paths belonging to separate components in the correct way
	 * @param paths
	 * @param nt
	 * @param cn
	 * @param twist
	 * @param horizontal
	 */
	private void connect(final GeneralPath[] paths, final GeneralPath[] nt,
			final int[] cn, final int twist, final boolean horizontal) {
		if (horizontal) { // horizontal twist
			paths[cn[0]].append(nt[0], false);
			paths[cn[1]].append(nt[1], false);
		} else { // vertical twist
			paths[cn[1]].append(nt[0], false);
			paths[cn[2]].append(nt[1], false);
		}
		if (TangleGraph.isOddTwist(twist)) {
			if (horizontal) {
				TangleGraph.swap(cn, 0, 1);
			} else {
				TangleGraph.swap(cn, 1, 2);
			}
		}
	}

	public void translate(final float x, final float y) {
		final AffineTransform affineTransform = new AffineTransform();
		affineTransform.translate(x, y);
		this.generalPath[0].transform(affineTransform);
		this.generalPath[1].transform(affineTransform);
		this.generalPath[2].transform(affineTransform);
	}

	/**
	 * Draws tangle graph
	 * @param g
	 */
	public void draw(final Graphics g) {
		final Graphics2D graphics2D = (Graphics2D) g;

		// Set stroke (2f is the stroke thickness)
		final BasicStroke twistBoxStroke = new BasicStroke(0f * this.strokeScale, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 0, new float[]{9}, 0);
		final BasicStroke fatStroke = new BasicStroke(0.7f * this.strokeScale, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		// twistBoxStroke was 0.2f* this.strookeScale
		// fatStroke the 0.7f was o.5f on line 322
		/* Draw Tangle paths: */
		graphics2D.setStroke(fatStroke);

		graphics2D.setColor(this.colorSources[0].getColor());
		graphics2D.draw(this.generalPath[0]);
		graphics2D.setColor(this.colorSources[1].getColor());
		graphics2D.draw(this.generalPath[1]);

		/* Draw TwistBox paths: */
		graphics2D.setStroke(twistBoxStroke);
		graphics2D.setColor(this.colorSources[2].getColor());

		graphics2D.draw(this.generalPath[2]);

		/* Draw Border paths: */
		graphics2D.setStroke(fatStroke);
		graphics2D.setColor(this.tangleBorderColor);

		final GeneralPath whole = new GeneralPath();
		whole.append(this.generalPath[0], false);
		whole.append(this.generalPath[1], false);
		this.mBounds = whole.getBounds();


		final Rectangle r = this.mBounds;
		final int width = r.width - 2;
		final int height = r.height - 2;
		
		graphics2D.setStroke(twistBoxStroke);

//		// draws a rectangle for each tangle Of, Ob, and R
		final RoundRectangle2D.Double box = new RoundRectangle2D.Double(r.x + 1, r.y + 1, width, height, 10, 10);
		graphics2D.draw(box);

		// draws an ellipse for each tangle Of, Ob, and R
		final Point2d topLeft = new Point2d(r.x + 1, r.y + 1);
		final Point2d bottomRight = new Point2d(topLeft.x + width, topLeft.y + height);

		final Point2d center = new Point2d((topLeft.x + bottomRight.x) / 2, (topLeft.y + bottomRight.y) / 2);

		final Point2d ellipseResult = MathFunctions.calculateEllipse(center, topLeft, bottomRight);
		final double a = ellipseResult.x; // x-axis-radius of ellipse
		final double b = ellipseResult.y; // y-axis-radius of ellipse

		//graphics2D.draw(new Ellipse2D.Double(center.x - a, center.y - b, 2 * a, 2 * b));
		graphics2D.setStroke(fatStroke);
	}


	@Override
	public void paintIcon(final Component c, final Graphics graphics, final int x,
			final int y) {
		final Graphics2D graphics2D = (Graphics2D) graphics;

		final AffineTransform affineTransform = new AffineTransform();
		affineTransform.translate(x, y);
		this.generalPath[0].transform(affineTransform);
		this.generalPath[1].transform(affineTransform);
		this.generalPath[2].transform(affineTransform);

		graphics2D.setColor(this.colorSources[0].getColor());
		graphics2D.draw(this.generalPath[0]);
		graphics2D.setColor(this.colorSources[1].getColor());
		graphics2D.draw(this.generalPath[1]);
		graphics2D.setColor(this.colorSources[2].getColor());
		graphics2D.draw(this.generalPath[2]);

		graphics2D.setColor(this.tangleBorderColor);
		final GeneralPath whole = new GeneralPath();
		whole.append(this.generalPath[0], false);
		whole.append(this.generalPath[1], false);
		this.mBounds = whole.getBounds();
		final Rectangle r = this.mBounds;
		final RoundRectangle2D.Double box = new RoundRectangle2D.Double(r.x + 1, r.y + 1, r.width - 2, r.height - 2, 10, 10);
		graphics2D.draw(box);
	}

	// ----------------------ACCESSOR METHODS------------------------//
	public Rectangle getBounds() {
		return this.mBounds;
	}

	@Override
	public int getIconWidth() {
		return this.mBounds.width + 5;
	}

	@Override
	public int getIconHeight() {
		return this.mBounds.height + 5;
	}

	public void flipColor() {
		final ColorSource temp = this.colorSources[0];
		this.colorSources[0] = this.colorSources[1];
		this.colorSources[1] = temp;
	}

	private static boolean isOddTwist(final int twist) {
		if (twist == 0) {
			return false;
		} else {
			return (Math.abs(twist) % 2) == 1;
		}
	}

	private static void swap(final int[] cn, final int a, final int b) {
		final int temp = cn[a];
		cn[a] = cn[b];
		cn[b] = temp;
	}

	private static double getDistance(final double twistSpace, final double coilWidth, final int numberTwists) {
		return (twistSpace * 2) + (numberTwists * coilWidth);
	}

	private static int getNumberCrossings(final VectorList list) {
		return (int) ((Rational) list.head()).getNumerator();
	}
}