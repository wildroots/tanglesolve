/**
 * @author Uknown
 * Last Updated : April 13, 2013
 * @edited by Crista Moreno
 */
package graphics;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import objects.InputData;
import objects.Parser;
import objects.VectorList;

public class NonProcessiveInput extends JPanel {

	// constants
	private static final long serialVersionUID = -2787173596595678162L;

	private static String FORMAT = "Input: 4-plat canonical vector(s)/rational number(s) separated by space";
	// private fields
	// labels
	private JLabel substratesLabel;
	private JLabel productsLabel;
	private JLabel crossings1Label;
	private JLabel crossings2Label;
	// text fields
	private JTextField substratesField;
	private JTextField productsField;
	private JTextField crossings1Field;
	private JTextField crossings2Field;
	// buttons
	private JButton fromTable1Button;
	private JButton fromTable2Button;
	private JButton solveButton;
	private JButton sampleInputButton;
	// lists
	private VectorList substrate1 = null;
	private VectorList product1 = null;
	private VectorList substrate2 = null;
	private VectorList product2 = null;
	// panel
	private TangleSolvePanel tangleSolvePanel;

	// constructor
	public NonProcessiveInput(final TangleSolvePanel tangleSolvePanel) {
		super(true);
		this.tangleSolvePanel = tangleSolvePanel;
		this.setLayout(new GridBagLayout());
		this.setOpaque(true);
		this.setBackground(UIManager.getColor("control"));

		// label and input fields for "substrates" and "products"
		this.substratesLabel = new JLabel("Substrate(s): {", SwingConstants.LEFT);
		this.productsLabel = new JLabel("Product(s)  : {", SwingConstants.LEFT);
		this.substratesField = new JTextField(10);
		this.productsField = new JTextField(10);
		
		this.substratesField.setToolTipText(NonProcessiveInput.FORMAT);
		this.productsField.setToolTipText(NonProcessiveInput.FORMAT);

		// label and input fields for crossings
		this.crossings1Label = new JLabel("} / crossing # ", SwingConstants.LEFT);
		this.crossings2Label = new JLabel("} / crossing # ", SwingConstants.LEFT);
		this.crossings1Field = new JTextField(3);
		this.crossings2Field = new JTextField(3);

		this.fromTable1Button = new JButton("From Table");
		this.fromTable2Button = new JButton("From Table");
		this.solveButton = new JButton("Solve");
		this.sampleInputButton = new JButton("Sample Input");

		// action listeners
		this.fromTable1Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				NonProcessiveInput.this.tangleSolvePanel.getFourPlatTable().setVisible(true);
				final String substratesString = NonProcessiveInput.this.substratesField.getText();
				String substrateValue;
				if (substratesString.equals("")) { // is the first substrate
					substrateValue = substratesString + NonProcessiveInput.this.tangleSolvePanel.getFourPlatTable().getValue();
					
				} else { // is not the first substrate
					substrateValue = substratesString + ", " + NonProcessiveInput.this.tangleSolvePanel.getFourPlatTable().getValue();
				}
				NonProcessiveInput.this.substratesField.setText(substrateValue);
				
			}
		});

		this.fromTable2Button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				NonProcessiveInput.this.tangleSolvePanel.getFourPlatTable().setVisible(true);
				final String productString = NonProcessiveInput.this.productsField.getText();
				String productValue;
				if (productString.equals("")) { // is the first product
					productValue = productString + NonProcessiveInput.this.tangleSolvePanel.getFourPlatTable().getValue();
				} else { // is not the first product
					productValue = productString + ", " + NonProcessiveInput.this.tangleSolvePanel.getFourPlatTable().getValue();
				}
				NonProcessiveInput.this.productsField.setText(productValue);
			}
		});

		this.solveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				try {
					String substrates = NonProcessiveInput.this.substratesField.getText();
					if (NonProcessiveInput.this.solvable(substrates)) {
						substrates = "fplist(" + substrates + ")";
						NonProcessiveInput.this.substrate1 = (VectorList) Parser.evaluate(substrates);
					}

					String products = NonProcessiveInput.this.productsField.getText();
					if (NonProcessiveInput.this.solvable(products)) {
						products = "fplist(" + products + ")";
						NonProcessiveInput.this.product1 = (VectorList) Parser.evaluate(products);
					}
					// Numbered 1: when input preceded by "fplist" means is
					// list of knots, not crossings

					String crossings1 = NonProcessiveInput.this.crossings1Field.getText();
					if (!crossings1.trim().equals("")) {
						crossings1 = "list(" + crossings1 + ")";
						NonProcessiveInput.this.substrate2 = (VectorList) Parser.evaluate(crossings1);
					}

					String crossings2 = NonProcessiveInput.this.crossings2Field.getText();
					if (!crossings2.trim().equals("")) {
						crossings2 = "list(" + crossings2 + ")";
						NonProcessiveInput.this.product2 = (VectorList) Parser.evaluate(crossings2);
					} // numbered 2: input preceded with "list" is from
						// "crossings"

					if (((NonProcessiveInput.this.substrate1 != null) || (NonProcessiveInput.this.substrate2 != null))
							&& ((NonProcessiveInput.this.product1 != null) || (NonProcessiveInput.this.product2 != null))) {
						final VectorList list = new VectorList(
								new VectorList(NonProcessiveInput.this.substrate2,
										NonProcessiveInput.this.substrate1), new VectorList(
										new VectorList(NonProcessiveInput.this.product2,
												NonProcessiveInput.this.product1), null));

						final String sg = VectorList.inputListString(list); 
						// keep record of original input list
						// for display purposes
						final InputData inData = new InputData(false, list, 0);

						// get result and give to gPanel
						final VectorList solution = inData.solve();
						NonProcessiveInput.this.tangleSolvePanel.getTree().addSolutionList(sg, solution);

						// clear the screen
						NonProcessiveInput.this.tangleSolvePanel.getDisplay().clearScreen();
						
						// clear text field
						NonProcessiveInput.this.substratesField.setText("");
						NonProcessiveInput.this.productsField.setText("");
						NonProcessiveInput.this.crossings1Field.setText("");
						NonProcessiveInput.this.crossings2Field.setText("");
						NonProcessiveInput.this.substrate1 = null;
						NonProcessiveInput.this.substrate2 = null;
						NonProcessiveInput.this.product1 = null;
						NonProcessiveInput.this.product2 = null;
					}
				} catch (final Exception exception) {
					System.out.println("Exception at nonprocessive panel " + exception);
					JOptionPane.showMessageDialog(NonProcessiveInput.this.tangleSolvePanel.getFrame(), "Invalid input");
				}
			}
		});

		this.sampleInputButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				NonProcessiveInput.this.substratesField.setText("(1,4,1) 2/3 b(3,1)");
				NonProcessiveInput.this.crossings2Field.setText("4,5");
			}
		});

		// Layout
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		this.add(this.sampleInputButton, constraints);
		
		constraints.gridy = 1;
		constraints.gridx = GridBagConstraints.RELATIVE;
		this.add(this.substratesLabel, constraints);
		this.add(this.substratesField, constraints);
		this.add(this.crossings1Label, constraints);
		this.add(this.crossings1Field, constraints);
		this.add(this.fromTable1Button, constraints);
		
		constraints.gridy = 2;
		this.add(this.productsLabel, constraints);
		this.add(this.productsField, constraints);
		this.add(this.crossings2Label, constraints);
		this.add(this.crossings2Field, constraints);
		this.add(this.fromTable2Button, constraints);
		
		constraints.gridy = 3;
		this.add(this.solveButton, constraints);
		
	}// end constructor

	private boolean solvable(String s) {
		s = s.trim();
		if (s.equals("")) {
			return false;
		} else {
			return true;
		}
	}

}
