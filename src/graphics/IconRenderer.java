package graphics;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class IconRenderer extends DefaultTableCellRenderer {

	// constants
	private static final long serialVersionUID = -7993846336299141056L;

	@Override
	public Component getTableCellRendererComponent(final JTable table,
			final Object value, final boolean isSelected,
			final boolean isFocused, final int row, final int column) {
		final Component component = super.getTableCellRendererComponent(table, value, isSelected, isFocused, row, column);
		if ((value != null) && (value instanceof FourPlatGraph)) {
			((JLabel) component).setText(((FourPlatGraph) value).getFourPlat().completeDescription());
			((JLabel) component).setIcon((FourPlatGraph) value);
			((JLabel) component).setVerticalTextPosition(SwingConstants.BOTTOM);
			((JLabel) component).setHorizontalTextPosition(SwingConstants.CENTER);
		} else {
			((JLabel) component).setIcon(null);
			((JLabel) component).setText(value == null ? "" : "Nonfplat");
		}
		return component;
	}
}