package graphics;

import java.awt.Color;

public interface TreeDisplayString {
	
	// constants
	public static final int ROUNDS = 4;
	public static final Color HIGHLIGHT = Color.RED;

	// constants for tree display
	public static final String INITIAL_STRING = "<html> <i><align=left>  Systems are highlighted "
			+ "in <b> <font color=red> red and bold </font> </b> to indicate "
			+ "all <p> solutions must be rational or sum of two rationals. In these <p>cases, "
			+ "list of solutions given by TangleSolve is complete.</i></html>";

	public static final String GRAPH_ALL_RATIONAL = "This system has only Rational Solutions.";
	public static final String GRAPH_NOT_RATIONAL = "This system need not have only Rational Solutions";

	public static final String RATIONAL_SOLUTION_NODE = "All solutions to this system must be Rational. Click to see why";
	public static final String INSTRUCTION = "Select the system in bold font and then click on "
			+ "\"Show Theorems...\" button to see why.";
}