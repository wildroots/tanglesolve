package graphics;

import objects.EquationSystem;

public class ConwayRepNode {
	
	// private fields
	private String textOnNode;
	private String toolTipText;

	// constructor
	public ConwayRepNode(final EquationSystem system) {
		this.textOnNode = system.toConwayString();
		this.toolTipText = system.getTheorem();
	}

	@Override
	public String toString() {
		return this.textOnNode;
	}

	public String getToolTip() {
		return this.toolTipText;
	}
}
