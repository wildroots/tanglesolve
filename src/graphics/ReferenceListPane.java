/**
 * @author Unknown
 * Last Updated : August 24, 2013
 * @edited by Crista Moreno
 */
package graphics;

import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

public class ReferenceListPane extends JDialog {

	// constants
	private static final long serialVersionUID = 92094004947430655L;

	// private fields
	private String referenceList = "<html>"
			+ "<body bgcolor=\"#FFFFFF\"><p><br><br></p><p align=\"center\"><b>References</b></p>"
			+ "<p><br><br></p>"
			+ "<blockquote>"
			+ "<p><i>Disclaimer: This is by no means a comprehensive list of references,<br>"
			+ "but it is a good starting point for the interested reader. Please e-mail<br>"
			+ "Mariel(mariel@math.berkeley.edu) if you would like to see a reference added to this list,<br>"
			+ "or if you would like more information about any of the following references:</i></p>"
			+ "</blockquote>"
			+ "<p><br></p><hr>"
			+

			"<blockquote>"
			+

			"<p><i>[A]</i> Colin C. Adams, The Knot Book, W.H. Freeman and Company, New York, 1994.<br></p>"
			+

			"<p><i>[B97]</i> S. D. Colloms, J. Bath, and D. J. Sherratt, <br>Topological selectivity in Xer site-specific recombination, <br>Cell 88 (1997), 855-864.<br></p>"
			+

			"<p><i>[B99]</i> S. D. Colloms, J. Bath, and D. J. Sherratt, <br>Topology of Xer recombination on catenanes produced by lambda integrase,<br> Journal of Molecular Biology 289 (1999), 873-883.<br></p>"
			+

			"<p><i>[BV]</i> D. Buck and C. V. Marcotte, <br>Tangle Solutions for a Family of DNA-Rearranging Proteins,<br> To Appear in Math. Proc. Camb. Phil. Soc.<br></p>"
			+

			"<p><i>[C03]</i> H. Cabrera Ibarra, <br>On the classification of rational 3-tangles, <br>Journal of Knot Theory and its Ramifications 12 (2003), no.7 921-946.<br></p>"
			+

			"<p><i>[C04]</i> H. Cabrera Ibarra, <br>Results on the classification of rational 3-tangles,<br> Journal of Knot Theory and Its Ramifications 13 (2004), No.2 175-192. <br></p>"
			+

			"<p><i>[Co97]</i> F. Cornet, B. Hallet, and D. J. Sherratt, <br>Xer recombination in Escherichia coli,<br> Journal of Biological Chemistry 270 (1997), no.35 21927-21931.<br></p>"
			+

			"<p><i>[Cr94]</i> N. J. Crisona, R. Kanaar, T. N. Gonzalez, E. L. Zechiedrich, A. Klippel, and N. R. Cozzarelli,<br> Processive recombination by wild-type Gin and an enhancer-independent mutant insight<br> into the mechanisms of recombination selectivity and strand exchange,<br> Journal of Molecular Biology 243 (1994), 437-457.<br></p>"
			+

			"<p><i>[D01]</i> Isabel Darcy, <br>Biological Distances on DNA Knots and Links: Applications to XER recombination<br> <b>Knots in Hellas '98, Vol. 2 (Delphi)</b>, J.Knot Theory Ramifications 10 (2001), no.2 269--294<br></p>"
			+

			"<p><i>[DLV]</i> I. Darcy, J. Luecke and M. Vazquez, <br>The Mu transposome; <br>in preparation <br></p>"
			+

			"<p><i>[E96]</i> Claus Ernst, <br>Tangle Equations, <br>J. of Knot Theory and Its Ramifications 5 (1996), no.2 145-159. <br></p>"
			+

			"<p><i>[E97]</i> Claus Ernst, <br>Tangle Equations II, <br>J. of Knot Theory and Its Ramifications 6 (1997), no.1 1-11. <br></p>"
			+

			"<p><i>[ES90]</i> C.Ernst and D. W. Sumners, <br>A calculus for rational tangles: applications to DNA recombination,<br> Math. Proc. Camb. Phil. Soc. 108 (1990), 489-515<br></p>"
			+

			"<p><i>[ES99]</i> C.Ernst and D. W. Sumners, <br>Solving Tangle Equations arising in a DNA recombination model,<br> Math. Proc. Camb. Phil. Soc. 126 (1999), 23-36.<br></p>"
			+

			"<p><i>[G]</i> Grainge et al, <br>Geometry of site alignment during Int family recombination: antiparallel synapsis by <br>the Flp recombinase, <br>Journal of Molecular Biology 298 (2000), no.5 749-764<br></p>"
			+

			"<p><i>[GK]</i> J.R. Goldman and L. Kauffman, <br>Rational tangles, <br>Advances in Applied Mathematics 18 (1997), 300-332.<br></p>"
			+

			"<p><i>[HS]</i> M. Hirasawa and K. Shimokawa, <br>Dehn Surgeries on strongly invertible knots which yield lens spaces, <br>Proc. Math. Camb. Phil. Soc. 128 (2000), 3445-3451. <br></p>"
			+

			"<p><i>[K90]</i> R. Kanaar, A. Klippel, E. Shekhtman, R. Kahmann, J. M. Dungan and N. R. Cozzarelli, <br>Processive recombination by the phage Mu Gin system: Implications for <br>the mechanisms of DNA strand exchange, DNA site alignment, and enhancer action, <br>Cell 62 (1990), 353-366<br></p>"
			+

			"<p><i>[P]</i> S. Pathania, M. Jayaram and R. Harshey, <br>Path of DNA within the Mu TranspososomeTransposase Interactions <br>Bridging Two Mu Ends and the Enhancer Trap Five DNA Supercoils,<br> Cell 109, Issue 4, Pages 425-436 <br></p>"
			+

			"<p><i>[R]</i> Dale Rolfsen, Knots and links, Publish or Perish Inc., Houston, 1976.<br></p>"
			+

			"<p><i>[S90]</i> De Witt Sumners, <br>Untangling DNA, <br>The Mathematical Intelligencer 12 (1990), 71-80. <br></p>"
			+

			"<p><i>[S95]</i> D. W. Sumners, C. Ernst, N. R. Cozzarelli, and S. J. Spengler, <br>Mathematical analysis of the mechanisms of DNA recombination using tangles, <br>Quarterly Reviews of Biophysics 28 (1995), 253-313.<br></p>"
			+

			"<p><i>[V]</i> Mariel Vazquez, <br>Tangle analysis of site-specific recombination: Gin and Xer systems, <br>PhD Dissertation, Mathematics, Florida State University (2000).<br></p>"
			+

			"<p><i>[VS]</i> M. Vazquez and D. W. Sumners, <br>Tangle Analysis of Gin site-specific recombination, <br>Math. Proc. Camb. Phil. Soc. (2004) 136, 565. <br></p>"
			+

			"<p><i>[V05]</i> M. Vazquez, S. D. Colloms and D. W. Sumners, <br>Tangle Analysis of Xer Recombination Reveals only Three Solutions, all Consistent with <br>a Single Three-dimensional Topological Pathway, <br>J. Mol. Biol. (2005) 346, 493-504. <br></p>"
			+

			"<p><br></p><hr>" +

			"</blockquote></body></html>";

	private JLabel listHolder;

	// constructor
	public ReferenceListPane(final Frame frame, final String title) {
		super(frame, title, false);
		this.setSize(600, 700);
		this.listHolder = new JLabel(this.referenceList);
		this.getContentPane().add(new JScrollPane(this.listHolder));
	}
}
