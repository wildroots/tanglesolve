
package graphics;

import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
/**
 * 
 * @author Yuki Saka
 * @edited by Crista Moreno
 */
public class TwistBoxUtil {

	// constants
	// Types of twists.
	public static final int POSITIVE_HORIZONTAL = 0;
	public static final int NEGATIVE_HORIZONTAL = 1;
	public static final int POSITIVE_VERTICAL = 2;
	public static final int NEGATIVE_VERTICAL = 3;
	
	// corner types
	// private static final int TYPE1 = 1; TYPE1 is not used.
	// private static final int TYPE2 = 2; TYPE2 is not used.
	// private static final int TYPE3 = 3; TYPE3 is not used.

	// Returns a coilshape xd, 
	// yd determines the origin, 
	// wd the vertical width of the box divided by 2,
	// twd determines the width and height of a coil. 
	// dhd determines the distance from the edge to the start of a coil.
	/**
	 * Returns the twist path.
	 * 
	 * @param boxWidth
	 * @param coilWidth
	 * @param twistSpace
	 * @param numberCrossings
	 * @param twistType
	 * @param cn
	 * @return
	 */
	public static GeneralPath[] twistBox(final float boxWidth, final float coilWidth, final float twistSpace,
			final int numberCrossings, final int twistType, final int[] cn) {
		return TwistBoxUtil.twistBox(0, 0, boxWidth, coilWidth, twistSpace, numberCrossings, twistType, cn);
	}

	public static GeneralPath[] twistBox(final float x, final float y, final float boxWidth, final float coilWidth, 
			final float twistSpace, final int numberCrossings, final int twistType, final int[] cn) {
		
		final float space = coilWidth / 4.0f;
		final float halfBoxWidth = boxWidth / 2.0f;
		final float dv = halfBoxWidth - (coilWidth / 2.0f);
		final float twistLength = numberCrossings * coilWidth;

		final GeneralPath strand1Path = new GeneralPath(); 
		final GeneralPath strand2Path = new GeneralPath();

		// Lines must be drawn with respect to smooth type
		TwistBoxUtil.makeSmooth(strand1Path, 0, cn[0], 0, -halfBoxWidth, twistSpace, -halfBoxWidth + dv);
		TwistBoxUtil.makeSmooth(strand2Path, 0, cn[3], 0, halfBoxWidth, twistSpace, halfBoxWidth - dv);

		float xst = twistSpace;
		float yst = -halfBoxWidth + dv;
		final float sm = space / 2.0f;
		
		// strand 1 path
		for (int i = 1; i <= numberCrossings; i++, xst += coilWidth) {
			if ((i % 2) == 0) {
				strand1Path.quadTo(xst + sm, yst, (xst + (coilWidth / 2.0f)) - space, (yst - (coilWidth / 2.0f)) + space);
				strand1Path.moveTo(xst + (coilWidth / 2.0f) + space, yst - (coilWidth / 2.0f) - space);
				// This is the last twist.
				if (i == numberCrossings) {
					strand1Path.lineTo(xst + coilWidth, yst - coilWidth);
				} else {
					strand1Path.quadTo((xst + coilWidth) - sm, yst - coilWidth, xst + coilWidth, yst - coilWidth);
				}
				yst -= coilWidth;
			} else {
				// This is the first twist.
				if (i == 1) { 
					strand1Path.lineTo(xst + (coilWidth / 2.0f), yst + (coilWidth / 2.0f));
				} else {
					strand1Path.quadTo(xst + sm, yst, xst + (coilWidth / 2.0f), yst + (coilWidth / 2.0f));
				}
				// This is the last twist.
				if (i == numberCrossings) {
					strand1Path.lineTo(xst + coilWidth, yst + coilWidth);
				} else {
					strand1Path.quadTo((xst + coilWidth) - sm, yst + coilWidth, xst + coilWidth, yst + coilWidth);
				}
				yst += coilWidth;
			}
		}
		// Number of crossings is even.
		if ((numberCrossings % 2) == 0) { 
			TwistBoxUtil.makeSmooth(strand1Path, 1, cn[1], twistLength + twistSpace, -halfBoxWidth + dv, twistLength + (2 * twistSpace), -halfBoxWidth);
		// Number of crossings is odd.
		} else {
			TwistBoxUtil.makeSmooth(strand1Path, 1, cn[2], twistLength + twistSpace, halfBoxWidth - dv, twistLength + (2 * twistSpace), halfBoxWidth);
		}

		xst = twistSpace;
		yst = halfBoxWidth - dv;
		
		// strand 2 path
		for (int i = 1; i <= numberCrossings; i++, xst += coilWidth) {
			if ((i % 2) == 1) {
				if (i == 1) {
					strand2Path.lineTo((xst + (coilWidth / 2.0f)) - space, (yst - (coilWidth / 2.0f)) + space);
				} else {
					strand2Path.quadTo(xst + sm, yst, (xst + (coilWidth / 2.0f)) - space, (yst - (coilWidth / 2.0f)) + space);
				}
				strand2Path.moveTo(xst + (coilWidth / 2.0f) + space, yst - (coilWidth / 2.0f) - space);
				if (i == numberCrossings) {
					strand2Path.lineTo(xst + coilWidth, yst - coilWidth);
				} else {
					strand2Path.quadTo((xst + coilWidth) - sm, yst - coilWidth, xst + coilWidth, yst - coilWidth);
				}
				yst -= coilWidth;
			} else {
				strand2Path.quadTo(xst + sm, yst, xst + (coilWidth / 2.0f), yst + (coilWidth / 2.0f));
				if (i == numberCrossings) {
					strand2Path.lineTo(xst + coilWidth, yst + coilWidth);
				} else {
					strand2Path.quadTo((xst + coilWidth) - sm, yst + coilWidth, xst + coilWidth, yst + coilWidth);
				}
				yst += coilWidth;
			}
		}
		// Number of crossings is even.
		if ((numberCrossings % 2) == 0) { 
			TwistBoxUtil.makeSmooth(strand2Path, 1, cn[2], twistLength + twistSpace, halfBoxWidth - dv, twistLength + (2 * twistSpace), halfBoxWidth);
		// Number of crossings is odd.
		} else { 
			TwistBoxUtil.makeSmooth(strand2Path, 1, cn[1], twistLength + twistSpace, -halfBoxWidth + dv, twistLength + (2 * twistSpace), -halfBoxWidth);
		}

		final AffineTransform affineTransform = new AffineTransform();
		affineTransform.translate(x, y);

		final GeneralPath[] twistPath = new GeneralPath[2];
		if (twistType == TwistBoxUtil.POSITIVE_HORIZONTAL) {
			affineTransform.translate(0, halfBoxWidth);
			strand1Path.transform(affineTransform);
			strand2Path.transform(affineTransform);
			twistPath[0] = strand1Path;
			twistPath[1] = strand2Path;
		} else if (twistType == TwistBoxUtil.NEGATIVE_HORIZONTAL) {
			affineTransform.translate(0, halfBoxWidth);
			final AffineTransform mirror = new AffineTransform(1, 0, 0, -1, 0, 0);
			affineTransform.concatenate(mirror);
			strand1Path.transform(affineTransform);
			strand2Path.transform(affineTransform);
			twistPath[0] = strand2Path;
			twistPath[1] = strand1Path;
		} else if (twistType == TwistBoxUtil.POSITIVE_VERTICAL) {
			affineTransform.translate(halfBoxWidth, 0);
			affineTransform.rotate(Math.PI / 2.0d);
			strand1Path.transform(affineTransform);
			strand2Path.transform(affineTransform);
			twistPath[0] = strand1Path;
			twistPath[1] = strand2Path;
		} else { // NEGATIVE_VERTICAL case
			affineTransform.translate(halfBoxWidth, 0);
			affineTransform.rotate(Math.PI / 2.0d);
			final AffineTransform mirror = new AffineTransform(1, 0, 0, -1, 0, 0);
			affineTransform.concatenate(mirror);
			strand1Path.transform(affineTransform);
			strand2Path.transform(affineTransform);
			twistPath[0] = strand2Path;
			twistPath[1] = strand1Path;
		}
		return twistPath;
	}

	private static float[] midPoint(final float x1, final float y1, final float x2, final float y2) {
		final float[] midPoint = new float[2];
		midPoint[0] = (x2 + x1) / 2.0f;
		midPoint[1] = (y2 + y1) / 2.0f;
		return midPoint;
	}

	/**
	 * 
	 * @param f1
	 * @param f2
	 * @return minimum of f1 and f2.
	 */
	private static float min(final float f1, final float f2) {
		if (f1 > f2) {
			return f2;
		} else {
			return f1;
		}
	}

	public static void makeSmooth(final GeneralPath path, final int type1,
			final int type2, final float fpx, final float fpy, final float spx,final float spy) {
		
		final float round = TwistBoxUtil.min(Math.abs(spx - fpx) / 4.0f, Math.abs(spy - fpy) / 4.0f);
		final float sign = Math.abs(spy - fpy) / (spy - fpy);
		
		float ipy;
		float ipx;
		float jpy;
		float jpx;
		
		if (type1 == 0) {
			// smooth out
			jpy = spy - (sign * round);
			jpx = spx - round;
			if (type2 == 0) {
				ipx = fpx + round;
				ipy = fpy;
			} else if (type2 == 1) {
				ipx = fpx + round;
				ipy = fpy + (sign * round);
			} else {
				ipx = fpx;
				ipy = fpy + (sign * round);
			}
		} else {
			ipy = fpy + (sign * round);
			ipx = fpx + round;
			if (type2 == 0) {
				jpx = spx - round;
				jpy = spy;
			} else if (type2 == 1) {
				jpx = spx - round;
				jpy = spy - (sign * round);
			} else {
				jpx = spx;
				jpy = spy - (sign * round);
			}
		}
		final float[] midPoint = TwistBoxUtil.midPoint(ipx, ipy, jpx, jpy);
		path.moveTo(fpx, fpy);
		path.quadTo(ipx, ipy, midPoint[0], midPoint[1]);
		path.quadTo(jpx, jpy, spx, spy);
	}
}