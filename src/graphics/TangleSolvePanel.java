/**
 * @author Unknown
 * Last Updated : August 24, 2013
 * @edited by Crista Moreno
 */

package graphics;



import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;

import objects.FourPlat;
import objects.Parser;

public class TangleSolvePanel extends JPanel implements ActionListener {

	// constants
	private static final long serialVersionUID = -2493901804479570297L;
	
	// private fields
	private PreferencesFrame preferencesFrame;
	
	// text
	private JTextField textField;
	private JTextArea textArea;
	private DisplayPanel display; // pane of displaying result
	// panes
	private JTabbedPane tabbedPane; // tabbed pane format on inputPane
	private TabbedDialog inputPane; // input pane
	private ReferenceListPane referencePane;
	// tree
	private TreeControl tree;
	// buttons
	private JButton hideInputPanelButton;
	private JButton referenceButton;
	
	private SelectDialog selectPane; // button to choose processive order
	private FourPlatTable fourPlatTable;
	private JFrame frame;
	
	// constructor
	public TangleSolvePanel(final JFrame frame, final boolean image) {
		super(true);
		this.frame = frame;

		// Menu
		final JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		final JMenu editMenu = new JMenu("Edit");
		menuBar.add(editMenu);

		final JMenuItem clearItem = new JMenuItem("Clear All");
		final JMenuItem deleteItem = new JMenuItem("Clear Selected");
		final JMenuItem preferencesItem = new JMenuItem("Preferences");
		
		editMenu.add(clearItem);
		editMenu.add(deleteItem);
		editMenu.add(preferencesItem);

		clearItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				TangleSolvePanel.this.tree.clearAll();
			}
		});
		
		deleteItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				TangleSolvePanel.this.tree.clearSelected();
			}
		});
		
		preferencesItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				TangleSolvePanel.this.showPreferencesFrame();
			}
		});
		
		// Initialization of four plat table and images
		FourPlat.loadFourPlats(10);
		if (image) {
			FourPlatGraph.loadImages();
		}

		// Knot / Link table
		this.fourPlatTable = new FourPlatTable(frame, "Knot/link table");
		this.setLayout(new GridBagLayout());

		// Tabbed pane
		this.tabbedPane = new JTabbedPane(SwingConstants.TOP);
		this.tabbedPane.addTab("Tangle Diagram", new TangleDiagramInput(this));
		this.tabbedPane.addTab("Non Processive", new NonProcessiveInput(this));
		this.tabbedPane.addTab("Processive", new ProcessiveInput(this));
		
		// Input Pane
		this.inputPane = new TabbedDialog(frame, this, this.tabbedPane, "Input Pane");
		this.selectPane = new SelectDialog(frame, this, new ProcessiveOrderKnown(this),
				"Processive Order Known");

		// Display Panel
		this.display = new DisplayPanel(frame);

		// Tree Control
		this.tree = new TreeControl(frame);
		this.tree.putClientProperty("JTree.lineStyle", "Angled");
		this.tree.collapseRow(0);
		this.tree.addTreeSelectionListener(this.display);

		ToolTipManager.sharedInstance().registerComponent(this.tree);

		// show button
		this.hideInputPanelButton = new JButton("Hide Input Panel");
		this.hideInputPanelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				if (TangleSolvePanel.this.inputPane.isShowing()) {
					TangleSolvePanel.this.inputPane.setVisible(false);
					TangleSolvePanel.this.hideInputPanelButton.setText("Show Input Panel");
				} else {
					TangleSolvePanel.this.inputPane.setVisible(true);
					TangleSolvePanel.this.hideInputPanelButton.setText("Hide Input Panel");
				}
			}
		});

		this.referencePane = new ReferenceListPane(frame, "List of All References");

		this.referenceButton = new JButton("Show list of all References");
		this.referenceButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				if (TangleSolvePanel.this.referencePane.isShowing()) {
					TangleSolvePanel.this.referencePane.setVisible(false);
					TangleSolvePanel.this.referenceButton.setText("Show list of all References");
				} else {
					TangleSolvePanel.this.referencePane.setVisible(true);
					TangleSolvePanel.this.referenceButton.setText("Hide list of all References");
				}
			}
		}// end of constructor of ActionListener
				);

		// Command Pane is not visible yet it might be useful for whatever????
//		this.textField = new JTextField(10);
//		this.textField.addActionListener(this);
//
//		this.textArea = new JTextArea(10, 200);
//		this.textArea.setEditable(false);
//		this.textArea.setLineWrap(true);
		
		final GridBagConstraints constraints = new GridBagConstraints();

		// Layout of Components on GPanel

		// Display/Edit Panel
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.gridwidth = 2;
		constraints.gridheight = 1;

		final JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		JScrollPane treeScrollPane = new JScrollPane(this.tree);
		
		this.display.setPreferredSize(new Dimension(800, 400));
		treeScrollPane.setPreferredSize(new Dimension(525, 400));

		splitPane.setTopComponent(this.display);
		splitPane.setBottomComponent(treeScrollPane);
		//splitPane.setPreferredSize(new Dimension(1300, 400));

		this.add(splitPane, constraints);

		// Hide/Show buttons
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		this.add(this.hideInputPanelButton, constraints);
		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		this.add(this.referenceButton, constraints);
		Parser.initialize();
	}

	protected void showPreferencesFrame() {
		if (this.preferencesFrame == null) {
			this.preferencesFrame = new PreferencesFrame();
		}

		this.preferencesFrame.setVisible(true);
	}

	// ----------------------ACCESSOR METHODS------------------------//
	public SelectDialog getSelectPane() {
		return selectPane;
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
	public FourPlatTable getFourPlatTable() {
		return fourPlatTable;
	}
	
	public TreeControl getTree() {
		return tree;
	}

	public DisplayPanel getDisplay() {
		return display;
	}

	@Override
	public void actionPerformed(final ActionEvent event) {
		try {
			final String text = this.textField.getText();
			final Object eval = Parser.evaluate(text);
			this.textArea.append(eval + "\n");
			this.textField.selectAll();
			this.textArea.selectAll();
		} catch (final Exception exception) {
			System.out.println(exception);
		}
	}
}
