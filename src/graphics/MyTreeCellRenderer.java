/**
 * <p>Title: MyTreeCellRenderer</p>
 *
 * <p>Description: Subclass of DefaultTreeCellRenderer. Customize each entry of
 * tree to vary based on whether has theorem applicable</p>
 *
 * @author Wenjing Zheng
 * @edited Crista Moreno
 * @version 1.0
 */

package graphics;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import objects.EquationSystem;

public class MyTreeCellRenderer extends DefaultTreeCellRenderer {

	// constants
	private static final long serialVersionUID = -8189414158934608210L;
	
	// private fields
	private Icon openIcon;
	private Icon closedIcon;
	private Icon leafIcon;

	// constructor
	public MyTreeCellRenderer(final Icon openIcon, final Icon closedIcon, final Icon leafIcon) {
		this.openIcon = openIcon;
		this.closedIcon = closedIcon;
		this.leafIcon = leafIcon;
		this.setLeafIcon(this.leafIcon);
		this.setOpenIcon(this.openIcon);
		this.setClosedIcon(this.closedIcon);
	}

	@Override
	public Component getTreeCellRendererComponent(final JTree tree,
			final Object value, final boolean sel, final boolean expanded,
			final boolean leaf, final int row, final boolean hasFocus) {
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
		if (this.systemWTheorem(value)) {
			/*
			 * setOpenIcon(openIcon); setClosedIcon(closedIcon);
			 * setTextNonSelectionColor(Color.RED);
			 * setTextSelectionColor(Color.RED);
			 */
			this.setToolTipText("All solutions to this system are rational");
			// setFont(new Font(this.getFont().getName(), Font.BOLD,
			// this.getFont().getSize()));
		} else {
			/*
			 * setOpenIcon(null); setClosedIcon(null);
			 * setTextNonSelectionColor(Color.BLACK);
			 * setTextSelectionColor(Color.BLACK);
			 */
			this.setToolTipText(null); // no tool tip
			// setFont(new Font(this.getFont().getName(), Font.PLAIN,
			// this.getFont().getSize()));
		}
		return this;
	}

	protected boolean systemWTheorem(final Object value) {

		final DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		final Object userObject = node.getUserObject();

		if (userObject instanceof EquationSystem) {
			final EquationSystem system = (EquationSystem) userObject;
			if (system.hasTheorem()) {
				return true;
			}
			return false;
		}
		return false;
	}
}