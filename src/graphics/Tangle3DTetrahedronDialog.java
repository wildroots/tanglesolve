package graphics;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.media.j3d.Background;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.ImageComponent;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.Node;
import javax.media.j3d.Screen3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.View;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Color3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import objects.Tangle;
import objects.Tangle3D;
import objects.Tangle3DUtils;

import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.j3d.utils.universe.ViewingPlatform;

public class Tangle3DTetrahedronDialog extends JDialog implements WindowListener {
	private static final Vector3f NUMERATOR_BOTTOM_OFFSET = new Vector3f(0, 0, 0.3f);
	private static final Vector3f NUMERATOR_TOP_OFFSET = new Vector3f(0, 0, -0.3f);

	public static final float SCALE = 0.3f;
	public static final float SITE_SCALE = 0.1f; // Scale of the recombination site.
	private static final int CROSSING_RESOLUTION = 100;
	private static final float CROSSING_SIZE = 10;
	private static final float CROSSING_HELIX_RADIUS = 0.1f;

	public static enum Options {
		TANGLE_OF(1, true, false, false, false, new Vector3f (0, 0, 0), null, null),
		TANGLE_OB(1, false, true, false, false, null, new Vector3f (0, 0, 0), null),
		TANGLE_R(1, false, false, true, false, null, null, new Vector3f (0, 0, 0)),
		TANGLES_OF_OB(2, true, true, false, false, new Vector3f (-0.4f, 0f, 0f), new Vector3f (0.4f, 0f, 0f), null),
		TANGLES_OB_R(2, false, true, true, false, null, new Vector3f (-0.4f, 0f, 0f), new Vector3f (0.4f, 0f, 0f)),
		TANGLES_OF_OB_R(3, true, true, true, true, new Vector3f (-0.8f, 0f, 0f), new Vector3f (0f, 0f, 0f), new Vector3f (0.8f, 0f, 0f));

		private int count;
		private boolean showOf;
		private boolean showOb;
		private boolean showR;
		private Vector3f shiftTangleOf;
		private Vector3f shiftTangleOb;
		private Vector3f shiftTangleR;
		private boolean showNumerator;

		private Options(int count, boolean showOf, boolean showOb, boolean showR, boolean showNumerator, Vector3f shiftTangleOf, Vector3f shiftTangleOb, Vector3f shiftTangleR) {
			this.count = count;
			this.showOf = showOf;
			this.showOb = showOb;
			this.showR = showR;
			this.showNumerator = showNumerator;
			this.shiftTangleOf = shiftTangleOf;
			this.shiftTangleOb = shiftTangleOb;
			this.shiftTangleR = shiftTangleR;
		}

		public int getCount() {
			return count;
		}

		public boolean isShowOf() {
			return showOf;
		}

		public boolean isShowOb() {
			return showOb;
		}

		public boolean isShowR() {
			return showR;
		}

		public boolean isShowNumerator() {
			return showNumerator;
		}

		public Vector3f getShiftTangleOf() {
			return shiftTangleOf;
		}

		public Vector3f getShiftTangleOb() {
			return shiftTangleOb;
		}

		public Vector3f getShiftTangleR() {
			return shiftTangleR;
		}
	}

	private static final long serialVersionUID = -8991936497915022071L;

	private static final Rotation[] ROTATIONS = new Rotation[] {
		
		// Have to compose true rotation with rotation (134) to get standard view matched with identity
//		new Rotation("e", new int[] {4,2,1,3}),
//		new Rotation("(14)(23)", new int[] {3,1,2,4}),
//		new Rotation("(124)", new int[] {3,4,1,2}),
//		new Rotation("(134)", new int[] {3,2,4,1}),
//		new Rotation("(132)", new int[] {2,1,4,3}),
//		new Rotation("(142)", new int[] {2,3,1,4}),
//		new Rotation("(12)(34)", new int[] {2,4,3,1}),
//		new Rotation("(243)", new int[] {4,1,3,2}),
//		new Rotation("(123)", new int[] {1,4,2,3}),
//		new Rotation("(13)(24)", new int[] {1,3,4,2}),
//		new Rotation("(143)", new int[] {1,2,3,4}),
//		new Rotation("(234)", new int[] {4,3,2,1})
		
		new Rotation("e", new int[] {1,2,3,4}),
		new Rotation("(14)(23)", new int[] {4,3,2,1}),
		new Rotation("(124)", new int[] {4,1,3,2}),
		new Rotation("(134)", new int[] {4,2,1,3}),
		new Rotation("(132)", new int[] {2,3,1,4}),
		new Rotation("(142)", new int[] {2,4,3,1}),
		new Rotation("(12)(34)", new int[] {2,1,4,3}),
		new Rotation("(243)", new int[] {1,3,4,2}),
		new Rotation("(123)", new int[] {3,1,2,4}),
		new Rotation("(13)(24)", new int[] {3,4,1,2}),
		new Rotation("(143)", new int[] {3,2,4,1}),
		new Rotation("(234)", new int[] {1,4,2,3})
		
	};

	private static final float TETRAHEDRON_LINE_THICKNESS = 0.005f; // was 0.0005

	private static final float TETRAHEDRON_CORNER_RADIUS = 0.03f;

	private static final float NUMERATOR_LINE_THICKNESS = 0.015f;

	private static final double EYE_DISTANCE_SCALE = 4.25; // was 5.25 for all three tangles and 2.25 for single tangle

	private static final int SCREENSHOT_UPSCALE = 1;
	private static final float AXIS_LENGTH = 1.1f;

	private TransformGroup rootTransformationGroup;

	private final SimpleUniverse universe;

	private final Canvas3D onScreenCanvas3D;
	private final Canvas3D offScreenCanvas3D;

	// constructor for 3D view of three tangles embedded inside of tetrahedra
	public Tangle3DTetrahedronDialog(final JFrame frame, final EquationGraph equationGraph, Options options) {
		super(frame, "Tangle Of: " + equationGraph.getOfTangleGraph().tangle.toString() + ", Tangle Ob: " + equationGraph.getObTangleGraph().tangle.toString() + ", Tangle R:" + equationGraph.getRTangleGraph()[0].tangle.toString(), false);

		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.addWindowListener(this);

		final GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();
		this.onScreenCanvas3D = new Canvas3D(config);
		this.offScreenCanvas3D = new Canvas3D(config, true);

		this.setLayout(new BorderLayout());
		this.add(BorderLayout.CENTER, this.onScreenCanvas3D);

		this.universe = new SimpleUniverse(this.onScreenCanvas3D);

		final View view = this.universe.getViewer().getView();
		view.addCanvas3D(offScreenCanvas3D);

		this.init3D(equationGraph, options);

		final JPanel rotationButtonsPanel = new JPanel();
		rotationButtonsPanel.setLayout(new BoxLayout(rotationButtonsPanel, BoxLayout.Y_AXIS));
		this.add(BorderLayout.EAST, rotationButtonsPanel);

		// Rotation Buttons:
		for (final Rotation rotation : Tangle3DTetrahedronDialog.ROTATIONS) {
			rotationButtonsPanel.add(this.createRotationButton(rotation));
		}

		// Screenshots buttons:
		final JButton screenshotsButton = new JButton("Screenshots");
		screenshotsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				final JFileChooser screenshotDirectoryChooser = new JFileChooser();
				screenshotDirectoryChooser.setMultiSelectionEnabled(false);
				screenshotDirectoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

				final int result = screenshotDirectoryChooser.showSaveDialog(null);

				if (result == JFileChooser.APPROVE_OPTION) {
					final File screenshotDirectory = screenshotDirectoryChooser.getSelectedFile();
					if (!screenshotDirectory.exists()) {
						screenshotDirectory.mkdirs();
					}

					for (final Rotation rotation : Tangle3DTetrahedronDialog.ROTATIONS) {
						Tangle3DTetrahedronDialog.this.resetCameraToRotation(rotation);

						final BufferedImage screenshot = takeScreenshot();
						final File screenshotFile = new File(screenshotDirectory, rotation.name + ".png");
						try {
							ImageIO.write(screenshot, "PNG", screenshotFile);
						} catch (final IOException e) {
							e.printStackTrace();
						}

						screenshot.flush();
					}
				}
			}
		});
		rotationButtonsPanel.add(screenshotsButton);
	}

	private void init3D(final EquationGraph equationGraph, Options options) {
		Vector3f shiftTangleOf = options.getShiftTangleOf();
		Vector3f shiftTangleOb = options.getShiftTangleOb();
		Vector3f shiftTangleR = options.getShiftTangleR();

		final Tangle tangleOf = equationGraph.getOfTangleGraph().tangle;
		final Tangle tangleOb = equationGraph.getObTangleGraph().tangle;
		final Tangle tangleR = equationGraph.getRTangleGraph()[0].tangle;

		final BranchGroup branchGroup = new BranchGroup();

		this.rootTransformationGroup = new TransformGroup();
		this.rootTransformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		this.rootTransformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		
		// Crista: Comment out this code to see if center white sphere can be removed
		//final TransformGroup tgOrigin = this.createTranslationTransformationGroup(0, 0, 0, new AxisAngle4d(0, 0, 0, 0), new Sphere(0.025f));
		final TransformGroup tgOrigin = this.createTranslationTransformationGroup(0, 0, 0, new AxisAngle4d(0, 0, 0, 0));

		this.rootTransformationGroup.addChild(tgOrigin);

		addLights();
		addBackground();

		/* Coordinate System */
		{
			final TransformGroup transformationGroupAxes = new TransformGroup();
			final Node xAxis = Tangle3DUtils.createArrowNode(new Point3f(0, 0, 0),  new Point3f(AXIS_LENGTH, 0, 0),  new Color3f(1, 0, 0), 0.01f); // X-Axis
			final Node yAxis = Tangle3DUtils.createArrowNode(new Point3f(0, 0, 0),  new Point3f(0, AXIS_LENGTH, 0),  new Color3f(0, 1, 0), 0.01f); // Y-Axis
			final Node zAxis = Tangle3DUtils.createArrowNode(new Point3f(0, 0, 0),  new Point3f(0, 0, AXIS_LENGTH),  new Color3f(0, 0, 1), 0.01f); // Z-Axis

			
			transformationGroupAxes.addChild(xAxis);
			transformationGroupAxes.addChild(yAxis);
			transformationGroupAxes.addChild(zAxis);

			tgOrigin.addChild(transformationGroupAxes);
		}
		
		/* Grid */
		{
			/* Lines in X direction: */
			final int LINES_PER_UNIT = 5;
			for (int i = -1 * LINES_PER_UNIT; i <= 1 * LINES_PER_UNIT; i++) {
				float progress = ((float)i) / LINES_PER_UNIT;
				tgOrigin.addChild(Tangle3DUtils.createLineNode(new Point3f(-1, progress, 0), new Point3f(1, progress, 0), new Color3f(0.25f, 0.25f, 0.25f), 0.002f));
				tgOrigin.addChild(Tangle3DUtils.createLineNode(new Point3f(progress, -1, 0), new Point3f(progress, 1, 0), new Color3f(0.25f, 0.25f, 0.25f), 0.002f));
			}
		}

		/* Tangle OF */
		Transform3D groupTransformOf = null;
		if (options.isShowOf()) {
			final TransformGroup transformationGroupOf = new TransformGroup();
			transformationGroupOf.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
			transformationGroupOf.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
			addTetrahedron(transformationGroupOf, SCALE);

			groupTransformOf = new Transform3D();
			groupTransformOf.setTranslation(shiftTangleOf);
			transformationGroupOf.setTransform(groupTransformOf);

			final Node tangleNode = new Tangle3D(tangleOf, equationGraph.getOfTangleGraph().colorSources[0].getColor3f(), equationGraph.getOfTangleGraph().colorSources[1].getColor3f(), CROSSING_RESOLUTION, CROSSING_SIZE, CROSSING_HELIX_RADIUS, SCALE);
			transformationGroupOf.addChild(tangleNode);

			tgOrigin.addChild(transformationGroupOf);
		}

		/* Tangle OB */
		Transform3D groupTransformOb = null;
		if (options.isShowOb()) {
			final TransformGroup transformationGroupOb = new TransformGroup();
			transformationGroupOb.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
			transformationGroupOb.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
			addTetrahedron(transformationGroupOb, SCALE);

			groupTransformOb = new Transform3D();
			groupTransformOb.setTranslation(shiftTangleOb);
			transformationGroupOb.setTransform(groupTransformOb);

			final Node tangleObNode = new Tangle3D(tangleOb, equationGraph.getObTangleGraph().colorSources[0].getColor3f(), equationGraph.getObTangleGraph().colorSources[1].getColor3f(), CROSSING_RESOLUTION, CROSSING_SIZE, CROSSING_HELIX_RADIUS, SCALE);
			transformationGroupOb.addChild(tangleObNode);

			tgOrigin.addChild(transformationGroupOb);
		}

		/* Tangle R */
		Transform3D groupTransformR = null;
		if (options.isShowR()) {
			final TransformGroup transformationGroupR = new TransformGroup();
			transformationGroupR.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
			transformationGroupR.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
			addTetrahedron(transformationGroupR, SITE_SCALE);

			groupTransformR = new Transform3D();
			groupTransformR.setTranslation(shiftTangleR);
			transformationGroupR.setTransform(groupTransformR);

			final Node tangleRNode = new Tangle3D(tangleR, equationGraph.getRTangleGraph()[0].colorSources[0].getColor3f(), equationGraph.getRTangleGraph()[0].colorSources[1].getColor3f(), CROSSING_RESOLUTION, CROSSING_SIZE, CROSSING_HELIX_RADIUS, SITE_SCALE);
			transformationGroupR.addChild(tangleRNode);

			tgOrigin.addChild(transformationGroupR);
		}

		/* Connections between Tangles: */
		if (options.isShowOf() && options.isShowOb()) {
			// top connection strand tangle Of to tangle Ob
			final Point3f tangleOfCornerD = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_D);
			tangleOfCornerD.scale(SCALE);
			final Point3f tangleObCornerA = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_A);
			tangleObCornerA.scale(SCALE);
			connectionStrand(tgOrigin, groupTransformOb, groupTransformOf, tangleObCornerA, tangleOfCornerD, equationGraph.getOfToObTopColor().getColor3f());

			// bottom connection strand tangle Of to tangle Ob
			final Point3f tangleOfCornerC = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_C);
			tangleOfCornerC.scale(SCALE);
			final Point3f tangleObCornerB = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_B);
			tangleObCornerB.scale(SCALE);

			connectionStrand(tgOrigin, groupTransformOb, groupTransformOf, tangleObCornerB, tangleOfCornerC, equationGraph.getOfToObBottomColor().getColor3f());
		}


		if (options.isShowOb() && options.isShowR()) {
			// top connection strand tangle Ob to tangle R
			final Point3f tangleObCornerD = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_D);
			tangleObCornerD.scale(SCALE);
			final Point3f tangleRCornerA = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_A);
			tangleRCornerA.scale(SITE_SCALE);
			connectionStrand(tgOrigin, groupTransformR, groupTransformOb, tangleRCornerA, tangleObCornerD, equationGraph.getObToRTopColor().getColor3f());

			// bottom connection strand tangle Ob to tangle R
			final Point3f tangleObCornerC = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_C);
			tangleObCornerC.scale(SCALE);
			final Point3f tangleRCornerB = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_B);
			tangleRCornerB.scale(SITE_SCALE);

			connectionStrand(tgOrigin, groupTransformR, groupTransformOb, tangleRCornerB, tangleObCornerC, equationGraph.getObToRBottomColor().getColor3f());
		}

		/* Numerator */
		if (options.isShowNumerator()) {
			// top numerator from tangle Of to R
			final Point3f tangleOfCornerA = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_A);
			tangleOfCornerA.scale(SCALE);
			final Point3f tangleRCornerD = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_D);
			tangleRCornerD.scale(SITE_SCALE);
			numeratorStrand(tgOrigin, groupTransformOf, groupTransformR, tangleOfCornerA, tangleRCornerD, equationGraph.getNumeratorTopColor().getColor3f(), NUMERATOR_TOP_OFFSET);

			// bottom numerator from tangle Of to R
			final Point3f tangleOfCornerB = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_B);
			tangleOfCornerB.scale(SCALE);
			final Point3f tangleRCornerC = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_C);
			tangleRCornerC.scale(SITE_SCALE);

			numeratorStrand(tgOrigin, groupTransformOf, groupTransformR, tangleOfCornerB, tangleRCornerC, equationGraph.getNumeratorBottomColor().getColor3f(), NUMERATOR_BOTTOM_OFFSET);
		}

		/* MouseRotate */
		final MouseRotate mouseRotate = new MouseRotate(this.rootTransformationGroup);
		mouseRotate.setSchedulingBounds(new BoundingSphere(new Point3d(0, 0, 0), 100000));
		this.rootTransformationGroup.addChild(mouseRotate);

		this.universe.getViewingPlatform().setNominalViewingTransform();

		branchGroup.addChild(this.rootTransformationGroup);

		this.universe.addBranchGraph(branchGroup);
	}

	private void connectionStrand(final TransformGroup tgOrigin, final Transform3D groupTransformOne, final Transform3D groupTransformTwo, Point3f cornerOne, Point3f cornerTwo, Color3f color) {
		/* Matrix to be reused for (local to global) coordinate conversion: */
		final Matrix4f matrix = new Matrix4f();

		groupTransformOne.get(matrix);
		matrix.transform(cornerOne);

		groupTransformTwo.get(matrix);
		matrix.transform(cornerTwo);

		final TransformGroup connectionTransfromOneToTwo = Tangle3DUtils.createLineNode(cornerOne, cornerTwo, color, NUMERATOR_LINE_THICKNESS);
		tgOrigin.addChild(connectionTransfromOneToTwo);
	}

	private void numeratorStrand(final TransformGroup tgOrigin, final Transform3D groupTransformOne, final Transform3D groupTransformTwo, Point3f cornerOne, Point3f cornerTwo, Color3f color, Vector3f offset) {
		/* Matrix to be reused for (local to global) coordinate conversion: */
		final Matrix4f matrix = new Matrix4f();

		groupTransformOne.get(matrix);
		matrix.transform(cornerOne);

		groupTransformTwo.get(matrix);
		matrix.transform(cornerTwo);

		final Point3f oneThird = new Point3f();
		oneThird.x = (cornerOne.x * 2f / 3f) + (cornerTwo.x * 1f / 3f); 
		oneThird.y = (cornerOne.y * 2f / 3f) + (cornerTwo.y * 1f / 3f); 
		oneThird.z = (cornerOne.z * 2f / 3f) + (cornerTwo.z * 1f / 3f);

		final Point3f twoThirds = new Point3f();
		twoThirds.x = (cornerOne.x * 1f / 3f) + (cornerTwo.x * 2f / 3f); 
		twoThirds.y = (cornerOne.y * 1f / 3f) + (cornerTwo.y * 2f / 3f); 
		twoThirds.z = (cornerOne.z * 1f / 3f) + (cornerTwo.z * 2f / 3f);

		oneThird.add(offset);
		twoThirds.add(offset);

		tgOrigin.addChild(Tangle3DUtils.createLineNode(cornerOne, oneThird, color, NUMERATOR_LINE_THICKNESS));
		tgOrigin.addChild(Tangle3DUtils.createLineNode(oneThird, twoThirds, color, NUMERATOR_LINE_THICKNESS));
		tgOrigin.addChild(Tangle3DUtils.createLineNode(twoThirds, cornerTwo, color, NUMERATOR_LINE_THICKNESS));
	}

	private void addTetrahedron(final TransformGroup target, final float scale) {
		// Tetrahedron corners:
		Point3f tetrahedronCornerA = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_A);
		tetrahedronCornerA.scale(scale);
		Point3f tetrahedronCornerB = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_B);
		tetrahedronCornerB.scale(scale);
		Point3f tetrahedronCornerC = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_C);
		tetrahedronCornerC.scale(scale);
		Point3f tetrahedronCornerD = new Point3f(Tangle3DUtils.TETRAHEDRON_CORNER_D);
		tetrahedronCornerD.scale(scale);

		target.addChild(Tangle3DUtils.createVertexNode(tetrahedronCornerA, Tangle3DUtils.RED, Tangle3DTetrahedronDialog.TETRAHEDRON_CORNER_RADIUS));
		target.addChild(Tangle3DUtils.createVertexNode(tetrahedronCornerB, Tangle3DUtils.GREEN, Tangle3DTetrahedronDialog.TETRAHEDRON_CORNER_RADIUS));
		target.addChild(Tangle3DUtils.createVertexNode(tetrahedronCornerC, Tangle3DUtils.BLUE, Tangle3DTetrahedronDialog.TETRAHEDRON_CORNER_RADIUS));
		target.addChild(Tangle3DUtils.createVertexNode(tetrahedronCornerD, Tangle3DUtils.FUCHSIA, Tangle3DTetrahedronDialog.TETRAHEDRON_CORNER_RADIUS));

		// Crista: Commented out the tetrahedron sides because they are distracting. It is hard to analyze the projections this way.
		// Tetrahedron sides:
				target.addChild(Tangle3DUtils.createLineNode(tetrahedronCornerA, tetrahedronCornerB, Tangle3DUtils.FUCHSIA, Tangle3DTetrahedronDialog.TETRAHEDRON_LINE_THICKNESS));
				target.addChild(Tangle3DUtils.createLineNode(tetrahedronCornerA, tetrahedronCornerC, Tangle3DUtils.FUCHSIA, Tangle3DTetrahedronDialog.TETRAHEDRON_LINE_THICKNESS));
				target.addChild(Tangle3DUtils.createLineNode(tetrahedronCornerA, tetrahedronCornerD, Tangle3DUtils.FUCHSIA, Tangle3DTetrahedronDialog.TETRAHEDRON_LINE_THICKNESS));
				target.addChild(Tangle3DUtils.createLineNode(tetrahedronCornerB, tetrahedronCornerC, Tangle3DUtils.FUCHSIA, Tangle3DTetrahedronDialog.TETRAHEDRON_LINE_THICKNESS));
				target.addChild(Tangle3DUtils.createLineNode(tetrahedronCornerC, tetrahedronCornerD, Tangle3DUtils.FUCHSIA, Tangle3DTetrahedronDialog.TETRAHEDRON_LINE_THICKNESS));
				target.addChild(Tangle3DUtils.createLineNode(tetrahedronCornerD, tetrahedronCornerB, Tangle3DUtils.FUCHSIA, Tangle3DTetrahedronDialog.TETRAHEDRON_LINE_THICKNESS));
	}

	private void addBackground() {
		// Background
		final Background background = new Background(1, 1, 1);
		background.setApplicationBounds(new BoundingSphere(new Point3d(0, 0, 0), 100000)); // Changes from black background to white background.
		this.rootTransformationGroup.addChild(background);
	}

	private void addLights() {
		/* Lights of the axis: */
		this.rootTransformationGroup.addChild(Tangle3DUtils.createDirectionalLight(0.8f, 0.8f, 0.8f, 0, 0, -2, 0, 1, 1, 1000f));
		this.rootTransformationGroup.addChild(Tangle3DUtils.createDirectionalLight(0.8f, 0.8f, 0.8f, 0, 0, -2, 0, -1, -1, 1000f));
		this.rootTransformationGroup.addChild(Tangle3DUtils.createDirectionalLight(0.8f, 0.8f, 0.8f, 0, 0, -2, 1, 1, 0, 1000f));
		this.rootTransformationGroup.addChild(Tangle3DUtils.createDirectionalLight(0.8f, 0.8f, 0.8f, 0, 0, -2, -1, -1, 0, 1000f));
		this.rootTransformationGroup.addChild(Tangle3DUtils.createDirectionalLight(0.8f, 0.8f, 0.8f, 0, 0, -2, 1, 0, 1, 1000f));
		this.rootTransformationGroup.addChild(Tangle3DUtils.createDirectionalLight(0.8f, 0.8f, 0.8f, 0, 0, -2, -1, 0, -1, 1000f));

		this.rootTransformationGroup.addChild(Tangle3DUtils.createAmbientLight(1, 1, 1, 0, 0, 0, 1000f));
	}

	public BufferedImage takeScreenshot() {
		final Screen3D on = this.onScreenCanvas3D.getScreen3D();
		final Screen3D off = this.offScreenCanvas3D.getScreen3D();

		final Dimension dim = on.getSize();
		dim.width *= SCREENSHOT_UPSCALE;
		dim.height *= SCREENSHOT_UPSCALE;
		off.setSize(dim);
		off.setPhysicalScreenWidth(on.getPhysicalScreenWidth() * SCREENSHOT_UPSCALE);
		off.setPhysicalScreenHeight(on.getPhysicalScreenHeight() * SCREENSHOT_UPSCALE);
		Point locationOnScreen = this.onScreenCanvas3D.getLocationOnScreen();
		this.offScreenCanvas3D.setOffScreenLocation(locationOnScreen);

		final BufferedImage bi = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_ARGB);
		final ImageComponent2D buffer = new ImageComponent2D(ImageComponent.FORMAT_RGBA, bi);
		this.offScreenCanvas3D.setOffScreenBuffer(buffer);
		this.offScreenCanvas3D.renderOffScreenBuffer();
		this.offScreenCanvas3D.waitForOffScreenRendering();
		return this.offScreenCanvas3D.getOffScreenBuffer().getImage();
	}

	/**
	 * @param name
	 * @param cornerIndices
	 *            of length 4, each value being within [1, 4]
	 */
	private JButton createRotationButton(final Rotation rotation) {
		final JButton button = new JButton(rotation.name);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				Tangle3DTetrahedronDialog.this.resetCameraToRotation(rotation);
			}
		});
		return button;
	}

	protected void resetCameraToRotation(final Rotation rotation) {
		this.resetCameraToRotation(rotation.cornerIndices[0], rotation.cornerIndices[1], rotation.cornerIndices[2], rotation.cornerIndices[3]);
	}

	/**
	 * @param backEdgeCornerIndexA
	 *            from [1, 4]
	 * @param backEdgeCornerIndexB
	 *            from [1, 4]
	 * @param frontEdgeCornerIndexA
	 *            from [1, 4]
	 * @param frontEdgeCornerIndexB
	 *            from [1, 4]
	 */
	private void resetCameraToRotation(final int backEdgeCornerIndexA, final int backEdgeCornerIndexB, final int frontEdgeCornerIndexA, final int frontEdgeCornerIndexB) {
		final Point3f lookAt = this.getTerahedronEdgeMidpoint(backEdgeCornerIndexA, backEdgeCornerIndexB);

		final Point3f lookThrough = this.getTerahedronEdgeMidpoint(frontEdgeCornerIndexA, frontEdgeCornerIndexB);
		final Vector3f cameraDirection = Tangle3DUtils.subtract(lookThrough, lookAt);

		final Point3f bottomMidPoint = this.getTerahedronSideMidpoint(backEdgeCornerIndexA, backEdgeCornerIndexB, frontEdgeCornerIndexB);
		final Point3f topPoint = Tangle3DUtils.TETRAHEDRON_VIEW_ROTATION_CORNERS[frontEdgeCornerIndexA - 1];
		final Vector3f up = Tangle3DUtils.subtract(topPoint, bottomMidPoint);
		
		Point3d center = new Point3d(lookAt);
		
		final Point3d eye = new Point3d(center);
		final Point3d normalizedCameraDirection = new Point3d(this.normalize(new Point3f(cameraDirection)));
		normalizedCameraDirection.scale(Tangle3DTetrahedronDialog.EYE_DISTANCE_SCALE);
		eye.add(normalizedCameraDirection);

		this.resetCameraToSomething(eye, center, new Vector3d(up));
	}

	/**
	 * @param i
	 *            from [1, 4]
	 * @param j
	 *            from [1, 4]
	 * @return
	 */
	protected Point3f getTerahedronEdgeMidpoint(final int i, final int j) {
		final Point3f a = Tangle3DUtils.TETRAHEDRON_VIEW_ROTATION_CORNERS[i - 1];
		final Point3f b = Tangle3DUtils.TETRAHEDRON_VIEW_ROTATION_CORNERS[j - 1];
		return new Point3f((a.x + b.x) / 2, (a.y + b.y) / 2, (a.z + b.z) / 2);
	}

	/**
	 * @param i
	 *            from [1, 4]
	 * @param j
	 *            from [1, 4]
	 * @param k
	 *            from [1, 4]
	 * @return
	 */
	protected Point3f getTerahedronSideMidpoint(final int i, final int j, final int k) {
		final Point3f a = Tangle3DUtils.TETRAHEDRON_VIEW_ROTATION_CORNERS[i - 1];
		final Point3f b = Tangle3DUtils.TETRAHEDRON_VIEW_ROTATION_CORNERS[j - 1];
		final Point3f c = Tangle3DUtils.TETRAHEDRON_VIEW_ROTATION_CORNERS[k - 1];
		return new Point3f((a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3, (a.z + b.z + c.z) / 3);
	}

	private void resetCameraToSomething(final Point3d eye, final Point3d center, final Vector3d up) { // TODO Better method name
		final Transform3D identity = new Transform3D();
		identity.setIdentity();
		this.rootTransformationGroup.setTransform(identity);
		final ViewingPlatform viewingPlatform = this.universe.getViewingPlatform();
		final TransformGroup viewTransform = viewingPlatform.getViewPlatformTransform();
		final Transform3D t3d = new Transform3D();
		viewTransform.getTransform(t3d);

		t3d.lookAt(eye, center, up);
		t3d.invert();
		viewTransform.setTransform(t3d);
	}

	private Point3f normalize(final Point3f point) {
		final Point3f normalized = new Point3f(point);
		final float length = point.distance(new Point3f(0, 0, 0));
		normalized.scale(1 / length);
		return normalized;
	}

	private TransformGroup createTranslationTransformationGroup(final float x, final float y, final float z, final AxisAngle4d rotation, final Node node) {
		final TransformGroup tg = this.createTranslationTransformationGroup(x, y, z, rotation);
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

		tg.addChild(node);

		return tg;
	}

	private TransformGroup createTranslationTransformationGroup(final float x, final float y, final float z, final AxisAngle4d rotation) {
		final TransformGroup tg = new TransformGroup();

		final Transform3D transform = new Transform3D();
		final Vector3d vector = new Vector3d(x, y, z);
		transform.setTranslation(vector);
		transform.setRotation(rotation);
		tg.setTransform(transform);
		return tg;
	}

	public static class Rotation {
		private final String name;
		private final int[] cornerIndices;

		public Rotation(final String name, final int[] cornerIndices) {
			this.name = name;
			this.cornerIndices = cornerIndices;
		}
	}

	@Override
	public void windowActivated(final WindowEvent pWindowEvent) {

	}

	@Override
	public void windowClosed(final WindowEvent pWindowEvent) {
		final View view = this.universe.getViewer().getView();
		this.universe.removeAllLocales();
		view.removeAllCanvas3Ds();
		try {
			this.offScreenCanvas3D.setOffScreenBuffer(null);
		} catch (final NullPointerException e) {
			// Nothing...
		}
	}

	@Override
	public void windowClosing(final WindowEvent pWindowEvent) {

	}

	@Override
	public void windowDeactivated(final WindowEvent pWindowEvent) {

	}

	@Override
	public void windowDeiconified(final WindowEvent pWindowEvent) {

	}

	@Override
	public void windowIconified(final WindowEvent pWindowEvent) {

	}

	@Override
	public void windowOpened(final WindowEvent pWindowEvent) {

	}
}
