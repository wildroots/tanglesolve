/**
 * A collection of tangle equation solvers
 * Author : Yuki Saka
 * Last Updated : August 24, 2013
 * @edited by Crista Moreno
 * 
 */

package objects;

public class EquationSolverUtil {

	private static boolean Debug = false;
	private static VectorList[] div3List = null;
	private static int divMax = 250;
	private static boolean initialized = false;

	public static void initialize() {
		// Initializes the division table for faster algorithm
		EquationSolverUtil.div3List = new VectorList[2 * EquationSolverUtil.divMax];
		for (int i = -EquationSolverUtil.divMax; i < 0; i++) {
			EquationSolverUtil.div3List[EquationSolverUtil.toIndex(i)] = EquationSolverUtil.div3(i);
		}
		for (int i = 1; i <= EquationSolverUtil.divMax; i++) {
			EquationSolverUtil.div3List[EquationSolverUtil.toIndex(i)] = EquationSolverUtil.div3(i);
		}
		EquationSolverUtil.initialized = true;

	}

	private static int toIndex(final long value) {
		if (value < 0) {
			return (int) (value + EquationSolverUtil.divMax);
		} else {
			return (int) ((value + EquationSolverUtil.divMax) - 1);
		}
	}

	// Solve1
	// Solves the equations of form:
	// N(O + P) = b1/a1
	// N(O + R) = b2/a2
	// Where P = (0), and R is integral
	public static VectorList solve1(final FourPlat substrate, final FourPlat product) {
		return EquationSolverUtil.solve2(substrate, product, 0, 1);
	}

	// Solve2
	// Solves the equations of form:
	// N(O + nR) = b1/a1
	// N(O + mR) = b2/a2
	// Where R is integral
	public static VectorList solve2(final FourPlat fourPlat1, final FourPlat fourPlat2,
			final long n, final long m) {
		// Step1: solve the system -> (u, vr)
		try {
			VectorList solutions = EquationSolverUtil.intFilter(MathFunctions.solve2(1, n, fourPlat1.getRational()
					.getDenominator(), 1, m, fourPlat2.getRational().getDenominator()));

			if (EquationSolverUtil.Debug) {
				System.out.println("Step1: " + solutions);
			}

			// Step2: find (u, v, r)
			VectorList temp = solutions;
			for (; temp != null; temp = temp.tail()) {
				final VectorList ls = (VectorList) temp.head();
				VectorList divs = EquationSolverUtil.div2(((Rational) ls.getNth(2)).getNumerator());
				// filter those with v negative
				divs = VectorList.appendHeads(((VectorList) temp.head()).getNth(1), divs);
				temp.setHead(divs);
			}
			solutions = VectorList.fringe_one(solutions);
			temp = solutions;
			for (; temp != null; temp = temp.tail()) {
				final VectorList elt = (VectorList) temp.head();
				temp.setHead(new VectorList(new Rational(((Rational) elt.getNth(1))
						.getNumerator(), ((Rational) elt.getNth(2)).getNumerator()), new VectorList(elt
						.getNth(3), null)));
			}
			if (EquationSolverUtil.Debug) {
				System.out.println("Step2: " + solutions);
			}

			// Step3:
			// Make list (b1/a1 b2/a2 u/v r)
			final VectorList wls = VectorList.appendHeads(fourPlat1, VectorList.appendHeads(fourPlat2, solutions));

			solutions = EquationSolverUtil.solve2_step3(wls, n, m);
			for (temp = solutions; temp != null; temp = temp.tail()) {
				final VectorList elt = (VectorList) temp.head();
				final Rational o = (Rational) elt.getNth(3);
				final Rational r = (Rational) elt.getNth(4);
				final Tangle[] t = { new Tangle(new Rational(0)),
						new Tangle(o), new Tangle(r) };
				temp.setHead(VectorList.arrayToList(t));
			}
			return solutions;
		} catch (final Exception e) {
			System.out.println("Problem in soln2 " + e);
			return null;
		}
	}

	private static VectorList solve2_step3(final VectorList wls, final long n, final long m) throws IllegalArgumentException {

		if (wls == null) {
			return null;
		} else {
			final VectorList vls = (VectorList) wls.head();
			final FourPlat fourPlat1 = (FourPlat) vls.head();
			final FourPlat fourPlat2 = (FourPlat) vls.getNth(2);
			final Rational o = (Rational) vls.getNth(3);
			final Rational r = (Rational) vls.getNth(4);
			final Rational r1 = r.multiply(n), r2 = r.multiply(m);
			if (fourPlat1.equals(new FourPlat(new Tangle(o), new Tangle(r1)))
					&& fourPlat2.equals(new FourPlat(new Tangle(o), new Tangle(r2)))) {
				return new VectorList(wls.head(), EquationSolverUtil.solve2_step3(wls.tail(),
						n, m));
			} else {
				return EquationSolverUtil.solve2_step3(wls.tail(), n, m);
			}
		}
	}

	// Solve3
	// Solves the equations of form:
	// N(O + R) = b1/a1
	// N(O + R + R) = b2/a2
	// where O is integral
	public static VectorList solve3(final FourPlat fourPlat1, final FourPlat fourPlat2) {
		try {
			// R = (p/q) O = (u)
			// Step1: Find (q, c) = (abs(q), abs(2p + uq)) such that their product = a2
			VectorList solutions = EquationSolverUtil.div2(fourPlat2.getRational().getDenominator());

			if (EquationSolverUtil.Debug) {
				System.out.println("Step1 : " + solutions);
			}

			// Step2: for each such pairs, solve the system
			// abs(p + qu) = a1
			// abs(2p + qu) = c
			// to obtain (q, p , u)
			for (VectorList temp = solutions; temp != null; temp = temp.tail()) {
				final VectorList current = (VectorList) temp.head();
				final Rational q = (Rational) current.head();
				final Rational c = (Rational) current.getNth(2);
				VectorList system = EquationSolverUtil.intFilter(MathFunctions.solve2(1, q.getNumerator(), fourPlat1
						.getRational().getDenominator(), 2, q.getNumerator(), c.getNumerator()));
				system = VectorList.appendHeads(q, system);
				temp.setHead(system);
			}
			solutions = VectorList.fringe_one(solutions);

			if (EquationSolverUtil.Debug) {
				System.out.println("Step2 : " + solutions);
			}

			// Step3: make a list (u, p/q)
			for (VectorList temp = solutions; temp != null; temp = temp.tail()) {
				final VectorList current = (VectorList) temp.head();
				final Rational q = (Rational) current.head();
				final Rational p = (Rational) current.getNth(2);
				final Rational u = (Rational) current.getNth(3);
				final Tangle[] tg = { new Tangle(new Rational(0)), new Tangle(u),
						new Tangle(new Rational(p.getNumerator(), q.getNumerator())) };

				temp.setHead(VectorList.arrayToList(tg));
			}
			if (EquationSolverUtil.Debug) {
				System.out.println("Step3 : " + solutions);
			}

			// Step4: check if they are solutions
			return EquationSolverUtil.solve3_check4(fourPlat1, fourPlat2, solutions);
		} catch (final Exception exception) {
			System.out.println("Problem in solve3 " + exception);
			return null;
		}
	}

	private static VectorList solve3_check4(final FourPlat fourPlat1, final FourPlat fourPlat2,
			final VectorList solutions) throws IllegalArgumentException {
		if (solutions == null) {
			return null;
		} else {
			final VectorList temp = (VectorList) solutions.head();
			final Tangle oTangle = (Tangle) temp.head();
			final Tangle rTangle = (Tangle) temp.getNth(2);
			if (fourPlat1.equals(new FourPlat(oTangle, rTangle))
					&& fourPlat2.equals(new FourPlat(rTangle, rTangle.addTail(oTangle.getRational().getNumerator())))) {
				return new VectorList(solutions.head(), EquationSolverUtil.solve3_check4(fourPlat1, fourPlat2,
						solutions.tail()));
			} else {
				return EquationSolverUtil.solve3_check4(fourPlat1, fourPlat2, solutions.tail());
			}
		}
	}

	// Montesinos Analysis
	// N(Of + Ob + (0)) = b1/a1
	// N(Of + Ob + (k)) = b2/a2

	public static VectorList solve4(final FourPlat fourPlat1, final FourPlat fourPlat2) {
		try {
			// if fourPlat1 = fourPlat2 then there are infinite number of solutions

			if (EquationSolverUtil.Debug) {
				System.out.println("Substrate: " + fourPlat1 + " Product : " + fourPlat2);
			}
			// Step1:
			// compute (vyk sig1 sig2)
			final long denominator1 = fourPlat1.getRational().getDenominator();
			final long denominator2 = fourPlat2.getRational().getDenominator();
			final long numerator1 = fourPlat1.getRational().getNumerator();
			final long numerator2 = fourPlat2.getRational().getNumerator();

			final Rational[] rationalArray1 = {new Rational(denominator1 - denominator2), new Rational(-1),
					new Rational(-1)};
			final Rational[] rationalArray2 = { new Rational(denominator1 + denominator2), new Rational(-1),
					new Rational(1) };
			final Rational[] rationalArray3 = { new Rational(-denominator1 - denominator2), new Rational(1),
					new Rational(-1) };
			final Rational[] rationalArray4 = { new Rational(-denominator1 + denominator2), new Rational(1),
					new Rational(1) };
			
			VectorList solutions = new VectorList(VectorList.arrayToList(rationalArray1), new VectorList(
					VectorList.arrayToList(rationalArray2), new VectorList(VectorList.arrayToList(rationalArray3),
							new VectorList(VectorList.arrayToList(rationalArray4), null))));

			if (EquationSolverUtil.Debug) {
				System.out.println("Step1 (vyk sig1 sig2): " + solutions);
			}

			// Step2:
			// compute (sig1, sig2, v, y, k)
			for (VectorList temp = solutions; temp != null; temp = temp.tail()) {
				final VectorList currentList = (VectorList) temp.head();
				final long vyk = ((Rational) currentList.head()).getNumerator();

				VectorList divls = EquationSolverUtil.div3(vyk);

				if (divls != null) {
					divls = VectorList.appendHeadList(currentList.tail(), divls);
				}
				temp.setHead(divls);
			}

			solutions = VectorList.fringe_one(solutions);

			if (EquationSolverUtil.Debug) {
				System.out.println("Step2 : (sig1 sig2 v y k)" + solutions);
			}

			// Step3:
			// compute (x1', x2' , u, x, sig1, sig2, v, y, k)
			for (VectorList temp = solutions; temp != null; temp = temp.tail()) {
				VectorList cls = (VectorList) temp.head();
				final long y = ((Rational) cls.getNth(4)).getNumerator();
				final long v = ((Rational) cls.getNth(3)).getNumerator();
				final long sig1 = ((Rational) cls.getNth(1)).getNumerator();
				final long sig2 = ((Rational) cls.getNth(2)).getNumerator();

				Rational x1p = new Rational(1, 0);
				Rational x2p = new Rational(1, 0);
				if (denominator1 == 0) {
					if ((((-v * sig1) % v) == 0) && (numerator1 == ((-v * sig1) / v))) {
						x1p = new Rational(1);
					} else {
						x1p = new Rational(1, 0);
					}
				}
				if (denominator2 == 0) {
					if ((((-v * sig2) % v) == 0) && (numerator2 == ((-v * sig2) / v))) {
						x2p = new Rational(1);
					} else {
						x2p = new Rational(1, 0);
					}
				}
				if (denominator1 != 0) {
					x1p = new Rational(((numerator1 * y) + (sig1 * v)), denominator1);
					if (!x1p.isInteger()) {
						final long inv = MathFunctions.moduloInverse1(numerator1, denominator1);
						x1p = new Rational(((inv * y) + (sig1 * v)), denominator1);
					}
				}
				if (denominator2 != 0) {
					x2p = new Rational(((numerator2 * y) + (sig2 * v)), denominator2);
					if (!x2p.isInteger()) {
						final long inv = MathFunctions.moduloInverse1(numerator2, denominator2);
						x2p = new Rational(((inv * y) + (sig2 * v)), denominator2);
					}
				}

				if (x1p.isInteger() && x2p.isInteger()) {
					Long inverse1;
					if (x1p.isZero()) {
						inverse1 = new Long(0);
					} else {
						inverse1 = new Long(MathFunctions.moduloInverse1(x1p.getNumerator(), y));
					}

					Long inverse2;
					if (x2p.isZero()) {
						inverse2 = new Long(0);
					} else {
						inverse2 = new Long(MathFunctions.moduloInverse1(x2p.getNumerator(), y));
					}

					if ((inverse1 == null) || (inverse2 == null)) {
						// TODO This sounds like it's wrong...???
						x1p = new Rational(5, 6);
						x2p = new Rational(5, 6);
					} else {
						final Rational x = new Rational(inverse1);
						final Rational u = new Rational((sig1 * denominator1) - (v * inverse1), y);

						final VectorList hls = new VectorList(u, new VectorList(x, null));
						cls = hls.append(cls);
					}
				}

				cls = new VectorList(x1p, new VectorList(x2p, cls));
				temp.setHead(cls);
			}
			solutions = EquationSolverUtil.intFilter(solutions);

			if (EquationSolverUtil.Debug) {
				System.out.println("Step3 (x1', x2' , u, x, sig1, sig2, v, y, k): " + solutions);
			}

			// Step4:
			// compute the minimum projection
			// (x1', x2', u, x, sig1, sig2, v, y, k)
			for (VectorList temp = solutions; temp != null; temp = temp.tail()) {
				final VectorList currentList = (VectorList) temp.head();

				final long u = ((Rational) currentList.getNth(3)).getNumerator();
				final long v = ((Rational) currentList.getNth(7)).getNumerator();
				final long x = ((Rational) currentList.getNth(4)).getNumerator();
				final long y = ((Rational) currentList.getNth(8)).getNumerator();
				final long k = ((Rational) currentList.getNth(9)).getNumerator();
				
				final Tangle[] t = { new Tangle(new Rational(x, y)),
						new Tangle(new Rational(u, v)),
						new Tangle(new Rational(k)) };
				temp.setHead(VectorList.arrayToList(t));
			}

			return VectorList.filterSame(solutions);

		} catch (final Exception exception) {
			System.out.println("Problem occured in solve4" + exception);
			return null;
		}
	}

	@SuppressWarnings("unused")
	private static long sign(final long a) {
		if (a == 0) {
			return 0;
		} else {
			return a / (Math.abs(a));
		}
	}

	// --------------------------UTILITY FUNCTIONS---------------------------
	// Finds ordered integer pairs (a,b) such that a*b = x and a > 0
	private static VectorList div2(final long x) {
		VectorList temp = null;
		final long n = Math.abs(x);
		final long lim = (long) Math.sqrt(n);
		for (int i = 1; i <= lim; i++) {
			if ((n % i) == 0) {
				if (x >= 0) {
					final Rational[] r1 = { new Rational(i),
							new Rational(n / i) };
					final VectorList ls1 = VectorList.arrayToList(r1);
					temp = new VectorList(ls1, temp);

					if (i != (n / i)) {
						final Rational[] r2 = { new Rational(n / i),
								new Rational(i) };
						final VectorList ls2 = VectorList.arrayToList(r2);
						temp = new VectorList(ls2, temp);
					}
				} else {
					final Rational[] r1 = { new Rational(i),
							new Rational(-n / i) };
					final VectorList ls1 = VectorList.arrayToList(r1);
					temp = new VectorList(ls1, temp);

					if (i != (n / i)) {
						final Rational[] r2 = { new Rational(n / i),
								new Rational(-i) };
						final VectorList ls2 = VectorList.arrayToList(r2);
						temp = new VectorList(ls2, temp);
					}

				}

			}
		}
		return temp;
	}

	private static VectorList div2ac(VectorList temp, final long first, final long x) {

		final long n = Math.abs(x);
		final long lim = (long) Math.sqrt(n);
		for (int i = 1; i <= lim; i++) {
			if ((n % i) == 0) {
				if (x >= 0) {
					final Rational[] r1 = { new Rational(first),
							new Rational(i), new Rational(n / i) };
					final VectorList ls1 = VectorList.arrayToList(r1);
					temp = new VectorList(ls1, temp);

					if (i != (n / i)) {
						final Rational[] r2 = { new Rational(first),
								new Rational(n / i), new Rational(i) };

						final VectorList ls2 = VectorList.arrayToList(r2);

						temp = new VectorList(ls2, temp);
					}
				} else {
					final Rational[] r1 = { new Rational(first),
							new Rational(i), new Rational(-n / i) };
					final VectorList ls1 = VectorList.arrayToList(r1);
					temp = new VectorList(ls1, temp);

					if (i != (n / i)) {
						final Rational[] r2 = { new Rational(first),
								new Rational(n / i), new Rational(-i) };

						final VectorList ls2 = VectorList.arrayToList(r2);

						temp = new VectorList(ls2, temp);
					}
				}
			}
		}
		return temp;
	}

	// Find ordered triplet (a, b, c) such that x = abc
	private static VectorList div3(final long x) {
		if (EquationSolverUtil.initialized && (Math.abs(x) <= EquationSolverUtil.divMax)) {
			return EquationSolverUtil.div3List[EquationSolverUtil.toIndex(x)];
		}

		VectorList temp = null;
		final long n = Math.abs(x);
		for (int i = 1; i <= n; i++) {
			if ((n % i) == 0) {
				if (x >= 0) {
					temp = EquationSolverUtil.div2ac(temp, i, n / i);
				} else {
					temp = EquationSolverUtil.div2ac(temp, i, -n / i);
				}
			}
		}
		return temp;
	}

	private static VectorList intFilter(final VectorList list) {
		if (list == null) {
			return null;
		} else if (EquationSolverUtil.isIntegerList((VectorList) list.head())) {
			return new VectorList(list.head(), EquationSolverUtil.intFilter(list.tail()));
		} else {
			return EquationSolverUtil.intFilter(list.tail());
		}
	}

	private static boolean isIntegerList(VectorList list) {
		for (; list != null; list = list.tail()) {
			if (!((Rational) list.head()).isInteger()) {
				return false;
			}
		}
		return true;
	}
}