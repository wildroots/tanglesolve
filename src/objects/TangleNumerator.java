/**
 * @author Yuki Saka
 * Last Updated : April 14, 2013
 * @edited by Crista Moreno
 */
package objects;

import graphics.SolutionGraph;

import javax.swing.DefaultListModel;


public class TangleNumerator implements Element {
	public VectorList<DefaultListModel> tangleList = null;
	public VectorList<String> nameList = null;
	public Tangle[] tangleArray = null;
	
	// private fields
	private int selectIndex;
	private DefaultListModel selection = null;
	private int round;
	private boolean isRational = false;

	// constructors
	// models of
	// N(O1 + O2 + (0)) -> N(O1 + O2 + (k)) or
	// lists all minimum projections which satisfy these models
	public TangleNumerator(final VectorList<FourPlat> products, final Tangle o1Tangle, final Tangle o2Tangle, final Tangle kTangle) {
		this(products, o1Tangle, o2Tangle, kTangle, true);
	}

	public TangleNumerator(final VectorList<FourPlat> products, final Tangle o1Tangle, final Tangle o2Tangle, final Tangle kTangle, final boolean compress) {
		try {
			this.round = products.length();
			// Compress the tangle if O1/O2 is integral
			Rational o2Rational = o2Tangle.getRational();
			Rational kRational = kTangle.getRational();
			Rational o1Rational = o1Tangle.getRational();
			
			// O2 Tangle is integral
			if (o2Rational.isInteger() && (this.round <= 2)
					&& (Math.abs(kRational.getNumerator()) == 1)) {
				this.isRational = true;
				final Tangle[] tangleArray = {new Tangle(new Rational(0)),
						o1Tangle.addTail(o2Rational.getNumerator()).rotate90()
								.addTail(-kRational.getNumerator()), new Tangle(new Rational(1, 0))};
				
				this.tangleList = new VectorList<DefaultListModel>(this.makeListModel(tangleArray, products), this.tangleList);
				this.nameList = new VectorList<String>(this.format(VectorList.arrayToList(tangleArray), true), this.nameList);
				
			// O1 Tangle is integral
			} else if (o1Rational.isInteger() && (this.round <= 2)
					&& (Math.abs(kRational.getNumerator()) == 1)) {
				this.isRational = true;
				final Tangle[] tangleArray = {new Tangle(new Rational(0)),
						o2Tangle.addTail(o1Rational.getNumerator()).rotate90()
						.addTail(-kRational.getNumerator()), new Tangle(new Rational(1, 0)) };
				this.tangleList = new VectorList<DefaultListModel>(this.makeListModel(tangleArray, products), this.tangleList);
				this.nameList = new VectorList<String>(this.format(VectorList.arrayToList(tangleArray), true), this.nameList);
			}
			// O1 Tangle is integral
			if (o1Rational.isInteger()) {
				this.isRational = true;
				final Tangle[] tangleArray = { new Tangle(new Rational(0)),
						o2Tangle.addTail(o1Rational.getNumerator()), kTangle };
				this.tangleArray = tangleArray;
				this.tangleList = new VectorList<DefaultListModel>(this.makeListModel(tangleArray, products), this.tangleList);
				this.nameList = new VectorList<String>(this.format(VectorList.arrayToList(tangleArray), true), this.nameList);
				
			// O2 Tangle is integral
			} else if (o2Rational.isInteger()) {
				this.isRational = true;
				final Tangle[] tangleArray = { new Tangle(new Rational(0)),
						o1Tangle.addTail(o2Rational.getNumerator()), kTangle };
				this.tangleArray = tangleArray;
				this.tangleList = new VectorList<DefaultListModel>(this.makeListModel(tangleArray, products), this.tangleList);
				this.nameList = new VectorList<String>(this.format(VectorList.arrayToList(tangleArray), true), this.nameList);
			} else {
				// Montesinos case
				if (o1Rational.isInfinity() || o2Rational.isInfinity()) {
					final Tangle[] tangles = { o1Tangle, o2Tangle, kTangle };
					this.tangleArray = tangles;
					this.tangleList = new VectorList<DefaultListModel>(this.makeListModel(tangles, products), this.tangleList);
					this.nameList = new VectorList<String>(this.format(VectorList.arrayToList(tangles), true), this.nameList);
				} else {
					final long k1 = o1Rational.getNumerator() / o1Rational.getDenominator();
					final long k2 = o2Rational.getNumerator() / o2Rational.getDenominator();

					final int sig1 = MathFunctions.sign(o1Rational.getNumerator()
							% o1Rational.getDenominator());
					final int sig2 = MathFunctions.sign(o2Rational.getNumerator()
							% o2Rational.getDenominator());
					
					final double d1 = -((2 * k1) + sig1) / 2.0d;
					final double d2 = ((2 * k2) + sig2) / 2.0d;

					long dhigh;
					long dlow;
					
					if ((int) d1 == (int) d2) {
						dhigh = (long) Math.ceil(d1);
						dlow = (long) Math.floor(d1);
					} else if (d1 > d2) {
						dhigh = (long) Math.floor(d1);
						dlow = (long) Math.ceil(d2);
					} else {
						dhigh = (long) Math.floor(d2);
						dlow = (long) Math.ceil(d1);
					}
					for (long i = dlow; i <= dhigh; i++) {
						final Tangle[] tangleArray = { o1Tangle.addTail(i), o2Tangle.addTail(-i), kTangle };
						this.tangleArray = tangleArray;
						this.tangleList = new VectorList<DefaultListModel>(this.makeListModel(tangleArray, products), this.tangleList);
						this.nameList = new VectorList<String>(this.format(VectorList.arrayToList(tangleArray), true), this.nameList);
					}
				}
			}

			if (!compress) {
				final Tangle[] tangleArray = { o1Tangle, o2Tangle, kTangle };
				this.tangleArray = tangleArray;
				this.tangleList = new VectorList<DefaultListModel>(this.makeListModel(tangleArray, products), this.tangleList);
				this.nameList = new VectorList<String>(this.format(VectorList.arrayToList(tangleArray), false), this.nameList);
			}

			this.selection = (DefaultListModel) this.tangleList.head();
			this.selectIndex = 1;
		} catch (final Exception exception) {
			exception.printStackTrace();
			System.out.println("Exception at TangleNumerator constructor " + exception);
		}
	}

	private DefaultListModel makeListModel(final Tangle[] tangleArray, final VectorList<FourPlat> list) {
		final DefaultListModel listModel = new DefaultListModel();
		for (int i = 0; i < list.length(); i++) {
			Tangle of = tangleArray[0];
			Tangle ob = tangleArray[1];
			Tangle k = tangleArray[2];
			FourPlat fourplat = (FourPlat) list.getNth(i + 1);
			listModel.addElement(new SolutionGraph(of, ob, k, fourplat, i));
		}
		return listModel;
	}

	public void select(final int i) {
		if (i <= this.tangleList.length()) {
			this.selection = (DefaultListModel) this.tangleList.getNth(i);
			this.selectIndex = i;
		}
	}

	public int getSelectedIndex() {
		return this.selectIndex;
	}

	public DefaultListModel selection() {
		return this.selection;
	}

	@Override
	public boolean equals(final Object object) {
		if (object instanceof TangleNumerator) {
			final TangleNumerator tangleNumerator = (TangleNumerator) object;
			return this.tangleArray[0].equals(tangleNumerator.tangleArray[0]) && this.tangleArray[1].equals(tangleNumerator.tangleArray[1])
					&& this.tangleArray[2].equals(tangleNumerator.tangleArray[2]);
		} else { // not a TangleNumerator object
			return false;
		}
	}

	public boolean isRational() {
		return this.isRational;
	}

	public String format(final VectorList<Tangle> list, final boolean min) {
		final String o1 = list.head().toString();
		final String o2 = list.tail().head().toString();
		final String k = list.tail().tail().head().toString();
		
		String s = "O = " + o1 + " + " + o2;
		if (((Tangle) list.tail().tail().head()).getRational().isInteger()) {
			s = s + ", R = " + k;
		} else {
			s = s + ", P = " + k + "-> (0)";
		}
		if (min) {
			s = s + " - O minimum";
		}
		return s;
	}

	@Override
	public String toString() {
		final SolutionGraph solutionGraph = (SolutionGraph) this.selection.elementAt(1);
		return "O = "  + solutionGraph.getOfTangle() + " + "
				+ solutionGraph.getObTangle() + ", R = " + solutionGraph.getRTangle();
	}
	
	/**
	 * Calls Object.clone() to give a field-to-field copy of the instance </p>
	 * 
	 * @return Object version of the clone
	 * */
	@Override
	public TangleNumerator clone() {
		try {
			return (TangleNumerator) super.clone();
		} catch (final Exception exception) {
			return null;
		}
	}
}
