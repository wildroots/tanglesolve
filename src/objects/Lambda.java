/**
 * Author : Yuki Saka
 * Last Updated : August 24, 2013
 * @edited by Crista Moreno
 */
package objects;

public interface Lambda {
	Element envoke(VectorList args) throws IllegalArgumentException;
}
