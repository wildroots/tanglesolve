package objects;
/**
 * Louis Kauffman and Sofia Lambropoulou showed that the numerator of the sum of two rational tangles, with 
 * fractions P/Q and R/S respectively,  is the unknot if and only if P * S + Q * R = +1 or -1.
 * 
 * @author Crista Moreno
 *
 */
public class KauffmanUnknotTheorem {

	/**
	 * Checks if the sum of the numerator of the sum of rational tangles tangle1 and tangle2 produce the unknot.
	 * 
	 * @param tangle1 has to be in reduced form
	 * @param tangle2 has to be in reduced form
	 * 
	 * @return true if the numerator of the sum is the unknot
	 */
	public static boolean unknotTest(Rational tangle1, Rational tangle2) {
		long p = tangle1.getNumerator(); 
		long q = tangle1.getDenominator();
		long r = tangle2.getNumerator();
		long s = tangle2.getDenominator();		
		
		if (Math.abs(p * s +  q * r) == 1) {
			return true;
		}
		return false;
	}
}
