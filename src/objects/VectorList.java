/**
 * <p>Title: VectorList </p>
 * <p>Description: A Vector List that is cloneable </p>
 * <p>Copyright: Yuki Saka</p>
 * @author Yuki Saka
 * @version 1.0
 */

package objects;

// List interface for list processing
public class VectorList<T> implements Element {

	// constants
	public final static int REGULAR = 0;
	public final static int INPUTLS = 1;
	public final static int ARROW = 2;
	public final static int SLASH = 3;
	
	// private fields
	private T head;
	private VectorList<T> tail;
	private int stringType = 0;
	
	// constructors
	/**
	 * @param head
	 * head of the VectorList
	 * 
	 * @param tail
	 * tail of the VectorList
	 */
	public VectorList(final T head, final VectorList<T> tail) {
		// VectorList FIELDS
		this.head = head;
		this.tail = tail;
	}

	/**
	 * Assigns Values to string type
	 * 
	 * @param type
	 */
	public void setString(final int type) {
		this.stringType = type;
	}

	/**
	 * Converts an array of objects to a VectorList
	 * 
	 * @param objectList
	 * Array of objects
	 * 
	 * @return VectorList containing the objects of the array.
	 */
	public static <T> VectorList<T> arrayToList(final T[] objectList) {
		VectorList<T> res = null;
		if (objectList == null) {
			return null;
		}
		for (int i = 0; i < objectList.length; i++) {
			res = new VectorList<T>(objectList[i], res);
		}
		return res.reverse();
	}

	/**
	 * Converts a VectorList to an array of Objects
	 * 
	 * @param list
	 * VectorList to be converted
	 * 
	 * @return an array of Objects that contains all elements of this list
	 */
	public static <T> T[] listToArray(VectorList<T> list) {
		final T[] elements = (T[]) new Object[list.length()];
		for (int i = 0; i < list.length(); i++, list = list.tail()) {
			elements[i] = list.head();
		}
		return elements;
	}

	/**
	 * Clones this VectorList
	 * 
	 * @return clone of this VectorList as an Object
	 */
	@Override
	public Element clone() {
		return this.cloneHelper(this);
	}

	/**
	 * Returns a clone of the VectorList list Assumes objects in list are instance of
	 * Element
	 */
	private VectorList<Element> cloneHelper(final VectorList<T> list) {
		if (list == null) {
			return null;
		} else {
			VectorList<Element> result = null;
			for (VectorList<T> temp = list; temp != null; temp = temp.tail()) {
				if (temp.head() != null) {
					result = new VectorList<Element>(((Element) temp.head()).clone(), result);
				} else {
					result = new VectorList<Element>(null, result);
				}
			}
			result = result.reverse();
			return result;
		}
	}

	//-------------------EQUIVALENCE ----------------//
	/**
	 * Checks if two VectorLists are equal
	 * 
	 * @param object
	 * VectorList to be checked against this one
	 * 
	 * @returns true if they are equal
	 */
	@Override
	public boolean equals(final Object object) {
		if (object instanceof VectorList) {
			final VectorList arg = (VectorList) object;
			final VectorList list = arg;
			if (list.head().equals(this.head)) {
				if ((this.tail != null) && (list.tail() != null)) {
					return this.tail.equals(list.tail());
				} else {
					return (this.tail == null) && (list.tail() == null);
				}
			} else {
				return false;
			}
		} else {
			return false;
		}	
	}

	//-------------------VectorList MUTATOR METHODS ----------------//
	/**
	 * Sets head of VectorList
	 * 
	 * @param head
	 * new head for the VectorList
	 */
	public void setHead(final T head) {
		this.head = head;
	}

	/**
	 * Sets tail of VectorList
	 * 
	 * @param tail
	 * new tail for the VectorList
	 */
	public void setTail(final VectorList tail) {
		this.tail = tail;
	}

	/**
	 * Replaces the Object in the nth position of the list
	 * 
	 * @param n
	 * position of the replacement
	 * 
	 * @param element
	 * the new element
	 */
	public void setNth(final long n, final T element) {
		final VectorList<T> nthList = this.listNth(n);
		if (nthList != null) {
			nthList.head = element;
		}
	}

	//-------------------STRING EXPRESSION TOOLS----------------//

	/**
	 * @return String representation of the VectorList
	 */
	// TODO Can this method be optimized more so?
	@Override
	public String toString() {
		final VectorList list = this;

		if (this.stringType == REGULAR) {
			return VectorList.regularListString(list);
		} else if (this.stringType == INPUTLS) {
			return VectorList.inputListString(list);
		} else if (this.stringType == ARROW) {
			return VectorList.arrowList(list);
		} else if (this.stringType == SLASH) {
			return VectorList.slashList(list);
		} else {
			return "";
		}
	}

	/**
	 * Expresses a VectorList enclosed in "()"
	 * 
	 * @param list
	 * VectorList to be expressed
	 * 
	 * @return String representation of VectorList enclosed in "()"
	 */
	public static String regularListString(VectorList list) {
		return enclosedIn(list, "(", "()", ")");
	}
	
	/**
	 * Expresses a VectorList enclosed in "{}"
	 * 
	 * @param list
	 * VectorList to be expressed
	 * 
	 * @return String representation of VectorList enclosed in "{}"
	 */
	public static String curlyBraceList(VectorList list) {
		return enclosedIn(list, "{", "{}", "}");
	}
	
	/**
	 * Expresses a VectorList enclosed in "<>"
	 * 
	 * @param list
	 * VectorList to be expressed
	 * 
	 * @return String representation of VectorList enclosed in "<>"
	 */
	public static String angleBracketList(VectorList list) {
		return enclosedIn(list, "<", "<>", ">");
	}

	private static String enclosedIn(VectorList list, String open, String together, String closed) {
		String s = open;
		for (; list != null; list = list.tail) {
			if (list.head == null) {
				s = s + together;
			} else {
				s = s + list.head;
			}

			if (list.tail != null) {
				s = s + ",";
			}
		}
		s = s.trim() + closed;
		return s;
	}

	/**
	 * Separates each element in the list with "->"
	 * 
	 * @param list
	 * VectorList to be expressed
	 * 
	 * @return String representation of list with "->" between elements
	 */
	public static String arrowList(VectorList list) {
		return separateBy(list, " -> ");
	}

	/**
	 * Separates each element in the list with "/"
	 * 
	 * @param list
	 * VectorList to be expressed
	 * 
	 * @return String representation of "list" with "/" between elements
	 */
	public static String slashList(VectorList list) {
		return separateBy(list, " / ");
	}

	/**
	 * Separates each element in the list with ",".
	 * 
	 * @param list
	 * VectorList to be expressed
	 * 
	 * @return String representation of "list" with "," between elements.
	 */
	public static String commaList(VectorList list) {
		return separateBy(list, " , ");
	}

	private static String separateBy(VectorList list, String seperator) {
		String s = "";
		for (; list != null; list = list.tail) {
			s = s + list.head;
			if (list.tail != null) {
				s = s + " , ";
			}
		}
		return s;
	}
	
	/**
	 * 
	 * @param list1
	 * @return
	 */
	public static String inputListString(VectorList list1) {
		String s = "";

		for (; list1 != null; list1 = list1.tail) {
			final VectorList list2 = (VectorList) list1.head;
			s = s + "{";
			
			if (list2.head() != null) {
				s = s + "(";
				for (VectorList temp = (VectorList) list2.head(); temp != null; temp = temp.tail()) {
					s = s + temp.head();
					if (temp.tail != null) {
						s = s + ",";
					}
				}
				s = s + " crossing)";
			}

			if (list2.tail != null) {
				final String t = VectorList.commaList(list2.tail);
				s = s + t;
			}
			s = s + "}";

			if (list1.tail != null) {
				s = s + " -> ";
			}
		}
		return s;
	}

	/**
	 * Expresses a VectorList enclosed in "{}"
	 * 
	 * @param list
	 * VectorList to be expressed
	 * 
	 * @return String representation of VectorList enclosed in "{}"
	 */
	public static String curlyBraceKnotList(VectorList list) {
		String s = "{";
		for (; list != null; list = list.tail) {
			if (list.head == null) {
				s = s + "{}";
			} else {
				if (list.head instanceof VectorList) {
					s = s + VectorList.commaList((VectorList) list.head) + " crossing";
				} else {
					s = s + list.head;
				}
			}
			if (list.tail != null) {
				s = s + ",";
			}
		}
		s = s.trim() + "}";
		return s;
	}
	
	//-------------------LIST PROCESSING FUNCTIONS ----------------//

	/**
	 * Gives a new VectorList that is the reverse of this VectorList
	 * 
	 * @return a copy of elements of this VectorList in reversed order
	 */
	public VectorList<T> reverse() {
		VectorList<T> list;
		VectorList<T> reverseList;
		for (reverseList = null, list = this; list != null; list = list.tail) {
			reverseList = new VectorList<T>(list.head, reverseList);
		}
		return reverseList;
	}

	// could not test
	/**
	 * Tests if any element in "list" is equal to a given Object.
	 * 
	 * @param element
	 * reference object with which to compare
	 * 
	 * @param list
	 * the VectorList where to find elements equal to element
	 * 
	 * @return true if list has an element equal to argument element
	 */
	public static boolean member(final Object element, VectorList list) {
		for (; list != null; list = list.tail()) {
			if (element.equals(list.head())) {
				return true;
			}
		}
		return false;
	}

	// could not test
	/**
	 * Removes the given object from this VectorList.
	 * 
	 * @param element
	 * element to be removed from list
	 * 
	 * @return a new VectorList with the given element removed if it was in the list,
	 * else returns null
	 */
	public VectorList<T> remove(final Object element) {
		final VectorList<T> list = this;
		if (list.head().equals(element)) {
			if (list.tail() != null) {
				return (VectorList<T>) list.tail().clone();
			} else {
				return null;
			}
		} else {
			return new VectorList<T>(list.head(), list.tail().remove(element));
		}
	}

	/**
	 * Removes last element in the list.
	 * 
	 * @param list
	 * VectorList from which to remove element
	 * 
	 * @return copy of this VectorList with last element removed
	 */
	public static VectorList removeLast(final VectorList list) {
		if (list == null) {
			return null;
		} else if (list.tail() == null) {
			return null;
		} else {
			return new VectorList(list.head(), VectorList.removeLast(list.tail()));
		}
	}

	/**
	 * Removes repeated elements from the list.
	 * 
	 * @param list
	 * VectorList from which to remove repeats
	 * 
	 * @return a copy of this list with repeated elements removed
	 */
	public static <T> VectorList<T> filterSame(final VectorList<T> list) {
		if (VectorList.isEmpty(list)) {
			return null;
		} else if (VectorList.member(list.head(), list.tail())) {
			return VectorList.filterSame(list.tail());
		} else {
			return new VectorList<T>(list.head(), VectorList.filterSame(list.tail()));
		}
	}

	/**
	 * Removes those elements with only 1 occurrence.
	 * 
	 * @param list
	 * List from which to remove the single elements
	 * 
	 * @return copy of this list with single elements removed
	 */
	public static VectorList filterIsolated(final VectorList list) {
		if (list == null) {
			return null;
		} else if (VectorList.member(list.head(), list.tail())) {
			return new VectorList(list.head(), VectorList.filterIsolated(list.tail()));
		} else {
			return VectorList.filterIsolated(list.tail());
		}
	}

	/**
	 * @return number of elements in this list
	 */
	public int length() {
		VectorList list = this;
		for (int n = 0;; n += 1) {
			if (list == null) {
				return n;
			} else {
				list = list.tail;
			}
		}
	}

	// -----------------COMBINATION ROUTINES---------------------//

	/**
	 * Append another list to tail of this list
	 * 
	 * @param list2
	 * the list to append
	 * 
	 * @return this list with list2 appended
	 */
	public VectorList<T> append2(final VectorList<T> list2) {
		VectorList<T> temp = this;
		while (temp.tail() != null) {
			temp = temp.tail();
		}
		temp.tail = list2;
		return this;
	}

	/**
	 * Appends a new list to clone of this list.
	 * 
	 * @param newList
	 * new list
	 * 
	 * @return clone of this list with arg appended
	 */
	public VectorList<T> append(final VectorList<T> newList) {
		final VectorList<T> list = (VectorList<T>) this.clone();
		return list.append2(newList);
	}

	/**
	 * Appends two lists.
	 * 
	 * @param list1
	 * first list
	 * 
	 * @param list2
	 * second list; will be appended to tail of first list
	 * 
	 * @return a list with both list joined
	 */
	public static <T> VectorList<T> append(final VectorList<T> list1, final VectorList<T> list2) {
		if (list1 == null) {
			return list2;
		} else {
			return list1.append(list2);
		}
	}

	/**
	 * Set containing elements in both lists, with all repeats removed.
	 * 
	 * @param list1
	 * @param list2
	 * @return list that contains each element of both lists exactly once
	 */
	public static VectorList union(final VectorList list1, final VectorList list2) {
		final VectorList list3 = VectorList.append(list1, list2);
		return VectorList.filterSame(list3);
	}

	/**
	 * Set containing elements common to both list, with all repeats removed.
	 * 
	 * @param list1
	 * @param list2
	 * @return list that is the intersection of list1, list2
	 */
	public static VectorList intersection(final VectorList list1, final VectorList list2) {
		VectorList list3 = VectorList.append(list1, list2);
		list3 = VectorList.filterIsolated(list3);
		return VectorList.filterSame(list3);
	}

	// -----------------LIST OF LISTS OPERATIONS-----------------//

	// could not test
	/**
	 * Fringes the list by one level, i.e. ((1 2 3) (3 4 5)) (2 3 4)) = ((1 2 3)
	 * (3 4 5) 2 3 4) ????????????????????
	 */
	public static VectorList fringe_one(final Element elementList) {
		if (elementList == null) {
			return null;
		} else if (elementList instanceof VectorList) {
			final VectorList list = (VectorList) elementList;
			return VectorList.append((VectorList) list.head(),VectorList.fringe_one(list.tail()));
		} else {
			return new VectorList(elementList, null);
		}
	}

	public static VectorList appendHeads(final Object element, final VectorList list) {
		if (VectorList.isEmpty(list)) {
			return null;
		} else {
			return new VectorList(new VectorList(element, (VectorList) list.head()),
					VectorList.appendHeads(element, list.tail()));
		}
	}

	public static VectorList appendHeadList(final VectorList head, final VectorList list) {
		if (VectorList.isEmpty(list)) {
			return null;
		} else {
			return new VectorList(head.append((VectorList) list.head()),
					VectorList.appendHeadList(head, list.tail()));
		}
	}

	public static VectorList removeHeads(final VectorList list) {
		if (VectorList.isEmpty(list)) {
			return null;
		} else {
			return new VectorList(((VectorList) list.head()).tail(), VectorList.removeHeads(list.tail()));
		}
	}

	public static VectorList extractSameElements(final VectorList list1) {
		final VectorList list2 = VectorList.fringe_one(list1);
		return VectorList.exiter(list1, list2);
	}

	private static VectorList exiter(final VectorList list1, final VectorList list2) {
		if (list1 == null) {
			return list2;
		} else {
			final VectorList headList1 = (VectorList) list1.head();
			final VectorList lit = VectorList.intersection(list2, headList1);
			return VectorList.exiter(list1, lit);
		}
	}

	// --------------------ACCESSOR METHODS----------------------//
	public static boolean overlapExists(final VectorList list) {
		if (list == null) {
			return false;
		} else if (VectorList.member(list.head(), list.tail())) {
			return true;
		} else {
			return VectorList.overlapExists(list.tail());
		}
	}

	public static <T> boolean allSame(final VectorList<T> list) {
		if (list == null) {
			return false;
		} else if (list.tail() == null) {
			return true;
		} else if (VectorList.member(list.head(), list.tail())) {
			return VectorList.allSame(list.tail());
		} else {
			return false;
		}
	}

	public static <T> boolean isEmpty(final VectorList<T> list) {
		return (list == null);
	}

	public T head() {
		return this.head;
	}

	public VectorList<T> tail() {
		return this.tail;
	}

	/**
	 * @param n in range [1, ..., length()]
	 * @return
	 */
	public T getNth(final long n) {
		return this.listNth(n).head();
	}

	/**
	 * Gives the sublist from the nth position forward
	 * 
	 * @param n in range [1, ..., length()]
	 * starting position of the sublist
	 * 
	 * @return the sublist
	 */
	public VectorList<T> listNth(long n) {
		VectorList<T> list = this;
		for (; (list != null) && (n > 1); list = list.tail(), n--) {
		}
		return list;
	}
}