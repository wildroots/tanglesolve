package objects;
/**
 * Tangle Class 
 * @author Yuki Saka
 * Last Updated : April 15, 2013
 * Edited By : Crista Moreno
 *
 */
public class Tangle implements Element {
	
	// fields
	private Rational rational; 
	public static boolean rationalString = false;
	
	// constructors
	public Tangle() {
		
	}

	public Tangle(final Rational rational) {
		this.rational = rational;
	}

	public Tangle(final VectorList<Rational> list) {
		this.rational = Rational.toRational(list.reverse());
	}

	@Override
	public String toString() {
		if (Tangle.rationalString) {
			return this.rational.toString();
		} else {
			return this.rational.continuedFraction().reverse().toString();
		}
	}

	// ----------------------ACCESSOR METHODS------------------------//
	public Rational getRational() {
		return this.rational;
	}

	public VectorList<Rational> getVector() {
		return this.rational.continuedFraction().reverse();
	}

	/** 
	 *  @return the number of crossings
	 */
	public long getNumberCrossings() {
		return this.getNumberCrossings(this.getVector());
	}

	private long getNumberCrossings(final VectorList<Rational> tangleVector) {
		if (tangleVector == null) {
			return 0;
		} else {
			final long crossings = ((Rational) tangleVector.head()).getNumerator();
			return crossings + this.getNumberCrossings(tangleVector.tail());
		}
	}

	// ----------------------BEHAVIOR METHODS------------------------//
	public Tangle addTail(final long r) {
		return new Tangle(this.rational.add(r));
	}

	public Tangle mirror() {
		return new Tangle(this.rational.getNegative());
	}

	public Tangle rotate90() {
		return new Tangle(this.rational.getReciprocal().getNegative());
	}

	// ----------------------UTILITY METHODS------------------------//
	/**
	 * 
	 * @param parity
	 * @return the parity of this tangle
	 */
	private static Rational horizontal(final Rational parity) {
		if (parity.isZero()) {
			return new Rational(1, 1);
		} else if (parity.isOne()) {
			return new Rational(0, 1);
		} else {
			return parity;
		}
	}

	private static Rational vertical(final Rational parity) {
		if (parity.isOne()) {
			return new Rational(0, 0);
		} else if (parity.isInfinity()) {
			return new Rational(1, 1);
		} else {
			return parity;
		}
	}

	public Rational parity() {
		VectorList tangleVector = this.rational.continuedFraction().reverse();
		Rational rational; // was "init"
		final int tangleVectorLength = tangleVector.length();
		if (MathFunctions.even(tangleVectorLength)) {
			rational = new Rational(0, 0);
		} else { // tangle vector length is odd
			rational = new Rational(0, 1);
		}
		int i = 1;
		while (tangleVector != null) {
			if (((MathFunctions.odd(i) && MathFunctions.even(tangleVectorLength)) || (MathFunctions.even(i) && MathFunctions
					.odd(tangleVectorLength))) && MathFunctions.odd(((Rational) tangleVector.head()).getNumerator())) {
				rational = Tangle.vertical(rational);
			} else if (((MathFunctions.even(i) && MathFunctions.even(tangleVectorLength)) || (MathFunctions.odd(i) && MathFunctions
					.odd(tangleVectorLength))) && MathFunctions.odd(((Rational) tangleVector.head()).getNumerator())) {
				rational = Tangle.horizontal(rational);
			}
			tangleVector = tangleVector.tail();
			i++;
		}
		return rational;
	}

	/**
	 * Calls Object.clone() to give a field-to-field copy of the instance </p>
	 * 
	 * @return Object version of the clone
	 * */
	@Override
	public Tangle clone() {
		try {
			return (Tangle) super.clone();
		} catch (final Exception exception) {
			return null;
		}
	}
	// ----------------------EQUIVALENCE------------------------//
	@Override
	public boolean equals(final Object object) {
		if (object instanceof Tangle) {
			final Tangle tangle = (Tangle) object;
			return this.rational.equals(tangle.rational);
		} else { // not a Tangle object
			return false;
		}
	}
}
