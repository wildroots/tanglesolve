/**
 * Math functions
 * @author Yuki Saka
 * Last Updated : April 14, 2013
 * @edited by Crista Moreno
 */
package objects;

import javax.vecmath.Point2d;

public class MathFunctions {

	// ----------------------SIMPLE FUNCTIONS------------------------//
	/**
	 * Checks if the number is even.
	 * @param a
	 * @return true if a is even.
	 */
	public static boolean even(final long a) {
		return (a & 1) == 0;
	}

	/**
	 * Checks if the number number a is odd.
	 * @param a
	 * @return true if a is odd.
	 */
	public static boolean odd(final long a) {
		return !MathFunctions.even(a);
	}

	/**
	 * 
	 * @param a
	 * @return the sign of the number.
	 */
	public static int sign(final float a) {
		return (int) (a / Math.abs(a));
	}

	// ----------------------NUMBER THEORETIC FUNCTIONS------------------------//

	/**
	 * Mod function
	 * @param b
	 * @param n
	 * @return
	 */
	public static long mod(final long b, final long n) {
		if (b >= 0) {
			return b % n;
		} else {
			return n + (b % n);
		}
	}

	/**
	 * inverse1(a,n)
	 *  Definition : Given an integer a with (a, m) = 1, a solution of ax = 1 (mod m) is called
	 *  an inverse of a modulo m.
	 *  Finds the inverse of "b mod n" b is the dividend and n is the divisor
	 * @param b
	 * @param n
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static long moduloInverse1(long b, final long n) throws IllegalArgumentException {
		if (n == 0) {
			if (b == 1) {
				return 1;
			} else {
				throw new IllegalArgumentException("There is no modulo inverse for b mod 0");
			}
		}

		if (n == 1) {
			return 1;
		}

		if (b == 0) {
			throw new IllegalArgumentException("There is no modulo inverse for 0 mod n");
		}
		b = MathFunctions.mod(b, n);

		// b and n must be coprime
		if (Rational.getGreatestCommonDivisor(b, n) > 1) {
			throw new IllegalArgumentException("There is no modulo inverse for b and n not coprime");
		}

		// Extension of the Euclidean Algorithm to compute the inverse modulo
		long b0 = b;
		long n0 = n;
		long t0 = 0;
		long t = 1;
		long quotient = n0 / b0;
		long remainder = n0 - (quotient * b0);

		while (remainder > 0) {
			long temp = t0 - (quotient * t);
			if (temp >= 0) {
				temp = MathFunctions.mod(temp, n);
			}
			if (temp < 0) {
				temp = n - MathFunctions.mod(-temp, n);
			}
			t0 = t;
			t = temp;
			n0 = b0;
			b0 = remainder;
			quotient = n0 / b0;
			remainder = n0 - (quotient * b0);
		}
		if (b0 != 1) {
			throw new IllegalArgumentException("Error in execution of extended Euclidean algorithm");
		} else {
			return MathFunctions.mod(t, n);
		}
	}

	/**
	 * Finds the values x' and y' such that xx' - yy' = 1
	 * @param x
	 * @param y
	 * @return these values in the array answer
	 */
	public static long[] inverse2(final long x, final long y) {
		final long inverse;
		try {
			inverse = MathFunctions.moduloInverse1(x, y);
		} catch (IllegalArgumentException e) {
			return null;
		}

		final long[] answer = new long[2];
		answer[0] = inverse;
		answer[1] = ((x * answer[0]) - 1) / y;
		return answer;
	}

	/**
	 * Tests if a = b (mod n) 
	 * congruentMod(a,b,n)
	 * @param a
	 * @param b
	 * @param n
	 * @return
	 */
	public static boolean congruentModulo(final long a, final long b, final long n) {
		return (MathFunctions.mod(a - b, n) == 0);
	}

	/**
	 * solveQuadratic(a, b, n)
	 * Finds the solutions to at^2 = b (mod n)
	 * @param a
	 * @param b
	 * @param n
	 * @return
	 */
	public static long[] solveQuadratic(final long a, final long b, final long n) {
		final long[] solutions = new long[2];
		for (int i = 0; i < n; i++) {
			if (MathFunctions.congruentModulo(a * i * i, b, n)) {
				solutions[0] = i;
				solutions[1] = n - i;
				return solutions;
			}
		}
		return null;
	}

	/**
	 * solve2(a, b, e1, c, d, e2)
	 * (abs ax + by) = e1 and (abs cx + dy) = e2
	 * @param a
	 * @param b
	 * @param e1
	 * @param c
	 * @param d
	 * @param e2
	 * @return
	 */
	public static VectorList<VectorList<Rational>> solve2(final long a, final long b, final long e1,
			final long c, final long d, final long e2) {
		// rationals
		final Rational x1 = new Rational((d * e1) - (b * e2), (a * d) - (b * c));
		final Rational y1 = new Rational((a * e2) - (c * e1), (a * d) - (b * c));
		final Rational x2 = new Rational((-d * e1) - (b * e2), (-a * d) + (b * c));
		final Rational y2 = new Rational((a * e2) + (c * e1), (-a * d) + (b * c));
		final Rational x3 = new Rational((d * e1) + (b * e2), (-a * d) + (b * c));
		final Rational y3 = new Rational((-a * e2) - (c * e1), (-a * d) + (b * c));
		final Rational x4 = new Rational((-d * e1) + (b * e2), (a * d) - (b * c));
		final Rational y4 = new Rational((-a * e2) + (c * e1), (a * d) - (b * c));

		// Vector lists
		final VectorList<Rational> l1 = new VectorList<Rational>(x1, new VectorList<Rational>(y1, null));
		final VectorList<Rational> l2 = new VectorList<Rational>(x2, new VectorList<Rational>(y2, null));
		final VectorList<Rational> l3 = new VectorList<Rational>(x3, new VectorList<Rational>(y3, null));
		final VectorList<Rational> l4 = new VectorList<Rational>(x4, new VectorList<Rational>(y4, null));

		return new VectorList<VectorList<Rational>>(l1, new VectorList<VectorList<Rational>>(l2, new VectorList<VectorList<Rational>>(l3, new VectorList<VectorList<Rational>>(l4, null))));
	}

	// ----------------------PARTITION FUNCTIONS------------------------//
	public static VectorList<VectorList<Rational>> partitions(final long n) {
		return MathFunctions.partition(n, n, null);
	}

	private static VectorList<VectorList<Rational>> partition(final long n, final long lim, final VectorList<Rational> ans) {
		if (n > 0) {
			return MathFunctions.partiter(n, Math.min(n, lim), ans);
		} else {
			return new VectorList<VectorList<Rational>>(ans, null);
		}
	}

	private static VectorList<VectorList<Rational>> partiter(final long n, final long i, final VectorList<Rational> ans) {
		if (i == 0) {
			return null;
		} else {
			return MathFunctions.partition(n - i, i, new VectorList<Rational>(new Rational(i, 1), ans))
					.append(MathFunctions.partiter(n, i - 1, ans));
		}
	}

	// ----------------------PERMUTATION FUNCTIONS------------------------//
	/**
	 * 
	 * @param list
	 * @return 
	 */
	public static <T> VectorList<VectorList<T>> permute(final VectorList<T> list) {
		return MathFunctions.rPermute(list.length(), list);
	}

	/**
	 * 
	 * @param r
	 * @param list
	 * @return all possible permutations with r elements of the given list
	 */
	public static <T> VectorList<VectorList<T>> rPermute(final long r, final VectorList<T> list) {
		if ((list == null) || (r == 0)) {
			return new VectorList<VectorList<T>>(null, null);
		} else {

			return MathFunctions.accumPermute(r, list);
		}
	}

	private static <T> VectorList<VectorList<T>> accumPermute(final long r, final VectorList<T> list) {
		VectorList<T> filter = VectorList.filterSame(list);
		VectorList<VectorList<T>> permutationsList = null;
		for (; filter != null; filter = filter.tail()) {
			final VectorList<T> removeL = list.remove(filter.head());

			VectorList<VectorList<T>> permuteList = MathFunctions.rPermute(r - 1, removeL);
			for (; permuteList != null; permuteList = permuteList.tail()) {
				permutationsList = new VectorList<VectorList<T>>(new VectorList<T>(filter.head(), (VectorList<T>) permuteList.head()),
						permutationsList);
			}
		}
		return permutationsList;
	}


	public static Point2d calculateEllipse(Point2d center, Point2d pointA, Point2d pointB) {
		final double b;
		if (pointB.x == center.x) {
			b = Math.abs(center.y - pointB.y);
		} else {
			if (Math.abs(pointA.x - center.x) == Math.abs(pointB.x - center.x)) { // TODO DELTA
				b = Math.abs(pointA.x - center.x) * 1.25; // ???
			} else {
				final double box = Math.pow(pointA.x - center.x, 2) / Math.pow(pointB.x - center.x, 2);
				final double numerator = Math.pow(pointA.y - center.y, 2) - box * Math.pow(pointB.y - center.y, 2);
				final double denominator = 1 - box;
	
				b = Math.sqrt(numerator / denominator);
			}
		}
		final double a;
		if (pointA.y - center.y == b) {
			if (pointA.y - center.y == b) {
				throw new IllegalArgumentException("Infinitely many solutions!");
			} else {
				a = Math.sqrt(Math.pow(pointB.x - center.x, 2) / (1 - (Math.pow(pointB.y - center.y, 2) / Math.pow(b, 2))));
			}
		} else {
			final double numerator = Math.pow(pointA.x - center.x, 2);
			final double temp1 = Math.pow(pointA.y - center.y, 2);
			final double temp2 = Math.pow(b, 2);
			final double denominator = 1 - (temp1 / temp2);
			a = Math.sqrt(numerator / Math.abs(denominator));
		}

		return new Point2d(a, b);
	}
}
