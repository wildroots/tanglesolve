package objects;

import javax.swing.JComponent;

/**
 * <p>
 * Title: ToolTipString
 * </p>
 * 
 * <p>
 * Description: Provides a String that has setToolTipText method
 * </p>
 * 
 * @author Wenjing Zheng
 * @version 1.0
 */
public class ToolTipString extends JComponent {

	// constants 
	private static final long serialVersionUID = 8923926381290668949L;
	
	// fields
	private String display;
	
	// constructors
	public ToolTipString(final String display, final String tipText) {
		this.display = display;
	}

	public ToolTipString(final String display) {
		this.display = display;
	}

	@Override
	public String toString() {
		return this.display;
	}

	public void setDisplay(final String string) {
		this.display = string;
	}
}