/**
 * So Far:
 * lens space theorem and Hirasawa theorem work individually but do not show together
 * Display of theorem does not show repainted stuff
 *
 *@author Yuki Saka
 *@edited by Crista Moreno
 *
 */
package objects;

public class InputData {

	// fields
	public VectorList data;
	private int round = 0;
	private boolean Debug = true;

	public boolean processive;
	private VectorList input;

	// Data format for processive
	// ((products non-ordered fp1 fp2 .. or crossing#)
	// (first round products or crossing #
	// (second round ..)

	// constructor
	// Data format for non-processive
	// pls.head() = ((crossing# substrates); 
	// pls.tail() = (crossing# products))
	public InputData(final boolean processive, VectorList processiveList, final int rounds) {
		this.input = processiveList;
		try {
			this.processive = processive;
			if (processive) {
				// processive input handling
				this.processive = true;
				this.round = rounds;
				for (VectorList temp = processiveList; temp != null; temp = temp.tail()) {
					VectorList listHead = (VectorList) temp.head();
					VectorList list = null;
					if (listHead.head() instanceof VectorList) {
						for (VectorList tm = (VectorList) listHead.head(); tm != null; tm = tm.tail()) {
							final Rational rational = (Rational) tm.head();
							list = VectorList.append(list, FourPlat.enumerateFourPlats(rational.getNumerator()));
						}
					}
					listHead = VectorList.append(list, listHead.tail());
					temp.setHead(listHead);
				}
				if (this.Debug) {
					System.out.println("Input : " + processiveList);
				}
				this.data = InputData.permuteList2(processiveList);
				// If processive order not given, data is all possible pairs of
				// (substrate, 1st product)

				// filter
			} else {
				// Nonprocessive input handling
				this.processive = false;
				VectorList substrateList = (VectorList) processiveList.head();
				VectorList productList = (VectorList) processiveList.tail().head();

				if ((substrateList.head() != null) && (substrateList.head() instanceof VectorList)) {
					// first element is a crossing number, translate crossing number
					// into list of four-plats
					VectorList list = null;
					for (VectorList tm = (VectorList) substrateList.head(); tm != null; tm = tm.tail()) {
						final Rational rational = (Rational) tm.head();
						list = VectorList.append(list, FourPlat.enumerateFourPlats(rational.getNumerator()));
					}
					substrateList = VectorList.append(list, substrateList.tail());
					// this is list of substrates with the given number of crossings
				}
				if ((productList.head() != null) && (productList.head() instanceof VectorList)) {
					// first element is a crossing number
					VectorList list = null;
					for (VectorList tm = (VectorList) productList.head(); tm != null; tm = tm.tail()) {
						final Rational rational = (Rational) tm.head();
						list = VectorList.append(list, FourPlat.enumerateFourPlats(rational.getNumerator()));
					}
					productList = VectorList.append(list, productList.tail());
					// this is list of products with given number of crossings
				}

				if (substrateList.head() == null) {
					substrateList = substrateList.tail();
				}

				if (productList.head() == null) {
					productList = productList.tail();
				}

				processiveList = new VectorList(substrateList, new VectorList(productList, null));
				// ///This processiveList has all substrates and products as list of four plats

				this.data = InputData.permuteList2(processiveList);
				// data contains all possible (substrate, product) pairs from processiveList

			}
		} catch (final Exception exception) {
			exception.printStackTrace();
		}
	}

	// SOLVES THE DATA
	public VectorList solve() {
		EquationSystem.resetTotalRationalSystems();
		try {
			if (this.data == null) {
				System.err.println("WARNING: Looks like there is no solution to system: '" + this.input.toString() + "'.");
				return null;
			}
			final VectorList solution = (VectorList) this.data.clone();
			VectorList returnList = null;

			// SOLVING PROCESSIVE
			if (this.processive) {
				if (this.Debug) {
					System.out.println(this.data);
				}
				for (VectorList temp = solution; temp != null; temp = temp.tail()) {
					final VectorList list = (VectorList) temp.head();
					final FourPlat sub = (FourPlat) list.head();

					if (sub.getRational().isOne() && (list.length() >= 3)) {
						final VectorList solution1 = EquationSolverUtil.solve2((FourPlat) list.getNth(2), (FourPlat) list.getNth(3), 1, 2);
						final VectorList solution2 = EquationSolverUtil.solve3((FourPlat) list.getNth(2), (FourPlat) list.getNth(3));

						VectorList solutions = VectorList.union(solution1, solution2);
						solutions = this.filterSolution(solutions, list);
						if (solutions != null) {
							returnList = new VectorList(new VectorList(list, solutions), returnList);
						}
					} else {
						VectorList sol = EquationSolverUtil.solve4((FourPlat) list.getNth(1), (FourPlat) list.getNth(2));
						sol = this.filterSolution(sol, list);
						if (sol != null) {
							returnList = new VectorList(new VectorList(list, sol), returnList);
						}
					}
				}
				returnList = InputData.addProdRounds(returnList, this.round - 1);

			}// END OF PROCESSIVE

			// SOLVING NONPROCESSIVE
			else {

				for (VectorList temp = solution; temp != null; temp = temp.tail()) {
					// list is every substrate-product pair, so has length = 2
					final VectorList list = (VectorList) temp.head();
					final FourPlat fourPlat1 = (FourPlat) list.head();
					final FourPlat fourPlat2 = (FourPlat) list.tail().head();
					final VectorList sol = EquationSolverUtil.solve4(fourPlat1, fourPlat2);
					if (sol != null) {
						// add solutions to returnList
						returnList = new VectorList(new VectorList(list, sol), returnList);

					}
				}
				// head of each node of _ret has form: (substrate, product)
				// tail of each node of _ret contains all possible solutions O
				// and R has form: ( (O, R), (O,R), ..)

			}// END OF NONPROCESSIVE

			final VectorList returnClone = (VectorList) returnList.clone();
			VectorList pointReturn = returnClone; // pointer for returnList

			// first node of returnList to create first node of list
			final VectorList system1 = (VectorList) ((VectorList) pointReturn.head()).head(); // (substrate-product)
			// in returnList
			final VectorList solns1 = ((VectorList) pointReturn.head()).tail();

			// list to have each node as (EqnSystem, sol in VectorList)
			VectorList list = new VectorList(new VectorList(new EquationSystem(system1, this.processive), solns1), null);

			// now proceed to the rest of the nodes
			for (pointReturn = returnClone.tail(); pointReturn != null; pointReturn = pointReturn.tail()) {
				// head of each node is _system, has form: (substrate, prod1, prod2, prod3, ..)
				// tail of each node is solution for O and R.
				final VectorList system = (VectorList) ((VectorList) pointReturn.head()).head(); // (substrate-product)
				final VectorList solist = ((VectorList) pointReturn.head()).tail(); // solutions for O and R

				final EquationSystem syst = new EquationSystem(system, this.processive);
				final VectorList next = new VectorList(syst, solist);
				list = list.append2(new VectorList(next, null));
			}
			// each node of list has the form (system, solist)
			return list;
		}// end of try

		catch (final Exception exception) {
			exception.printStackTrace();
			System.out.println("Exception at solve " + exception);
			return null;
		}
	}// END OF SOLVE

	private static VectorList addProdRounds(final VectorList sol, final int round) {
		try {
			VectorList ret = null;
			for (VectorList temp = sol; temp != null; temp = temp.tail()) {
				final VectorList set = (VectorList) temp.head();
				final VectorList fourPlatList = (VectorList) set.head();
				final VectorList tsol = set.tail();
				final int length = fourPlatList.length();
				if (round > length) {
					for (VectorList t2 = tsol; t2 != null; t2 = t2.tail()) {
						VectorList fpadd = null;
						final VectorList tgset = (VectorList) t2.head();
						final Tangle ofTangle = (Tangle) tgset.head();
						final Tangle obTangle = (Tangle) tgset.getNth(2);
						final Tangle kTangle = (Tangle) tgset.getNth(3);
						if (kTangle.getRational().isInteger()) {
							for (int i = length; i <= round; i++) {
								final FourPlat nf = new FourPlat(ofTangle, obTangle.addTail(kTangle.getRational().getNumerator() * i));
								fpadd = new VectorList(nf, fpadd);
							}
							fpadd = fpadd.reverse();
						}
						fpadd = VectorList.append(fourPlatList, fpadd);
						fpadd = new VectorList(fpadd, new VectorList(tgset, null));
						ret = new VectorList(fpadd, ret);
					}
				} else {
					ret = new VectorList(temp.head(), ret);
				}
			}
			return ret;
		} catch (final Exception exception) {
			System.out.println(exception + " at addProdRounds");
			return null;
		}
	}

	private VectorList filterSolution(final VectorList solution, final VectorList list) {
		if (solution == null) {
			return null;
		} else if (this.isSolution((VectorList) solution.head(), list, 0)) {
			return new VectorList(solution.head(), this.filterSolution(solution.tail(), list));
		} else {
			return this.filterSolution(solution.tail(), list);
		}
	}

	private boolean isSolution(final VectorList solution, final VectorList list, int round) {
		if (list == null) {
			return true;
		} else {
			try {
				final Tangle ofTangle = (Tangle) solution.head();
				Tangle obTangle = (Tangle) solution.getNth(2);
				final Tangle kTangle = (Tangle) solution.getNth(3);

				final FourPlat fourPlat = (FourPlat) list.head();

				if (kTangle.getRational().isInteger()) {
					obTangle = obTangle.addTail(kTangle.getRational().getNumerator() * round);
					return fourPlat.equals(new FourPlat(ofTangle, obTangle)) && this.isSolution(solution, list.tail(), ++round);
				} else if (obTangle.getRational().isInteger()) {
					if (round == 0) {
						return fourPlat.equals(new FourPlat(obTangle, new Tangle(new Rational(0)))) && this.isSolution(solution, list.tail(), ++round);
					} else if (round == 1) {
						return fourPlat.equals(new FourPlat(obTangle, kTangle)) && this.isSolution(solution, list.tail(), ++round);
					} else if (round == 2) {
						return fourPlat.equals(new FourPlat(obTangle, kTangle.addTail(obTangle.getRational().getNumerator()))) && this.isSolution(solution, list.tail(), ++round);
					} else {
						return false;
					}
				} else {
					return false;
				}
			} catch (final Exception exception) {
				System.out.println(exception);
				return false;
			}
		}
	}

	@Override
	public String toString() {
		return this.data.toString();
	}

	/**
	 * Given a list containing many possible substrate and products, returns all
	 * possible (substrate, product) pairs.
	 * 
	 */
	public static VectorList permuteList2(final VectorList list) {
		if (list == null) {
			return null;
		} else {
			final VectorList listHead = (VectorList) list.head();
			final VectorList rest = InputData.permuteList2(list.tail());
			VectorList returnList = null;
			for (VectorList substrate = listHead; substrate != null; substrate = substrate.tail()) {
				if (rest == null) {
					returnList = new VectorList(new VectorList(substrate.head(), null), returnList);
				} else {
					for (VectorList product = rest; product != null; product = product.tail()) {
						final Rational substrateRational = ((FourPlat) substrate.head()).getRational();
						final boolean haveSameCrossings = InputData.hasSameCrossings((FourPlat) substrate.head(), (VectorList) product.head());
						if (substrateRational.isOne() || !haveSameCrossings) {
							returnList = new VectorList(new VectorList(substrate.head(), (VectorList) product.head()), returnList);
						}
					}
				}
			}
			return returnList;
		}
	}

	public static boolean hasSameCrossings(final FourPlat element, VectorList list) {

		for (; list != null; list = list.tail()) {
			if (element.getNumberCrossings() == ((FourPlat) list.head()).getNumberCrossings()) {
				return true;
			}
		}
		return false;
	}
}
