package objects;

/**
 * <p>
 * Title: TangleTheorems
 * </p>
 * <p>
 * Description: Provides static methods to verify whether given system of tangle
 * equations with 4-plat substrate and products should have rational solutions
 * </p>
 * <p>
 * Copyright: Wenjing Zheng 2003
 * </p>
 * 
 * @author Wenjing Zheng
 * @version 1.0
 */

public class TangleTheorems {

	// ----------------------NONPROCESSIVE------------------------//
	/**
	 * Check if given substrate and product in the system of tangle equations in
	 * non-processive recombinaton imply rational solutions
	 * 
	 * @param system
	 *            a VectorList of two FourPlats
	 * @return String
	 */
	public static String checkNonProcessive(final VectorList system) {
		if (system.length() != 2) {
			return null;
		}

		String result = null;
		final FourPlat fourPlat1 = (FourPlat) system.head();
		final FourPlat fourPlat2 = (FourPlat) system.tail().head();

		final Rational rational1 = fourPlat1.getRational(); // rational1 = b1/a1
		final long a1 = rational1.getDenominator();
		final long b1 = rational1.getNumerator();
		final Rational rational2 = fourPlat2.getRational();
		final long a2 = rational2.getDenominator();
		final long b2 = rational2.getNumerator();

		// check for HirasawaTheorem:
		// First condition rational=1 and a2 is even
		if ((a1 == 1) & (b1 == 1) & ((a2 % 2) == 0)) {
			result = TangleTheorems.checkHirasawaTheorem(a1, b1, a2, b2);
			if (result != null) {
				return result;
			}
		}
		return result;
	}

	/**
	 * 
	 * @param system
	 *            VectorList a VectorList of Four Plats
	 * @return String theorem or null if none apply
	 */
	public static String checkProcessive(final VectorList<FourPlat> system) {
		String result = null;

		try {
			if ((!(system.head() instanceof FourPlat))
					|| (system.length() <= 2)) {
				return null;
			}

			final FourPlat substrate = (FourPlat) system.head();
			final FourPlat fourPlat1 = (FourPlat) system.getNth(2); // b(a,b)
			final FourPlat fourPlat2 = (FourPlat) system.getNth(3); // b(a', b')

			// if substrate is unknot, check for DenomP1 condition
			if ((substrate.getRational().getNumerator() == 1)
					&& (substrate.getRational().getDenominator() == 1)) {
				result = TangleTheorems.checkDenomP1(substrate, fourPlat1, fourPlat2);
			}

			// if doesn't satisfy DenomP1's condition, then check for theorem 2.2 in
			// [ES]
			if (result == null) {
				if (system.length() >= 4) {
					result = TangleTheorems.checkES96Theorem2_2(system);
				}
			}

		}// end of try
		catch (final Exception exception) {
			System.out.println("Exeception in checkProcessive: " + exception);
		}
		return result;
	}

	/**
	 * Checks if given two rat1 and rat2 satisfy conditions of Hirasawa Theorem
	 * @param a1
	 * @param b1
	 * @param a2
	 * @param b2
	 * @return null if it does not satisfy conditions of Hirasawa Theorem
	 * assumes a1 = b1 = 1
	 */
	private static String checkHirasawaTheorem(final long a1, final long b1,
			final long a2, final long b2) {
		String theoremResult = null;

		// doublecheck conditions
		if ((a1 != 1) || (b1 != 1) || ((a2 % 2) != 0)) {
			return theoremResult;
		}

		// safely assume a2 is even, a1=b1=1:
		final boolean isom = TangleTheorems.isIsomorphicLensSpace(a1, b1, a2, b2); // check if L(a1, b1) ~ L(a2, b2

		if ((b2 == 1) | (isom)) {
			theoremResult = TheoremInterface.HIRASAWA_THEOREM;
		}
		return theoremResult;
	}

	/**
	 * Checks if b(a1, b1) ~ b(a2, b2)
	 * 4-Plat Classification Theorem
	 * Two 4-plats b(a1, b1) and b(a2, b2) are equivalent (as unoriented knots and links) 
	 * if and only if a1 = a2 and b1^(+- 1) = b2 mod a1.
	 * @param a1
	 * @param b1
	 * @param a2
	 * @param b2
	 * @return
	 */
	public static boolean isIsomorphicFourPlat(final long a1, final long b1, final long a2, final long b2) {
		if (a1 != a2) {
			return false; // Assume a1 = a2
		}

		final long b1_mod = (b1 % a1); // b1 in Z_a1
		long b1_modinv = -1; // inv b1 in Z_a1
		// find if b1 has inverse in a1
		for (int i = 1; i < a1; i++) {
			if (((i * b1_mod) % a1) == 1) {
				b1_modinv = i;
				break;
			}
		}
		if (((b2 % a2) == b1_mod) || ((b1_modinv > 0) && ((b2 % a2) == b1_modinv))) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if L(a1, b1) ~ L(a2, b2)
	 * @param a1
	 * @param b1
	 * @param a2
	 * @param b2
	 * @return
	 */
	public static boolean isIsomorphicLensSpace(final long a1, final long b1,
			final long a2, final long b2) {
		if (a1 != a2) {
			return false;
			// Assume a1 = a2
		}

		final long b1_mod = (b1 % a1); // b1 in Z_a1
		long b1_modinv = -1; // inv b1 in Z_a1
		// find if b1 has inverse in a1
		for (int i = 1; i < a1; i++) {
			if (((i * b1_mod) % a1) == 1) {
				b1_modinv = i;
				break;
			}
		}
		if (((b2 % a2) == b1_mod) || ((b2 % a2) == (a2 - b1_mod))) {
			return true;
		}
		if (((b1_modinv > 0) && (((b2 % a2) == b1_modinv) || ((b2 % a2) == (a2 - b1_modinv))))) {
			return true;
		}

		return false;
	}

	private static String checkDenomP1(final FourPlat substrate,
			final FourPlat fourPlat1, final FourPlat fourPlat2) {
		// if substrate != unknot
		if ((substrate.getRational().getNumerator() != 1)
				| (substrate.getRational().getDenominator() != 1)) {
			return null;
		}
		// if(|a-a'| <= 1)
		if (Math.abs(fourPlat1.getRational().getDenominator() - fourPlat2.getRational().getDenominator()) <= 1) {
			return null;
		}

		String theoremResult = null;
		final long alpha = fourPlat1.getRational().getDenominator();
		if ((alpha == 2) || (alpha == 3) || (alpha == 4)) {
			theoremResult = TheoremInterface.SPECIAL_P1_DENOM;
		}
		return theoremResult;
	}

	// Assumptions: system is a VectorList of FourPlats
	private static String checkES96Theorem2_2(final VectorList<FourPlat> system) {

		// double check system has at least 4 equations
		if (system.length() < 4) {
			return null;
		}

		VectorList<FourPlat> products = null;
		for (int i = 4; i >= 2; i--) {
			products = new VectorList<FourPlat>(system.getNth(i), products);
		}

		System.out.println("first 3 products in checkTheorem2.2: " + products);
		final VectorList<FourPlat> filtered = VectorList.filterSame(products);

		// if have less than 2 different knot types in products
		if ((filtered == null) || (filtered.length() < 2)) {
			return null;
		}// assertion: all elements in system are the different

		return TheoremInterface.ES96_THEOREM_2_2;
	}
}