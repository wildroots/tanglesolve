package objects;

/**
 * <p>
 * Title: EBool
 * </p>
 * <p>
 * Description: Wrapper class for boolean that is cloneable
 * </p>
 * <p>
 * Copyright: Yuki Saka
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author Yuki Saka
 * @edited by Crista Moreno
 * @version 1.0
 */

public class BoolElement implements Element {
	
	// private fields
	private boolean booleanElement;

	// constructor
	public BoolElement(final boolean b) {
		this.booleanElement = b;
	}

	public boolean bool() {
		return this.booleanElement;
	}

	/**
	 * Calls Object.clone() to give a field-to-field copy of the instance </p>
	 * 
	 * @return Object version of the clone
	 * */
	@Override
	public BoolElement clone() {
		try {
			return (BoolElement) super.clone();
		} catch (final Exception exception) {
			return null;
		}
	}
	
	//-------------------EQUIVALENCE ----------------//
	/**
	 * @param object
	 * Assumed to be an instance of BooleanElement
	 * 
	 * @return true if value this BooleanElement is equal to that of the argument's
	 */
	@Override
	public boolean equals(final Object object) {
		if (object instanceof BoolElement) {
			return this.booleanElement == ((BoolElement) object).bool();
		} else { // not a BoolElement object
			return false;
		}
	}
}
