// Yuki Saka
// Parser class

// This class parses a string and evaluates it, returning the output

/* The language is similar to lisp
 The type of expressions are:
 Self-Evaluating:
 Integers      1,3,4 etc
 Rational numbers 2/3, -3/4 etc
 Applications:
 (Function_Name arg1 arg2 arg3 ... argn)
 */
package objects;

import java.util.Hashtable;

public class Parser {

	//constants 
	private static final String NO_OBJECT = "OBJECT TYPE NOT FOUND";
	private static final String NO_COMMAND = "COMMAND TYPE NOT FOUND";
	
	// fields
	public static boolean isInitialized = false;
	
	// Algebraic operations
	// String tables
	// Error tables
	private static Hashtable<String, Command> commandTable;
	private static boolean Debug = false;

	public static void main(final String[] args) {
		Parser.initialize();
		try {
			System.out.println(Parser.extractHead(0, "expand(2/3)"));
			System.out.println(Parser.extractHead(0, "b<2, 3, 4>"));
			System.out.println(Parser.extractHead(7, "expand(torat(2,3))"));
			System.out.println(Parser.evaluate("(2 3 2)"));
			System.out.println(Parser.evaluate("expand(torat(2,3))"));
			System.out.println(Parser.evaluate("expand(2/3)"));
			System.out.println(Parser.evaluate("torat(2,3)"));
			System.out.println(Parser.evaluate("fplist((2 3 2))"));
			System.out.println(Parser.evaluate("list(2,3)"));
		} catch (final Exception exception) {
			System.out.println(exception);
		}
	}

	public static void initialize() {
		Parser.commandTable = new Hashtable<String, Command>();
		Parser.commandTable.put(Command.QUOTE, new Command(Command.QUOTE));
		Parser.commandTable.put(Command.EXPAND, new Command(Command.EXPAND));
		Parser.commandTable.put(Command.TO_RATIONAL, new Command(Command.TO_RATIONAL));
		Parser.commandTable.put(Command.SOLVE, new Command(Command.SOLVE));
		Parser.commandTable.put(Command.FOUR_PLAT_LIST, new Command(Command.FOUR_PLAT_LIST));
		Parser.commandTable.put(Command.TANGLE_LIST, new Command(Command.TANGLE_LIST));
		Parser.commandTable.put(Command.LIST, new Command(Command.LIST));
		Parser.commandTable.put(Command.CONWAY, new Command(Command.CONWAY));
	}

	/**
	 * 
	 * @param expressionString
	 * @return either a list with multiple items or single item.
	 * @throws IllegalArgumentException
	 */
	public static Object evaluate(String expressionString) throws IllegalArgumentException {
		expressionString = expressionString.toLowerCase();

		if (Parser.Debug) {
			System.out.println("Evaluating " + expressionString);
		}

		if (Parser.isSelfEval(expressionString)) {
			return Parser.selfEval(expressionString);
		} else {
			final VectorList<String> exp = Parser.expressionToList(expressionString);
			return Parser.apply(exp);
		}
	}

	// ----------------------PARSER SPECIFICATIONS------------------------//

	/**
	 * @return true is expression is open.
	 */
	public static boolean isOpen(final char c) {
		if ((c == '(') || (c == '<') || (c == '[')) {
			return true;
		} else {
			return false;
		}
	}
	
	/** 
	 * @return true is expression is closed. 
	 */
	public static boolean isClosed(final char c) {
		if ((c == ')') || (c == '>') || (c == ']')) {
			return true;
		} else {
			return false;
		}
	}

	// ----------------------AUXILIARY METHODS------------------------//
	/**
	 * @return false if it is a list, true otherwise
	 */
	public static boolean isSelfEval(String s) {
		s = s.trim();
		for (int i = 0; i < s.length(); i++) {
			if (Parser.isOpen(s.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	// precondition: s must be a list i.e. of form fn(a, b, c, d)
	public static VectorList<String> expressionToList(String s) {
		// first extract function header
		VectorList<String> list = null;
		s = s.trim();
		s = Parser.extractHead(0, s);
		s = s.substring(1, s.length() - 1);
		int i = 0;
		while (i < s.length()) {
			if (!Parser.isDelimitter(s.charAt(i))) {
				if (Parser.parenthesesComing(i, s)) {
					final int init = i;
					for (; i < s.length(); i++) {
						if (Parser.isOpen(s.charAt(i))) {
							break;
						}
					}

					// traverse until we see right parentheses
					int j = i;
					int parentheses = 0;
					while ((j < s.length()) && (Parser.isOpen(s.charAt(j)) || (parentheses >= 1))) {
						// there is still remaining parentheses
						if (Parser.isClosed(s.charAt(j))) {
							parentheses--;
						}
						if (Parser.isOpen(s.charAt(j))) {
							parentheses++;
						}
						j++;
					}
					list = new VectorList<String>(s.substring(init, j), list);
					i = j + 1;
				} else {
					int j = i;
					while ((j < s.length()) && !Parser.isDelimitter(s.charAt(j))) {
						j++;
					}
					list = new VectorList<String>(s.substring(i, j), list);
					i = j + 1;
				}
			} else {
				i++;
			}
		}
		return list.reverse();
	}

	private static boolean parenthesesComing(int i, final String s) {
		for (; i < s.length(); i++) {
			if (Parser.isOpen(s.charAt(i))) {
				return true;
			} else if (Parser.isDelimitter(s.charAt(i))) {
				return false;
			}
		}
		return false;
	}

	private static String extractHead(int i, String s) {
		final int init = i;
		final String string = s.substring(0, i);
		for (; i < s.length(); i++) {
			if (Parser.isOpen(s.charAt(i))) {
				break;
			}
		}
		String header;
		if (i == init) {
			header = "'";
		} else {
			// function header
			header = s.substring(init, i);
		}
		s = string + "(" + header + " " + s.substring(i + 1);
		return s;

	}

	private static boolean isDelimitter(final char c) {
		return ((c == ' ') || (c == '\t') || (c == '\n') || (c == '\r')
				|| (c == '\f') || (c == ','));
	}

	// ----------------------EVALUATION METHODS------------------------//
	private static Object selfEval(String s) throws IllegalArgumentException {
		s = s.trim();

		if (s.length() == 0) {
			return null;
		} else if (Rational.isRational(s)) {
			return new Rational(s);
		} else if (Parser.commandTable.get(s) != null) {
			if (Parser.Debug) {
				System.out.println(s + " procedure");
			}
			return Parser.commandTable.get(s);
		} else {
			return Parser.NO_OBJECT;
		}

	}

	public static Object apply(final VectorList<String> exp) throws IllegalArgumentException {

		// Have to evaluate the arguments
		VectorList<Object> args = null;
		for (VectorList<String> p = exp.tail(); p != null; p = p.tail()) {
			args = new VectorList<Object>(Parser.evaluate((String) (p.head())), args);
		}
		args = args.reverse();

		if (Parser.Debug) {
			System.out.println("Argument is" + args);
		}
		try {

			final Object c = Parser.evaluate((String) exp.head());
			if ((c == null) || !(c instanceof Lambda)) {
				return new String(Parser.NO_COMMAND);
			} else {
				return ((Lambda) c).envoke(args);
			}
		} catch (final Exception exception) {
			return exception;
		}
	}
}
