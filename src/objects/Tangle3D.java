package objects;

import java.util.ArrayList;
import java.util.List;

import javax.media.j3d.Node;
import javax.media.j3d.TransformGroup;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Color3f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

public class Tangle3D extends TransformGroup {
	private static final float TWIST_PADDING_HORIZONTAL = 10;
	private static final float TWIST_PADDING_VERTICAL = 10;

	private static final Vector3f AXIS_X = new Vector3f(1, 0, 0);
	private static final Vector3f AXIS_Y = new Vector3f(0, 1, 0);
	private static final Vector3f AXIS_Z = new Vector3f(0, 0, 1);

	private static final float SPHERE_RADIUS = 0.0055f * 2;
	private static final float CYLINDER_RADIUS = 0.005f * 2;

	// private fields
	private final Tangle tangle;

	private final Color3f firstStrandColor;
	private final Color3f secondStrandColor;

	private final float scale;
	private final float crossingSize;
	private final int crossingResolution;
	private final float crossingHelixRadius;

	// constructor

	public Tangle3D(Tangle tangle, final int crossingResolution, final float crossingSize, final float crossingHelixRadius, final float scale) {
		this(tangle, new Color3f(0, 0, 0), new Color3f(0, 0, 0), crossingResolution, crossingSize, crossingHelixRadius, scale);
	}

	public Tangle3D(Tangle tangle, Color3f firstStrandColor, Color3f secondStrandColor, final int crossingResolution, final float crossingSize,final float crossingHelixRadius, final float scale) {
		this.tangle = tangle;
		this.firstStrandColor = firstStrandColor;
		this.secondStrandColor = secondStrandColor;
		this.crossingResolution = crossingResolution;
		this.crossingSize = crossingSize;
		this.crossingHelixRadius = crossingHelixRadius;
		this.scale = scale;

		this.init();
	}

	public void init() {
		/* 1. Calculate Twist objects: */

		/* The theorem of ___ says that every rational tangle can be drawn as an
		 * alternating sequence of vertical and horizontal twists. We always
		 * want to end on a horizontal one, so we determine which orientation we
		 * have to start in to get there: */
		TwistOrientation twistOrientation;
		if (this.hasOddNumberOfTwists()) {
			twistOrientation = TwistOrientation.HORIZONTAL;
		} else {
			twistOrientation = TwistOrientation.VERTICAL;
		}

		float x = 0;
		float y = 0;

		final List<Twist> twists = new ArrayList<Twist>();

		final VectorList tangleList = tangle.getVector();
		for (int i = 1; i <= tangleList.length(); i++) {
			final Rational rational = (Rational) tangleList.getNth(i);
			final CrossingType crossingType = CrossingType.fromRational(rational);
			final long numberOfCrossings = Math.abs(rational.getNumerator());

			/* Determine where the twist starts, depending on its orientation and how: */
			final float twistX;
			final float twistY;
			final float twistWidth;
			final float twistHeight;
			switch (twistOrientation) {
				case HORIZONTAL:
					twistX = x;
					twistY = 0;
					twistWidth = Twist.getSize(TwistOrientation.HORIZONTAL, numberOfCrossings, this.crossingSize);
					if (i == 1) {
						twistHeight = TWIST_PADDING_VERTICAL + this.crossingSize + TWIST_PADDING_VERTICAL;
					} else {
						twistHeight = y;
					}
					break;
				case VERTICAL:
					twistX = 0;
					twistY = y;
					if (i == 1) {
						twistWidth = TWIST_PADDING_HORIZONTAL + this.crossingSize + TWIST_PADDING_HORIZONTAL;
					} else {
						twistWidth = x;
					}
					twistHeight = Twist.getSize(TwistOrientation.VERTICAL, numberOfCrossings, this.crossingSize);
					break;
				default:
					throw new IllegalStateException();
			}

			/* Create Twist: */
			int twistIndex = i - 1;
			final Twist twist = new Twist(twistIndex, twistX, twistY, twistWidth, twistHeight, twistOrientation, crossingType, numberOfCrossings, this.crossingResolution, this.crossingSize, this.crossingHelixRadius, this.scale);
			twists.add(twist);

			/* Update x and y: */
			switch (twistOrientation) {
				case HORIZONTAL:
					x += twist.width;
					y = twistY + twist.height;
					break;
				case VERTICAL:
					x = twistX + twist.width;
					y += twist.height;
					break;
				default:
					throw new IllegalStateException();
			}

			/* Alternate the orientation of the next twist: */
			twistOrientation = twistOrientation.alternate();
		}

		/* 2. Trace the two strands through the twists: */
		/* 2.1. Trace first strand from top left: */
		Twist twist = twists.get(0);
		TwistCorner twistCorner = TwistCorner.TOP_LEFT;

		final List<TwistAndTwistCorner> firstStrandTrace = new ArrayList<TwistAndTwistCorner>();
		firstStrandTrace.add(new TwistAndTwistCorner(twist, twistCorner));

		Twist secondStrandStartTwist = null;
		TwistCorner secondStrandStartTwistCorner = null;

		while (twist != null) {
			twistCorner = twist.traceCorner(twistCorner);

			try {
				final TwistAndTwistCorner twistAndTwistCorner = this.getNextTwistAndTwistCorner(twists, twist, twistCorner);
				firstStrandTrace.add(twistAndTwistCorner);

				twist = twistAndTwistCorner.twist;
				twistCorner = twistAndTwistCorner.twistCorner;
			} catch (final TopLeftCornerReachedException e) {
				throw new IllegalStateException(e);
			} catch (final TopRightCornerReachedException e) {
				secondStrandStartTwist = twists.get(twists.size() - 1);
				secondStrandStartTwistCorner = TwistCorner.BOTTOM_RIGHT;
				break;
			} catch (final BottomLeftCornerReachedException e) {
				secondStrandStartTwist = twists.get(twists.size() - 1);
				secondStrandStartTwistCorner = TwistCorner.TOP_RIGHT;
				break;
			} catch (final BottomRightCornerReachedException e) {
				secondStrandStartTwist = twists.get(twists.size() - 1);
				secondStrandStartTwistCorner = TwistCorner.TOP_RIGHT;
				break;
			}
		}

		/* 2.2. Trace second strand from where ever the first one didn't start or end: */
		twist = secondStrandStartTwist;
		twistCorner = secondStrandStartTwistCorner;

		final List<TwistAndTwistCorner> secondStrandTrace = new ArrayList<TwistAndTwistCorner>();
		secondStrandTrace.add(new TwistAndTwistCorner(twist, twistCorner));

		while (twist != null) {
			twistCorner = twist.traceCorner(twistCorner);

			try {
				final TwistAndTwistCorner twistAndTwistCorner = this.getNextTwistAndTwistCorner(twists, twist, twistCorner);
				secondStrandTrace.add(twistAndTwistCorner);

				twist = twistAndTwistCorner.twist;
				twistCorner = twistAndTwistCorner.twistCorner;
			} catch (final TopLeftCornerReachedException e) {
				if (secondStrandStartTwistCorner == TwistCorner.TOP_LEFT) {
					throw new IllegalStateException(e);
				} else {
					break;
				}
			} catch (final TopRightCornerReachedException e) {
				if (secondStrandStartTwistCorner == TwistCorner.TOP_RIGHT) {
					throw new IllegalStateException(e);
				} else {
					break;
				}
			} catch (final BottomLeftCornerReachedException e) {
				if (secondStrandStartTwistCorner == TwistCorner.BOTTOM_LEFT) {
					throw new IllegalStateException(e);
				} else {
					break;
				}
			} catch (final BottomRightCornerReachedException e) {
				if (secondStrandStartTwistCorner == TwistCorner.BOTTOM_RIGHT) {
					throw new IllegalStateException(e);
				} else {
					break;
				}
			}
		}

		firstStrandVertices = this.getVertices(firstStrandTrace);
		secondStrandVertices = this.getVertices(secondStrandTrace);

		/* 3.1. Normalize the vertices into a x=[0, 1] and y[0, 1] interval */
		Tangle3DUtils.normalize(firstStrandVertices, new Point2f(x, y));
		Tangle3DUtils.normalize(secondStrandVertices, new Point2f(x, y));

		/* 4. Transform coordinates to tetrahedron space: */
		tetrahedronize(firstStrandVertices);
		tetrahedronize(secondStrandVertices);

		/* 5. Create Nodes from transformed vertices: */
		final Node firstStrand = this.createStrandNode(firstStrandVertices, this.firstStrandColor);
		final Node secondStrand = this.createStrandNode(secondStrandVertices, this.secondStrandColor);

		/* Add Strands to ourself (since we are a TransformationGroup): */
		this.addChild(firstStrand);
		this.addChild(secondStrand);
	}

	private static final float DELTA = 0.0001f;

	private List<Point3f> firstStrandVertices;
	private List<Point3f> secondStrandVertices;

	public List<Point3f> getFirstStrandVertices() {
		return firstStrandVertices;
	}

	public List<Point3f> getSecondStrandVertices() {
		return secondStrandVertices;
	}

	private void tetrahedronize(List<Point3f> vertices) {
		for (final Point3f vertex : vertices) {
			final Point3f p = xy_to_xyz(vertex.x, vertex.y);

			final Point3f a = xy_to_xyz(vertex.x + DELTA, vertex.y);
			final Point3f b = xy_to_xyz(vertex.x, vertex.y + DELTA);

			final Vector3f pa = Tangle3DUtils.subtract(a, p);
			final Vector3f pb = Tangle3DUtils.subtract(b, p);

			final Vector3f normal = Tangle3DUtils.cross(pa, pb);
			normal.normalize();

			normal.scale(vertex.z);
			p.add(normal);

			vertex.set(p);
		}
	}

	private Point3f xy_to_xyz(final float x, final float y) {
		final Vector3f ad = Tangle3DUtils.aplusbtimesx(Tangle3DUtils.TETRAHEDRON_CORNER_A, x, Tangle3DUtils.TETRAHEDRON_EDGE_AD);
		final Vector3f ab = Tangle3DUtils.aplusbtimesx(Tangle3DUtils.TETRAHEDRON_CORNER_A, y, Tangle3DUtils.TETRAHEDRON_EDGE_AB);
		final Vector3f dc = Tangle3DUtils.aplusbtimesx(Tangle3DUtils.TETRAHEDRON_CORNER_D, y, Tangle3DUtils.TETRAHEDRON_EDGE_DC);
		final Vector3f bc = Tangle3DUtils.aplusbtimesx(Tangle3DUtils.TETRAHEDRON_CORNER_B, x, Tangle3DUtils.TETRAHEDRON_EDGE_BC);

		final Vector3f ab_to_dc = Tangle3DUtils.subtract(dc, ab);
		final Vector3f ad_to_bc = Tangle3DUtils.subtract(bc, ad);

		final Vector3f q = ad;
		final Vector3f p = ab;

		final Vector3f r = ab_to_dc;
		final Vector3f s = ad_to_bc;

		final float t = Tangle3DUtils.cross((Tangle3DUtils.subtract(q, p)), s).length() / Tangle3DUtils.cross(r, s).length();
		
		final Point3f result = new Point3f(Tangle3DUtils.aplusbtimesx(p, t, r));
		result.scale(this.scale);
		return result;
	}

	private TransformGroup createStrandNode(List<Point3f> vertices, Color3f color) {
		final TransformGroup strandTransformGroup = new TransformGroup();
		strandTransformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		strandTransformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

		Point3f previousVertex = null;
		for (final Point3f vertex : vertices) {
			final Node shpereNode = Tangle3DUtils.createVertexNode(vertex, color, SPHERE_RADIUS);
			strandTransformGroup.addChild(shpereNode);

			if (previousVertex != null) {
				final Node lineNode = Tangle3DUtils.createLineNode(previousVertex, vertex, color, CYLINDER_RADIUS);
				strandTransformGroup.addChild(lineNode);
			}

			previousVertex = vertex;
		}
		return strandTransformGroup;
	}

	private List<Point3f> getVertices(List<TwistAndTwistCorner> strandTrace) {
		final List<Point3f> vertices = new ArrayList<Point3f>();

		for (final TwistAndTwistCorner twistAndTwistCorner : strandTrace) {
			final Twist twist = twistAndTwistCorner.twist;
			final TwistCorner twistCorner = twistAndTwistCorner.twistCorner;
			vertices.addAll(twist.getVertices(twistCorner));
		}

		return vertices;
	}

	private boolean hasOddNumberOfTwists() {
		return MathFunctions.odd(tangle.getVector().length());
	}

	private TwistAndTwistCorner getNextTwistAndTwistCorner(final List<Twist> twists, Twist twist, TwistCorner twistCorner) throws TopRightCornerReachedException, BottomRightCornerReachedException, TopLeftCornerReachedException, BottomLeftCornerReachedException {
		if (this.hasOddNumberOfTwists()) {
			/* Starts and ends with a horizontal twist: */
			switch (twist.orientation) {
				case HORIZONTAL:
					switch (twistCorner) {
						case TOP_LEFT:
							if (twist.index == 0) {
								throw new TopLeftCornerReachedException();
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index - 2), TwistCorner.TOP_RIGHT);
							}
						case TOP_RIGHT:
							if (twist.index == twists.size() - 1) {
								throw new TopRightCornerReachedException();
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index + 2), TwistCorner.TOP_LEFT);
							}
						case BOTTOM_LEFT:
							if (twist.index == 0) {
								if (twists.size() == 1) {
									throw new BottomLeftCornerReachedException();
								} else {
									return new TwistAndTwistCorner(twists.get(1), TwistCorner.TOP_LEFT);
								}
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index - 1), TwistCorner.BOTTOM_RIGHT);
							}
						case BOTTOM_RIGHT:
							if (twist.index == twists.size() - 1) {
								throw new BottomRightCornerReachedException();
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index + 1), TwistCorner.TOP_RIGHT);
							}
						default:
							throw new IllegalStateException();
					}
				case VERTICAL:
					switch (twistCorner) {
						case TOP_LEFT:
							if (twist.index == 1) {
								return new TwistAndTwistCorner(twists.get(0), TwistCorner.BOTTOM_LEFT);
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index - 2), TwistCorner.BOTTOM_LEFT);
							}
						case TOP_RIGHT:
							return new TwistAndTwistCorner(twists.get(twist.index - 1), TwistCorner.BOTTOM_RIGHT);
						case BOTTOM_LEFT:
							if (twist.index == twists.size() - 2) {
								throw new BottomLeftCornerReachedException();
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index + 2), TwistCorner.TOP_LEFT);
							}
						case BOTTOM_RIGHT:
							return new TwistAndTwistCorner(twists.get(twist.index + 1), TwistCorner.BOTTOM_LEFT);
						default:
							throw new IllegalStateException();
					}
				default:
					throw new IllegalStateException();
			}
		} else {
			/* Starts with a vertical and ends with a horizontal twist: */
			switch (twist.orientation) {
				case HORIZONTAL:
					switch (twistCorner) {
						case TOP_LEFT:
							if (twist.index == 1) {
								return new TwistAndTwistCorner(twists.get(0), TwistCorner.TOP_RIGHT);
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index - 2), TwistCorner.TOP_RIGHT);
							}
						case TOP_RIGHT:
							if (twist.index == twists.size() - 1) {
								throw new TopRightCornerReachedException();
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index + 2), TwistCorner.TOP_LEFT);
							}
						case BOTTOM_LEFT:
							return new TwistAndTwistCorner(twists.get(twist.index - 1), TwistCorner.BOTTOM_RIGHT);
						case BOTTOM_RIGHT:
							if (twist.index == twists.size() - 1) {
								throw new BottomRightCornerReachedException();
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index + 1), TwistCorner.TOP_RIGHT);
							}
						default:
							throw new IllegalStateException();
					}
				case VERTICAL:
					switch (twistCorner) {
						case TOP_LEFT:
							if (twist.index == 0) {
								throw new TopLeftCornerReachedException();
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index - 2), TwistCorner.BOTTOM_LEFT);
							}
						case TOP_RIGHT:
							if (twist.index == 0) {
								return new TwistAndTwistCorner(twists.get(twist.index + 1), TwistCorner.TOP_LEFT);
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index - 1), TwistCorner.BOTTOM_RIGHT);
							}
						case BOTTOM_LEFT:
							if (twist.index == twists.size() - 2) {
								throw new BottomLeftCornerReachedException();
							} else {
								return new TwistAndTwistCorner(twists.get(twist.index + 2), TwistCorner.TOP_LEFT);
							}
						case BOTTOM_RIGHT:
							return new TwistAndTwistCorner(twists.get(twist.index + 1), TwistCorner.BOTTOM_LEFT);
						default:
							throw new IllegalStateException();
					}
				default:
					throw new IllegalStateException();
			}
		}
	}

	public static class TwistAndTwistCorner {
		private final Twist twist;
		private final TwistCorner twistCorner;

		public TwistAndTwistCorner(Twist twist, TwistCorner twistCorner) {
			this.twist = twist;
			this.twistCorner = twistCorner;
		}
	}

	private static class Twist {
		private final int index;
		private final float x;
		private final float y;
		private final float width;
		private final float height;
		private final TwistOrientation orientation;
		private final CrossingType crossingType;
		private final long numberOfCrossings;
		private final int crossingResolution; // the interpolation steps per crossing (more is smoother)
		private final float crossingSize;
		private final float crossingHelixRadius;
		private final float scale;

		public Twist(int twistIndex, float twistX, float twistY, float twistWidth, float twistHeight, TwistOrientation twistOrientation, CrossingType crossingType, long numberOfCrossings, int crossingResolution, float crossingSize, float crossingHelixRadius, float scale) {
			this.index = twistIndex;
			this.x = twistX;
			this.y = twistY;
			this.width = twistWidth;
			this.height = twistHeight;
			this.orientation = twistOrientation;
			this.crossingType = crossingType;
			this.numberOfCrossings = numberOfCrossings;
			this.crossingResolution = crossingResolution;
			this.crossingSize = crossingSize;
			this.crossingHelixRadius = crossingHelixRadius;
			this.scale = scale;
		}

		public List<Point3f> getVertices(TwistCorner twistCorner) {
			final List<Point3f> localVertices = this.getVerticesLocal(twistCorner);

			/* Move to 'global' position: */
			this.translate(localVertices, this.x, this.y, 0);

			return localVertices;
		}

		private List<Point3f> getVerticesLocal(TwistCorner twistCorner) {
			final List<Point3f> vertices = new ArrayList<Point3f>();

			switch (this.orientation) {
				case HORIZONTAL:
					/* We can make a lot of use of symmetry: */
					switch (twistCorner) {
						case TOP_LEFT:
							vertices.add(new Point3f(0, 0, 0));

							for (int i = 0; i < this.numberOfCrossings * this.crossingResolution; i++) {
								final float t = (((float) i) / this.crossingResolution) * ((float) Math.PI);
								final float x = t / ((float) Math.PI) * this.crossingSize;
								final float y = this.crossingSize * (float) Math.sin(t - (float) Math.PI / 2);
								final float z;
								switch (this.crossingType) {
									case POSITIVE:
										z = -this.crossingHelixRadius * this.scale * (float) Math.cos(t - (float) Math.PI / 2);
										break;
									case NEGATIVE:
										z = this.crossingHelixRadius * this.scale * (float) Math.cos(t - (float) Math.PI / 2);
										break;
									default:
										throw new IllegalStateException();
								}

								final Point3f vertex = new Point3f(x, y, z);
								vertex.x += TWIST_PADDING_HORIZONTAL;
								vertex.y += this.height / 2;
								vertices.add(vertex);
							}

							if (this.hasEvenNumberOfCrossings()) {
								vertices.add(new Point3f(this.width, 0, 0));
							} else {
								vertices.add(new Point3f(this.width, this.height, 0));
							}
							break;
						case BOTTOM_LEFT:
							vertices.addAll(this.getVerticesLocal(TwistCorner.TOP_LEFT));
							this.translate(vertices, -(this.width * 0.5f), -(this.height * 0.5f), 0);
							this.rotateAroundXAxis(vertices, (float) Math.PI);
							this.translate(vertices, (this.width * 0.5f), (this.height * 0.5f), 0);
							break;
						case TOP_RIGHT:
							vertices.addAll(this.getVerticesLocal(TwistCorner.TOP_LEFT));
							this.translate(vertices, -(this.width * 0.5f), -(this.height * 0.5f), 0);
							this.rotateAroundYAxis(vertices, (float) Math.PI);
							this.translate(vertices, (this.width * 0.5f), (this.height * 0.5f), 0);
							break;
						case BOTTOM_RIGHT:
							vertices.addAll(this.getVerticesLocal(TwistCorner.TOP_RIGHT));
							this.translate(vertices, -(this.width * 0.5f), -(this.height * 0.5f), 0);
							this.rotateAroundXAxis(vertices, (float) Math.PI);
							this.translate(vertices, (this.width * 0.5f), (this.height * 0.5f), 0);
							break;
					}
					break;
				case VERTICAL:
					final Twist horizontalTwist = new Twist(this.index, this.x, this.y, this.height, this.width, TwistOrientation.HORIZONTAL, this.crossingType.getNegative(), this.numberOfCrossings, this.crossingResolution, this.crossingSize, this.crossingHelixRadius, this.scale);
					final List<Point3f> horizontalVertices = horizontalTwist.getVerticesLocal(twistCorner.getRotatedBy90Degrees());
					/* Rotate vertices by -90deg around the zAxis: */
					this.translate(horizontalVertices, -(this.height * 0.5f), -(this.width * 0.5f), 0);
					this.rotateAroundZAxis(horizontalVertices, (float) (Math.PI / 2));
					this.translate(horizontalVertices, (this.width * 0.5f), (this.height * 0.5f), 0);
					vertices.addAll(horizontalVertices);
					break;
				default:
					throw new IllegalStateException();
			}

			return vertices;
		}

		private void rotateAroundXAxis(List<Point3f> vertices, float angle) {
			this.rotateAroundAxis(vertices, AXIS_X, angle);
		}

		private void rotateAroundYAxis(List<Point3f> vertices, float angle) {
			this.rotateAroundAxis(vertices, AXIS_Y, angle);
		}

		private void rotateAroundZAxis(List<Point3f> vertices, float angle) {
			this.rotateAroundAxis(vertices, AXIS_Z, angle);
		}

		private void rotateAroundAxis(List<Point3f> vertices, final Vector3f axis, float angle) {
			final AxisAngle4f axisAngle = new AxisAngle4f(axis, angle);
			final Matrix3f matrix = new Matrix3f();
			matrix.set(axisAngle);
			for (Point3f vertex : vertices) {
				matrix.transform(vertex);
			}
		}

		private void translate(List<Point3f> vertices, float x, float y, float z) {
			for (Point3f vertex : vertices) {
				vertex.set(vertex.x + x, vertex.y + y, vertex.z + z);
			}
		}

		private boolean hasEvenNumberOfCrossings() {
			return MathFunctions.even(this.numberOfCrossings);
		}

		public static float getSize(final TwistOrientation twistOrientation, final long numberOfCrossings, final float crossingSize) {
			switch (twistOrientation) {
				case HORIZONTAL:
					return TWIST_PADDING_HORIZONTAL + numberOfCrossings * crossingSize + TWIST_PADDING_HORIZONTAL;
				case VERTICAL:
					return TWIST_PADDING_HORIZONTAL + numberOfCrossings * crossingSize + TWIST_PADDING_HORIZONTAL;
				default:
					throw new IllegalStateException();
			}
		}

		public TwistCorner traceCorner(final TwistCorner twistCorner) {
			if (this.hasEvenNumberOfCrossings()) {
				switch (this.orientation) {
					case HORIZONTAL:
						return twistCorner.mirrorHorizontal();
					case VERTICAL:
						return twistCorner.mirrorVertical();
					default:
						throw new IllegalStateException();
				}
			} else {
				return twistCorner.mirrorDiagonal();
			}
		}
	}

	private static enum TwistCorner {
		TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT;

		public TwistCorner mirrorHorizontal() {
			switch (this) {
				case TOP_LEFT:
					return TOP_RIGHT;
				case TOP_RIGHT:
					return TOP_LEFT;
				case BOTTOM_LEFT:
					return BOTTOM_RIGHT;
				case BOTTOM_RIGHT:
					return BOTTOM_LEFT;
				default:
					throw new IllegalStateException();
			}
		}

		public TwistCorner getRotatedBy90Degrees() {
			switch (this) {
				case TOP_LEFT:
					return BOTTOM_LEFT;
				case BOTTOM_LEFT:
					return BOTTOM_RIGHT;
				case BOTTOM_RIGHT:
					return TOP_RIGHT;
				case TOP_RIGHT:
					return TOP_LEFT;
				default:
					throw new IllegalStateException();
			}
		}

		public TwistCorner mirrorVertical() {
			switch (this) {
				case TOP_LEFT:
					return BOTTOM_LEFT;
				case TOP_RIGHT:
					return BOTTOM_RIGHT;
				case BOTTOM_LEFT:
					return TOP_LEFT;
				case BOTTOM_RIGHT:
					return TOP_RIGHT;
				default:
					throw new IllegalStateException();
			}
		}

		public TwistCorner mirrorDiagonal() {
			return this.mirrorHorizontal().mirrorVertical();
		}
	}

	private static enum CrossingType {
		POSITIVE, NEGATIVE;

		public static CrossingType fromRational(final Rational rational) {
			if (rational.getNumerator() > 0) {
				return POSITIVE;
			} else {
				return NEGATIVE;
			}
		}

		public CrossingType getNegative() {
			switch (this) {
				case POSITIVE:
					return NEGATIVE;
				case NEGATIVE:
					return POSITIVE;
				default:
					throw new IllegalStateException();
			}
		}
	}

	private static enum TwistOrientation {
		HORIZONTAL, VERTICAL;

		public TwistOrientation alternate() {
			switch (this) {
				case HORIZONTAL:
					return VERTICAL;
				case VERTICAL:
					return HORIZONTAL;
				default:
					throw new IllegalStateException();
			}
		}
	}

	public class TopLeftCornerReachedException extends Exception {
		private static final long serialVersionUID = 7012011125748326637L;
	}

	public class TopRightCornerReachedException extends Exception {
		private static final long serialVersionUID = 8564691023344185137L;
	}

	public class BottomLeftCornerReachedException extends Exception {
		private static final long serialVersionUID = -6914980569669271457L;
	}

	public class BottomRightCornerReachedException extends Exception {
		private static final long serialVersionUID = -203563620601795911L;
	}
}
