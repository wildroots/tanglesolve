package objects;

/**
 * @title Equation System
 * 
 * <p>
 * Description: Wrapper class for each resulting system, contains the system and
 * the applicable theorem
 * </p>
 * 
 * @author Wenjing Zheng
 * @edited by Crista Moreno
 * @version 1.0
 */
public class EquationSystem {

	private final VectorList system; // contains the list of equations for the system to be solved
	private String theorem = ""; // theorem for the corresponding system
	private boolean isProcessive = false;
	private final int numberEquations; // number of equations in the system
	private static int totalRationalSystems = 0; // records number of systems where O must be a rational tangle

	/**
	 * Constructor
	 * 
	 * @param system
	 * VectorList represents system in terms of tangles
	 * 
	 * @param isProcessive
	 * boolean true if solution is for a processive recombination
	 */
	public EquationSystem(final VectorList system, final boolean isProcessive) {
		this.system = system;
		this.isProcessive = isProcessive;
		this.numberEquations = this.system.length();
		if (this.isProcessive) {
			this.theorem = TangleTheorems.checkProcessive(this.system);
		} else {
			this.theorem = TangleTheorems.checkNonProcessive(this.system);
		}
		if (this.theorem != null) {
			EquationSystem.totalRationalSystems++;
		}
	}

	// ----------------------ACCESSOR METHODS------------------------//
	/**
	 * 
	 * @return VectorList the system
	 */
	public VectorList getSystem() {
		return this.system;
	}

	/**
	 * 
	 * @return number of equations in the system
	 */
	public int getNumberEquations() {
		return this.numberEquations;
	}

	/**
	 * 
	 * @return String theorem in html format that applies to this system,
	 * returns null if no such theorem exists
	 * 
	 */
	public String getTheorem() {
		final String conwaySystem = this.toConwayString();
		final String theoremString = "<html><table align=left border><tbody><tr><td align=left>"
				+ conwaySystem
				+ "<p>"
				+ this.theorem
				+ "</td></tr></tbody></table></html>";
		return theoremString;
	}

	public String getString() {
		return VectorList.arrowList(this.system);
	}

	public VectorList getConwayRep() {
		final VectorList conwaySystem = (VectorList) this.system.clone();
		VectorList pter = conwaySystem;
		for (VectorList temp2 = this.system; temp2 != null; temp2 = temp2.tail()) {
			pter.setHead(((FourPlat) temp2.head()).getConwayString());
			pter = pter.tail();
		}
		return conwaySystem;
	}

	public static int getTotalRationalSystems() {
		return EquationSystem.totalRationalSystems;
	}

	/**
	 * 
	 * @return boolean true if there is applicable theorem to this system
	 */
	public boolean hasTheorem() {
		return (this.theorem != null);
	}

	// ----------------------UTILITY METHODS------------------------//
	public static void resetTotalRationalSystems() {
		EquationSystem.totalRationalSystems = 0;
	}

	
	/**
	 * @return String String representation of the system in format of an
	 * arrowList
	 */
	@Override
	public String toString() {
		final String returnString = VectorList.arrowList(this.system);
	
		// if has theorem then return a string that appears bold and red in
		// labels
		if (this.theorem != null) {
			return new String("<html> <font color=red> <b>" + returnString
					+ "</b> </font> <html>");
		} else {
			return returnString;
		}
	}

	public String toConwayString() {
		return VectorList.arrowList(this.getConwayRep());
	}
}
//----------------------EQUIVALENCE------------------------//