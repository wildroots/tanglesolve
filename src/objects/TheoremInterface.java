package objects;

/**
 * Title: TheoremInterface
 * </p>
 * 
 * <p>
 * Description: List of all Tangle Theorems in string form
 * </p>
 * 
 * @author Wenjing Zheng
 * @version 1.0
 */

public interface TheoremInterface {
	
	// constants
	public static final String HIRASAWA_THEOREM = "Lemma <b>[HS]</b><p>  "
			+ "No Dehn surgery on a non-trivial strongly invertible knot can produce<p>  "
			+ "a lens space of the form L(2n, 1) for any integer n."
			+ "<blockquote><i><small>Reference: M. Hirasawa and K. Shimokawa, Dehn surgery on strongly invertible knots.<p>  "
			+ "Proc. Am. Math. Soc. 128, 3445-3451.</small></i></blockquote><p> "
			+

			"This lemma can be applied directly to a system of tangle equations of the form: <p>  "
			+ "N(O + P) = b(a,b) <p>  N(O + R) = b(c,d) <p>  "
			+ "Where a = 1 = b, c is even and d congruent to +-(1)<sup>+-1</sup> (mod c)."
			+ "<blockquote><i><small>Reference: M. Vazquez, S.D. Colloms, D.W. Sumners. Tangle Analysis of Xer Recombination <p>  "
			+ "Reveals only Three Solutions, all Consistent with a Single Three-dimensional Topological Pathway. <p>  "
			+ "J. Mol. Biol. 346 (2005) 493-504. </small></i></blockquote><p>";
	/*
	 * public static final String FPlatClassif = "Theorem(Schubert, 1956)<p>  "+
	 * "Two 4-Plats b(a,b) and b(c,d) are equivalent (as unoriented knots or links)<p>   "
	 * + "if and only if a = c and d is congruent to b or b^-1 modulo a. <p>  "+
	 * "Reference: H.Schubert. Knoten mit zwei Brucken. Math Z 65 (1956) 133-170. <p>"
	 * ;
	 * 
	 * public static final String XerTheorem1 = "Theorem(Vazquez, 2005)<p>  "+
	 * "Given a system of two tangle equations from non-processive recombination <p>  "
	 * + "N(O+P) = b(a,b)<p>  "+ "N(O+R) = b(c,d) <p>  "+
	 * "If a = b = 1, and if c is even and L(c,d) ~ L(c, 1), then O must be rational. <p>  "
	 * +
	 * "Reference: M. Vazquez, S.D. Colloms, D.W. Sumners. Tangle Analysis of Xer Recombination <p>  "
	 * +
	 * "Reveals only Three Solutions, all Consistent with a Single Three-dimensional Topological Pathway. <p>  "
	 * + "J. Mol. Biol. 346 (2005) 493-504. <p>" ;
	 * 
	 * public static final String LensSpaceClassif =
	 * "Classification of Lens Spaces<p>  "+
	 * "Two Lens Spaces L(a,b) and L(c,d) are equivalent, i.e. L(a,b) ~ L(c,d) <p>  "
	 * +
	 * "if and only if a = c and d is congruent to +-b or +-b^-1 modulo a. <p>  "
	 * + "Reference: . <p>";
	 */

	public static final String SPECIAL_P1_DENOM = "Theorem <b>[ES90]</b><p>  "
			+ "O any tangle, and exists Ai for 1 <= i <= 3, with A2, A3 locally unknotted, <p>   "
			+ "such that the following 3 equations hold: <p>  "
			+ "N(O + A1) = b(1,1) = unknot <p>  "
			+ "N(O + A2) = b(a, b) a > 1 <p>  "
			+ "N(O + A3) = b(a', b') a' > 1 <p>  "
			+ "If |a - a'| > 1, and if a = 2, 3, or 4, then O is rational."
			+ "<blockquote><i><small>Reference: C. Ernst and D. W. Sumners, A Calculus for Rational Tangles: Application to DNA Recombination, <p>  "
			+ "Math. Proc. Camb. Phil. Soc. (1990) 108, 489-515.</small></i></blockquote><p>";

	public static final String ES96_THEOREM_2_2 = "Theorem <b>[ES99] </b> <p>  "
			+ "Let {O, R} be a simultaneous solutions of the system: <p>  "
			+ "N(O) = K0 <p>  "
			+ "N(O + 1R) = K1 <p>  "
			+ "N(O + 2R) = K2 <p>  "
			+ "N(O + 3R) = K3 <p>  "
			+ "Where the Ki's are 4-plats and {K1, K2, K3} represent at least <p>  "
			+ "two different link or knot types. Then R is a non-zero integral tangle <p>  "
			+ "and O is either rational or sum of rational tangles. "
			+ "<blockquote><i><small>Reference: C. Ernst and D. W. Sumners, Solving Tangle Equations arising in a DNA recombination model.<p>  "
			+ "Math. Proc. Camb. Phil. Soc. (1999) 126, 23-36.</small></i></blockquote><p>";
}
