/**
 * Author : Yuki Saka
 * Last Updated : August 24, 2013
 * @edited by Crista Moreno
 */
package objects;

import java.util.ArrayList;
import java.util.List;

public class FourPlat implements Element {
	
	// constants
	private static final String NON_CANONICAL = "NONCANONICAL";
	
	// private fields
	private Rational rational;
	private static List<VectorList<FourPlat>> fptb = null;
	private static int crtb = 0;
	private static boolean tbFlag = false;
	
	// constructors
	public FourPlat(final Rational rational) throws IllegalArgumentException {
		if ((!rational.isInfinity() && (rational.getNumerator() > rational.getDenominator())) || (rational.getSignature() == -1) || rational.isZero()) {
			throw new IllegalArgumentException(FourPlat.NON_CANONICAL);
		} else {
			this.rational = rational;
		}
	}

	public FourPlat(final VectorList<Rational> list) throws IllegalArgumentException {
		if ((list.length() % 2) == 0) {
			throw new IllegalArgumentException(FourPlat.NON_CANONICAL);
		} else {
			this.rational = Rational.toRational(list).getReciprocal();
		}
	}

	public FourPlat(final Tangle tangle1, final Tangle tangle2) throws IllegalArgumentException {
		this.rational = FourPlat.n_f(tangle1.getRational(), tangle2.getRational());
	}

	public static void loadFourPlats(final int cr) {
		FourPlat.fptb = new ArrayList<VectorList<FourPlat>>(cr);
		for (int i = 0; i < cr; i++) {
			FourPlat.fptb.add(FourPlat.enumerateFourPlats(i));
		}
		FourPlat.tbFlag = true;
	}

	public static Rational n_f(final Rational rational1, final Rational rational2)
			throws IllegalArgumentException {

		final long u = rational1.getNumerator();
		final long v = rational1.getDenominator();
		final long x = rational2.getNumerator();
		final long y = rational2.getDenominator();
		
		final long al = Math.abs((x * v) + (y * u));
		if (al == 0) {
			return new Rational(1, 0);
		} else if (al == 1) {
			return new Rational(1, 1);
		} else {
			final long sign = ((x * v) + (y * u)) / al;

			final long[] inverse = MathFunctions.inverse2(x, y);
			if (inverse == null) {
				throw new IllegalArgumentException("Inverse of " + x + " does not exist");
			}
			return FourPlat.inverseR(new Rational(MathFunctions.mod((sign * ((v * inverse[1]) + (u * inverse[0]))), al), al));
		}
	}

	/**
	 * 
	 * @return true (is a knot) if denominator of rational is odd.
	 */
	public boolean isKnot() {
		return ((this.rational.getDenominator() % 2) == 1);
	}

	private long sumCrossings(final VectorList list) {
		if (list == null) {
			return 0;
		} else {
			final long crossings = ((Rational) list.head()).getNumerator();
			return crossings + this.sumCrossings(list.tail());
		}
	}

	private static Rational inverseR(final Rational rational) {
		final long a = rational.getDenominator();
		long b = rational.getNumerator();

		b = MathFunctions.moduloInverse1(b, a);
		return new Rational(b, a);
	}

	@Override
	public String toString() {
		final String s = this.knotLinkTypeString();
		if (s.equals(" knot") || s.equals(" link")) {
			return this.getVector().toString();
		} else {
			return s;
		}
	}

	/**
	 * Calls Object.clone() to give a field-to-field copy of the instance </p>
	 * 
	 * @return Object version of the clone
	 * */
	@Override
	public FourPlat clone() {
		try {
			return (FourPlat) super.clone();
		} catch (final Exception exception) {
			return null;
		}
	}
	
	public String shortDescription() {
		final String s = this.knotLinkTypeString();
		return this.getVector() + s;
	}

	public String completeDescription() {
		final String s = this.knotLinkTypeString();
		return this.getVector() + " = " + this.getRational().getConwayString() + " = " + s;
	}

	public String knotLinkTypeString() {
		String s =  "";
		
		// torus family
		final long numerator = this.rational.getNumerator();
		final long denominator = this.rational.getDenominator();
		if (numerator == 1) {
			final String torusString = (new Long(denominator)).toString();
			// torus knot
			if ((denominator % 2) == 1) {
				s = " -" + torusString + " torus";
			} else {
				// torus link
				s = " Left Handed " + torusString + " torus";
			}
		} else if ((denominator - numerator) == 1) {
			final String torusString = (new Long(denominator)).toString();
			if ((denominator % 2) == 1) {
				s = " +" + torusString + " torus";
			} else {
				s = " Right Handed " + torusString + " torus";
			}
		}

		if (((numerator == 2) && ((denominator % 2) == 1))
				|| (denominator == ((numerator * 2) - 1))) {
			// twist knot
			s = " +" + this.getNumberCrossings() + " twist";
		} else if ((denominator == ((numerator * 2) + 1))
				|| ((numerator == (denominator - 2)) && ((denominator % 2) == 1))) {
			s = " -" + this.getNumberCrossings() + " twist";
		}

		if (this.rational.isInfinity()) {
			s = " un";
		} else if (this.rational.isOne()) {
			s = " un";
		} else if (this.rational.equals(new Rational(1, 2))) {
			s = " Hopf";
		} else if (this.rational.equals(new Rational(1, 3))) {
			s = " -trefoil";
		} else if (this.rational.equals(new Rational(2, 3))) {
			s = " +trefoil";
		} else if (this.rational.equals(new Rational(2, 5)) || this.rational.equals(new Rational(3, 5))) {
			s = " figure-8";
		} else if (this.rational.equals(new Rational(3, 8))) {
			s = " -Whitehead";
		} else if (this.rational.equals(new Rational(5, 8))) {
			s = " +Whitehead";
		}

		if ((denominator % 2) == 0) {
			if (s.equals(" un")) {
				s = s + "link";
			} else {
				s = s + " link";
			}
		} else {
			if (s.equals(" un")) {
				s = s + "knot";
			} else {
				s = s + " knot";
			}
		}
		return s;
	}


	// ----------------------ACCESSOR METHODS------------------------//
	/**
	 * 
	 * @return rational
	 */
	public Rational getRational() {
		return this.rational;
	}

	public VectorList getVector() {
		try {
			long denominator = this.rational.getDenominator();
			long numerator = this.rational.getNumerator();
			if (!this.rational.isInfinity() && ((denominator < 0) || (numerator < 0) || (numerator > denominator))) {
				this.rational = FourPlat.reduce(this.rational);
			}
			final long addOneNumerator = this.rational.add(1).getNumerator();
			final long addOneDenominator = this.rational.add(1).getDenominator();
			
			return FourPlat.convertCanonical((new Rational(addOneNumerator, addOneDenominator)).continuedFraction());
		} catch (final IllegalArgumentException exception) {
			return null;
		}
	}

	/**
	 * 
	 * @return long the number of crossings. 
	 */
	public long getNumberCrossings() {
		return this.sumCrossings(this.getVector());
	}
	
	public String getConwayString() {
		final String conway = "b(" + this.rational.getDenominator() + ", " + this.rational.getNumerator() + ")";
		return conway;
	}
	
	// ----------------------EQUIVALENCE------------------------//
	@Override
	public boolean equals(final Object object) {
		if (object instanceof FourPlat) {
			final FourPlat rightHandSide = (FourPlat) object; // was rhs?????
			return FourPlat.fEquals(this.rational, rightHandSide.rational);
		} else { // not a FourPlat object
			return false;
		}
		
	}

	private static boolean fEquals(final Rational a, final Rational b) {
		return ((a.getDenominator() == b.getDenominator()) && ((a.getNumerator() == b.getNumerator()) || (a.getNumerator() == MathFunctions
				.moduloInverse1(b.getNumerator(), a.getDenominator()))));
	}

	// ----------------------ENUMERATION METHODS------------------------//
	public static VectorList<FourPlat> enumerateFourPlats(final long cn) {
		try {
			if (cn == -1) {
				return null;
			}
			if (cn == 0) {
				return new VectorList<FourPlat>(new FourPlat(new Rational(1, 0)), null);
			}

			if (FourPlat.tbFlag && (FourPlat.crtb >= cn)) {
				return FourPlat.fptb.get((int) cn);
			}
			VectorList<VectorList<Rational>> part = FourPlat.filterOdd(MathFunctions.partitions(cn));
			VectorList<VectorList<Rational>> res = null;
			for (; part != null; part = part.tail()) {
				res = MathFunctions.permute((VectorList<Rational>) part.head()).append(res);
			}
			res = FourPlat.filterPalindrome(res);

			VectorList<FourPlat> result = null;
			for (; res != null; res = res.tail()) {
				final VectorList<Rational> rationalList = (VectorList<Rational>) res.head();
				result = VectorList.append(result, new VectorList<FourPlat>(new FourPlat(rationalList), null));
			}
			return result;
		} catch (final Exception exception) {
			return null;
		}
	}

	public static <T> VectorList<VectorList<T>> filterPalindrome(final VectorList<VectorList<T>> list) {
		if (list == null) {
			return null;
		} else if (VectorList.member(((VectorList<T>) list.head()).reverse(), list.tail())) {
			return FourPlat.filterPalindrome(list.tail());
		} else {
			return new VectorList<VectorList<T>>(list.head(), FourPlat.filterPalindrome(list.tail()));
		}
	}

	private static Rational reduce(final Rational rational) {
		long denominator = rational.getDenominator();
		if (denominator == 1) {
			return rational;
		} else {
			long numerator = rational.getNumerator();
			if (numerator > 0) {
				return new Rational(numerator % denominator, denominator);
			} else {
				return new Rational((numerator % denominator) + denominator, denominator);
			}
		}
	}

	private static VectorList<Rational> convertCanonical(final VectorList<Rational> list)
			throws IllegalArgumentException {
		if (VectorList.isEmpty(list.tail())) {
			return new VectorList<Rational>(((Rational) list.head()).subtract(1), null);
		} else if ((list.length() % 2) == 1) {
	
			final VectorList<Rational> r = list.tail().reverse();
			if (((Rational) r.head()).getNumerator() == 1) {
				return (new VectorList<Rational>(((Rational) r.tail().head()).add(1), r.tail().tail())).reverse();
			} else {
				return (new VectorList<Rational>(new Rational(1), new VectorList<Rational>(((Rational) r.head()).subtract(1), r.tail()))).reverse();
			}
		} else {
			return list.tail();
		}
	}

	private static <T> VectorList<VectorList<T>> filterOdd(final VectorList<VectorList<T>> list) {
		if (list == null) {
			return null;
		} else if ((((VectorList<T>) list.head()).length() % 2) == 0) {
			return FourPlat.filterOdd(list.tail());
		} else {
			return new VectorList<VectorList<T>>(list.head(), FourPlat.filterOdd(list.tail()));
		}
	}
}
