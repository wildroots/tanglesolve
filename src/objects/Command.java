package objects;

public class Command implements Lambda, Element {

	// constants
	public static final int OPERATION_EXPAND = 1;
	public static final int OPERATION_TO_RATIONAL = 2;
	public static final int OPERATION_SOLVE = 3;
	public static final int OPERATION_QUOTE = 4;
	public static final int OPERATION_FOUR_PLAT_LIST = 5;
	public static final int OPERATION_TANGLE_LIST = 6;
	public static final int OPERATION_CONWAY = 7;
	public static final int OPERATION_LIST = 8;

	// function string constants
	public static final String QUOTE = "'";
	public static final String EXPAND = "expand";
	public static final String TO_RATIONAL = "torat";
	public static final String SOLVE = "solve";
	public static final String FOUR_PLAT_LIST = "fplist";
	public static final String TANGLE_LIST = "tglist";
	public static final String LIST = "list";
	public static final String CONWAY = "b";

	// fields
	private int operation;

	/**
	 * Constructor
	 * 
	 * @param command
	 */
	public Command(final String command) {
		if (command.equals(Command.EXPAND)) {
			this.operation = OPERATION_EXPAND;
		} else if (command.equals(Command.TO_RATIONAL)) {
			this.operation = OPERATION_TO_RATIONAL;
		} else if (command.equals(Command.SOLVE)) {
			this.operation = OPERATION_SOLVE;
		} else if (command.equals(Command.QUOTE)) {
			this.operation = OPERATION_QUOTE;
		} else if (command.equals(Command.FOUR_PLAT_LIST)) {
			this.operation = OPERATION_FOUR_PLAT_LIST;
		} else if (command.equals(Command.TANGLE_LIST)) {
			this.operation = OPERATION_TANGLE_LIST;
		} else if (command.equals(Command.CONWAY)) {
			this.operation = OPERATION_CONWAY;
		} else if (command.equals(Command.LIST)) {
			this.operation = OPERATION_LIST;
		} else {
			this.operation = -1;
		}
	}

	@Override
	public Element envoke(final VectorList args) throws IllegalArgumentException {
		switch (this.operation) {
			case OPERATION_EXPAND:
				return Command.expand(args);
			case OPERATION_TO_RATIONAL:
				return Command.toRational(args);
			case OPERATION_SOLVE:
				return Command.solve(args);
			case OPERATION_QUOTE:
				return Command.quote(args);
			case OPERATION_FOUR_PLAT_LIST:
				return Command.fourPlatList(args);
			case OPERATION_TANGLE_LIST:
				return Command.tangleList(args);
			case OPERATION_CONWAY:
				return Command.conway(args);
			case OPERATION_LIST:
				return Command.list(args);
			default:
				throw new IllegalArgumentException("No commands");
		}
	}

	public static Element quote(final VectorList args) {
		return args;
	}

	/**
	 * 
	 * @param args
	 *            Argument: rational number +- a/b such that b != 0
	 * @return expansion of a/b [a1 .. an] such that a/b = a1 + 1/(a2 + 1/( ...
	 *         1/an))
	 */
	public static Element expand(final VectorList args) {
		final Rational rational = (Rational) args.head();
		return rational.continuedFraction();
	}

	/**
	 * 
	 * @param args
	 *            Argument : list v = [a1, ..., an]
	 * @return rational number a/b such that a/b = a1 + 1/(a2 + 1/( .. 1/an)) if
	 *         an = 0, 0/0 is returned
	 */
	public static Element toRational(final VectorList args) {
		VectorList list;
		if (args.head() instanceof VectorList) {
			list = (VectorList) args.head();
		}
		list = args;
		return new Rational(list);
	}

	/**
	 * 
	 * @param args
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static Element solve(final VectorList<VectorList<? extends Element>> args) throws IllegalArgumentException {
		for (VectorList<VectorList<? extends Element>> temp = args; temp != null; temp = temp.tail()) {
			final VectorList<? extends Element> list = (VectorList<? extends Element>) temp.head();
			if (((Rational) list.head()).isZero()) {
				temp.setHead(Command.fourPlatList(list.tail()));
			} else {
				final VectorList<FourPlat> enumerateFourPlats = FourPlat.enumerateFourPlats(((Rational) list.head()).getNumerator());
				temp.setHead(VectorList.append(enumerateFourPlats, Command.fourPlatList(list.tail())));
			}
		}
		final InputData nd = new InputData(false, args, 0);
		return nd.solve();
	}

	/**
	 * 
	 * @param list
	 * @return
	 * @throws IllegalArgumentException
	 */
	private static VectorList<FourPlat> fourPlatList(final VectorList<? extends Element> list) throws IllegalArgumentException {
		if (list == null) {
			return null;
		} else {
			if (list.head() instanceof Rational) {
				return new VectorList<FourPlat>(new FourPlat((Rational) list.head()), Command.fourPlatList(list.tail()));
			} else if (list.head() instanceof VectorList) {
				return new VectorList<FourPlat>(new FourPlat((VectorList) list.head()), Command.fourPlatList(list.tail()));
			} else {
				return null;
			}
		}
	}

	/**
	 * 
	 * @param list
	 * @return tangle list
	 */
	private static VectorList tangleList(final VectorList list) {
		if (list == null) {
			return null;
		} else {
			if (list.head() instanceof Rational) {
				return new VectorList(new Tangle((Rational) list.head()), Command.tangleList(list.tail()));
			} else if (list.head() instanceof VectorList) {
				return new VectorList(new Tangle((VectorList) list.head()), Command.tangleList(list.tail()));
			} else {
				return null;
			}
		}
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	private static VectorList list(final VectorList list) {
		if (list == null) {
			return null;
		} else {
			return list;
		}
	}

	/**
	 * 
	 * @param list
	 * @return a rational number b/a given b(a,b)
	 */
	private static Rational conway(final VectorList list) {
		final Rational a = (Rational) list.head();
		final Rational b = (Rational) list.tail().head();
		return new Rational(b.getNumerator(), a.getNumerator());
	}

	/**
	 * Calls Object.clone() to give a field-to-field copy of the instance </p>
	 * 
	 * @return Object version of the clone
	 * */
	@Override
	public Command clone() {
		try {
			return (Command) super.clone();
		} catch (final Exception exception) {
			return null;
		}
	}
}
