/**
 * <p>Title: Element </p>
 * <p>Description: An object that implements cloneable, and hence can call
 * Object.clone() to make a field-for-field copy of instances of that class.</p>
 * <p>Copyright: Yuki Saka </p>
 * <p>Company: </p>
 * @author Yuki Saka
 * @edited by Crista Moreno
 * @version 1.0
 */

package objects;

public interface Element extends Cloneable {
	/**
	 * Calls Object.clone() to give a field-to-field copy of the instance </p>
	 * 
	 * @return Object version of the clone
	 * */
	public Element clone();
}