package objects;

import java.util.List;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.ColoringAttributes;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.Material;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Color3f;
import javax.vecmath.Point2f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.geometry.Cone;
import com.sun.j3d.utils.geometry.Cylinder;
import com.sun.j3d.utils.geometry.Sphere;

public class Tangle3DUtils {
	// constants
	public static final Vector3f AXIS_X = new Vector3f(1, 0, 0);
	public static final Vector3f AXIS_Y = new Vector3f(0, 1, 0);
	public static final Vector3f AXIS_Z = new Vector3f(0, 0, 1);
	
	public static final Color3f BLACK = new Color3f(0f, 0f, 0f);

	public static final Color3f RED = new Color3f(1f, 0f, 0f);
	public static final Color3f GREEN = new Color3f(0f, 0.7f, 0.3f);
	public static final Color3f BLUE = new Color3f(0f, 0f, 1f);
	public static final Color3f FUCHSIA = new Color3f(.7f,.3f,.83f); // was

	/* The tetrahedron corners used for having a fixed calculation for the camera eye and center */
	public static final Point3f TETRAHEDRON_VIEW_ROTATION_CORNER_A = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
	public static final Point3f TETRAHEDRON_VIEW_ROTATION_CORNER_B = new Point3f(0, -1, 1 / (float) Math.sqrt(2)); 
	public static final Point3f TETRAHEDRON_VIEW_ROTATION_CORNER_C = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
	public static final Point3f TETRAHEDRON_VIEW_ROTATION_CORNER_D = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
	
	public static final Point3f[] TETRAHEDRON_VIEW_ROTATION_CORNERS = new Point3f[] {
		TETRAHEDRON_VIEW_ROTATION_CORNER_A,
		TETRAHEDRON_VIEW_ROTATION_CORNER_B,
		TETRAHEDRON_VIEW_ROTATION_CORNER_C,
		TETRAHEDRON_VIEW_ROTATION_CORNER_D
	};

	// Tetrahedron 1 Geometry 2 NICE VIEW!! Standard is at rotation (12)(34)
	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 Geometry 1 A <-> B
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
	
//	// Tetrahedron 1 Geometry 1 B <-> C NICE VIEW!!
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, -1, 1 / (float) Math.sqrt(2)); 
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	
	// Tetrahedron 1 Geometry 1 C <-> D 
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(1, 0, -1 / (float) Math.sqrt(2)); 
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 Geometry 1 A <-> D 
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));  

	// Tetrahedron 1 Geometry 1  B <-> D
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, 1, 1 / (float) Math.sqrt(2)); 
	
	// Tetrahedron 1 Geometry 1  A <-> C
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 1" A -> B -> C -> D 
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 1" A -> B -> D -> C 
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 1" A -> C -> B -> D
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	
	// Tetrahedron 1 "Geometry 1" A -> C -> D -> B
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 1" A -> D -> C -> B
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 1" A -> D -> B -> C
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 2" A -> B -> C
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 2" A -> C -> B
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 2" A -> B -> D
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 2" A -> D -> B
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 2" A -> C -> D
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry " A -> D -> C
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry " B -> C -> D
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry " B -> D -> C
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 2" A<->C and B<->D
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, 1, 1 / (float) Math.sqrt(2));

	// Tetrahedron 1 "Geometry 2" A <-> D and B <-> C 
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(0, -1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
	
	// Tetrahedron 1 "Geometry 2" A <-> B and C <-> D 
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(0, 1, 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(-1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(1, 0, -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(0, -1, 1 / (float) Math.sqrt(2)); 
	
	// Different Tetrahedron "tetrahedron 2" rotated in space
	// Alternative Tetrahedron Vertice Coordinates (Simplifies crossings)
/*	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(-1, 1, -1);
	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(-1, -1, 1);
	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(1, 1, 1);
	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(1, -1, -1);*/
	
	// Tetrahedron 2 Scaled by 1/sqrt(2) to have the same scale as Tetrahedron 1
	// Alternative Tetrahedron Vertice Coordinates (Simplifies crossings)
//	public static final Point3f TETRAHEDRON_CORNER_A = new Point3f(-1 /(float) Math.sqrt(2) , 1 / (float) Math.sqrt(2), -1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_B = new Point3f(-1 / (float) Math.sqrt(2), -1 / (float) Math.sqrt(2), 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_C = new Point3f(1 / (float) Math.sqrt(2), 1 / (float) Math.sqrt(2), 1 / (float) Math.sqrt(2));
//	public static final Point3f TETRAHEDRON_CORNER_D = new Point3f(1 / (float) Math.sqrt(2), -1 / (float) Math.sqrt(2), -1 / (float) Math.sqrt(2));

	public static final Point3f[] TETRAHEDRON_CORNERS = new Point3f[] {
		TETRAHEDRON_CORNER_A,
		TETRAHEDRON_CORNER_B,
		TETRAHEDRON_CORNER_C,
		TETRAHEDRON_CORNER_D
	};

	public static final Vector3f TETRAHEDRON_EDGE_AD = subtract(Tangle3DUtils.TETRAHEDRON_CORNER_D, TETRAHEDRON_CORNER_A);
	public static final Vector3f TETRAHEDRON_EDGE_BC = subtract(Tangle3DUtils.TETRAHEDRON_CORNER_C, TETRAHEDRON_CORNER_B);
	public static final Vector3f TETRAHEDRON_EDGE_AB = subtract(Tangle3DUtils.TETRAHEDRON_CORNER_B, TETRAHEDRON_CORNER_A);
	public static final Vector3f TETRAHEDRON_EDGE_DC = subtract(Tangle3DUtils.TETRAHEDRON_CORNER_C, TETRAHEDRON_CORNER_D);

	/**
	 * Subtracts b from a.
	 */
	public static Vector3f subtract(Tuple3f a, Tuple3f b) {
		return new Vector3f(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	public static Vector3f cross(Vector3f r, Vector3f s) {
		final Vector3f crossVector = new Vector3f();

		crossVector.cross(r, s);

		return crossVector;
	}

	public static Vector3f aplusbtimesx(Tuple3f a, float x, Tuple3f b) {
		return new Vector3f(a.x + x * b.x, a.y + x * b.y, a.z + x * b.z);
	}

	public static void normalize(List<Point3f> vertices, Point2f size) {
		for (final Point3f vertex : vertices) {
			vertex.x = vertex.x / size.x;
			vertex.y = vertex.y / size.y;
		}
	}

	public static TransformGroup createLineNode(Point3f fromVertex, Point3f toVertex, Color3f color, float thickness) {
		final Appearance cylinderAppearance = Tangle3DUtils.createColorAppearance(color);

		final Vector3f distanceVector = new Vector3f(toVertex);
		distanceVector.sub(fromVertex);
		final float distance = distanceVector.length();

		final Cylinder cylinder = new Cylinder(thickness, distance, cylinderAppearance);

		final TransformGroup cylinderTransformationGroup = new TransformGroup();
		cylinderTransformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		cylinderTransformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

		final Transform3D transform = new Transform3D();

		/* 1. Translate center of cylinder to midpoint between from and to: */
		final Vector3f midPoint = new Vector3f((fromVertex.x + toVertex.x) / 2, (fromVertex.y + toVertex.y) / 2, (fromVertex.z + toVertex.z) / 2);
		transform.setTranslation(midPoint);

		/* 2. Rotate cylinder to the orientation between fromVertex and toVertex: */
		final Vector3f rotationAxis = new Vector3f();
		rotationAxis.cross(AXIS_Y, distanceVector);

		/* theta = acos((A dot B) / (|A| * |B|)) /*/
		final float angle = (float) Math.acos(AXIS_Y.dot(distanceVector) / (AXIS_Y.length() * distanceVector.length()));
		transform.setRotation(new AxisAngle4f(rotationAxis.x, rotationAxis.y, rotationAxis.z, angle));

		cylinderTransformationGroup.setTransform(transform);

		cylinderTransformationGroup.addChild(cylinder);
		return cylinderTransformationGroup;
	}
	
	public static TransformGroup createArrowNode(Point3f fromVertex, Point3f toVertex, Color3f color, float thickness) {
		final Appearance cylinderAppearance = Tangle3DUtils.createColorAppearance(color);

		final Vector3f distanceVector = new Vector3f(toVertex);
		distanceVector.sub(fromVertex);
		final float distance = distanceVector.length();

		final Cylinder cylinder = new Cylinder(thickness, distance, cylinderAppearance);
		final Cone cone = new Cone(0.025f, 0.05f, cylinderAppearance);
		
		
		final TransformGroup arrowTransformationGroup = new TransformGroup();
		arrowTransformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		arrowTransformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		
		final TransformGroup coneTransformationGroup = new TransformGroup();
		coneTransformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		coneTransformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		
		final TransformGroup cylinderTransformationGroup = new TransformGroup();
		cylinderTransformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		cylinderTransformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

		
		final Transform3D cylinderTransform = new Transform3D();
		final Transform3D coneTransform = new Transform3D();

		
		
		/* 1. Translate center of cylinder to midpoint between from and to: */
		final Vector3f midPoint = new Vector3f((fromVertex.x + toVertex.x) / 2, (fromVertex.y + toVertex.y) / 2, (fromVertex.z + toVertex.z) / 2);
		cylinderTransform.setTranslation(midPoint);

		/* 2. Rotate cylinder to the orientation between fromVertex and toVertex: */
		final Vector3f rotationAxis = new Vector3f();
		rotationAxis.cross(AXIS_Y, distanceVector);

		/* theta = acos((A dot B) / (|A| * |B|)) /*/
		final float angle = (float) Math.acos(AXIS_Y.dot(distanceVector) / (AXIS_Y.length() * distanceVector.length()));
		cylinderTransform.setRotation(new AxisAngle4f(rotationAxis.x, rotationAxis.y, rotationAxis.z, angle));

		coneTransform.setTranslation(new Vector3f(toVertex));
		coneTransform.setRotation(new AxisAngle4f(rotationAxis.x, rotationAxis.y, rotationAxis.z, angle));

		
		
		
		cylinderTransformationGroup.setTransform(cylinderTransform);
		coneTransformationGroup.setTransform(coneTransform);

		
		
		
		cylinderTransformationGroup.addChild(cylinder);
		coneTransformationGroup.addChild(cone);
		
		
		arrowTransformationGroup.addChild(cylinderTransformationGroup);
		arrowTransformationGroup.addChild(coneTransformationGroup);
		
		return arrowTransformationGroup;
	}

	public static TransformGroup createVertexNode(final Point3f vertex, Color3f color, float radius) {
		final Appearance sphereAppearance = Tangle3DUtils.createColorAppearance(color);
		final Sphere sphere = new Sphere(radius, sphereAppearance);

		final TransformGroup shpereTransformationGroup = createTranslationTransformationGroup(new Vector3f(vertex));
		shpereTransformationGroup.addChild(sphere);
		return shpereTransformationGroup;
	}

	public static AmbientLight createAmbientLight(float r, float g, float b, float x, float y, float z, float influenceRadius) {
		final Color3f lightColor = new Color3f(r, g, b);
		final BoundingSphere bounds = new BoundingSphere(new Point3d(x, y, z), influenceRadius);
		final AmbientLight light = new AmbientLight(lightColor);
		light.setInfluencingBounds(bounds);
		return light;
	}

	public static DirectionalLight createDirectionalLight(float r, float g, float b, float x, float y, float z, float dx, float dy, float dz, float influenceRadius) {
		final Color3f lightColor = new Color3f(r, g, b);
		final BoundingSphere bounds = new BoundingSphere(new Point3d(x, y, z), influenceRadius);
		final Vector3f lightDirection = new Vector3f(dx, dy, dz);
		final DirectionalLight light = new DirectionalLight(lightColor, lightDirection);
		light.setInfluencingBounds(bounds);
		return light;
	}

	public static Appearance createColorAppearance(final Color3f color) {
		final Appearance appearance = new Appearance();
		appearance.setColoringAttributes(new ColoringAttributes(color, ColoringAttributes.NICEST));

		final Color3f ambient = color;
		final Color3f emissive = color; // new Color3f(0, 0 ,0);
		final Color3f diffuse = color;
		final Color3f specular = new Color3f(1, 1, 1);
		final Material material = new Material(ambient, emissive, diffuse, specular, 128);
		appearance.setMaterial(material);
		return appearance;
	}

	public static TransformGroup createTranslationTransformationGroup(final Vector3f vector) {
		final TransformGroup transformationGroup = new TransformGroup();
		transformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		transformationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		final Transform3D transform = new Transform3D();
		transform.setTranslation(vector);
		transformationGroup.setTransform(transform);
		return transformationGroup;
	}
}
