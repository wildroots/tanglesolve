package objects;

import java.util.regex.Pattern;

/**
 * Rational Number class.
 * @author Yuki Saka
 * Last Updated : April 13, 2013
 * @edited by Crista Moreno
 */
public class Rational implements Element {
	/**
	 * Regular expression to check if a string is a valid rational.
	 */
	private static final Pattern RATIONAL_PATTERN = Pattern.compile("(inf)|([-]?[\\d]+)|([-]?[\\d]+/[-]?[\\d]+)");

	// constants
	private static final String BAD_FORMAT = "BAD FORMAT";
	
	// private fields
	private long numerator;
	private long denominator;

	// constructors
	// Default: constructs a rational equal to 0.
	public Rational() {
		this.numerator = 0;
		this.denominator = 1;
	}

	// Constructing a Rational number from a string (example: Rational("5/6"))
	// Rational number is corrupted if given an invalid string.
	public Rational(final String rational) throws IllegalArgumentException {

		if (!Rational.isRational(rational)) {
			throw new IllegalArgumentException(Rational.BAD_FORMAT);
		}
		if (rational.equals("inf")) {
			this.numerator = 1;
			this.denominator = 0;
			return;
		}

		int indexOfSlash = rational.indexOf('/');

		if (indexOfSlash != -1) {
			String numeratorString = rational.substring(0, indexOfSlash);
			this.numerator = Long.parseLong(numeratorString);

			String denominatorString = rational.substring(indexOfSlash + 1);
			this.denominator = Long.parseLong(denominatorString);

			this.reduce();
		} else {
			this.numerator = (new Long(rational)).longValue();
			this.denominator = 1;
		}
	}

	// copy constructor
	public Rational(final Rational rational) {
		this.numerator = rational.getNumerator();
		this.denominator = rational.getDenominator();
	}

	// Integer constructor
	public Rational(final long integer) {
		this.numerator = integer;
		this.denominator = 1;
	}

	// Rational constructor given the numerator and the denominator.
	public Rational(final long numerator, final long denominator) {
		this.numerator = numerator;
		this.denominator = denominator;

		this.reduce();
	}

	// ---------------------ACCESSORS------------------------------
	/**
	 * 
	 * @return signature
	 */
	public long getSignature() {
		if (this.isInfinity()) {
			return 0;
		}
		return this.numerator / Math.abs(this.numerator);
	}

	/**
	 * 
	 * @return numerator
	 */
	public long getNumerator() {
		return this.numerator;
	}

	/**
	 * 
	 * @return denominator 
	 */
	public long getDenominator() {
		return this.denominator;
	}

	/** 
	 * 
	 * @return reciprocal of the rational number.
	 */
	public Rational getReciprocal() {
		return new Rational(this.denominator, this.numerator);
	}

	/**
	 * 
	 * @return the negative of the rational.
	 */
	public Rational getNegative() {
		if (!this.isInfinity()) {
			return new Rational(-this.numerator, this.denominator);
		} else {
			return new Rational(this.numerator, this.denominator);
		}
	}
	
	/**
	 * 
	 * @return true if the rational is an integer.
	 */
	public boolean isInteger() {
		return (this.denominator == 1);
	}

	/**
	 * 
	 * @return true if the rational is equal to 1.
	 */
	public boolean isOne() {
		return (this.numerator == 1) && (this.denominator == 1);
	}

	/**
	 * 
	 * @return true if rational is equal to 0.
	 */
	public boolean isZero() {
		return (this.numerator == 0) && (this.denominator == 1);
	}

	/** 
	 * 
	 * @return true if rational is equal to -1.
	 */
	public boolean isNegativeOne() {
		return (this.numerator == -1) && (this.denominator == 1);
	}

	/** 
	 * 
	 * @return true if rational is equal to infinity.
	 */
	public boolean isInfinity() {
		return (this.denominator == 0);
	}

	// ----------------------EQUIVALENCE------------------------//
	@Override
	public boolean equals(final Object object) {
		if (object instanceof Rational) {
			final Rational rational = (Rational) object;
			return (this.numerator == rational.getNumerator()) && (this.denominator == rational.getDenominator());
		} else { // not a Rational object
			return false;
		}
	}

	// ------------------------ARITHMETIC----------------------------//
	/**
	 * Reciprocates the rational.
	 */
	public void reciprocate() {
		final long temp = this.denominator;
		this.denominator = this.numerator;
		this.numerator = temp;
	}

	public Rational invMod() {
		if (this.numerator == 0) {
			return new Rational(0);
		} else if (this.denominator == 0) {
			return new Rational(1, 0);
		} else {
			final long l = MathFunctions.moduloInverse1(this.numerator, this.denominator);
			return new Rational(l, this.denominator);
		}
	}

	/**
	 * Negates the rational. 
	 */
	public void negateNumerator() {
		this.numerator = -this.numerator;
	}

	public Rational add(final Rational rational) {
		if ((this.denominator == 0) || rational.isInfinity()) {
			return new Rational(1, 0);
		}
		return new Rational((rational.numerator * this.denominator) + (this.numerator * rational.denominator),
				rational.denominator * this.denominator);
	}

	public Rational add(final long i) {
		return this.add(new Rational(i, 1));
	}

	public Rational subtract(final Rational r) throws IllegalArgumentException {
		return this.add(r.getNegative());
	}

	public Rational subtract(final long i) throws IllegalArgumentException {
		return this.subtract(new Rational(i, 1));
	}

	public Rational multiply(final Rational r) {
		return new Rational(this.numerator * r.numerator, r.denominator * this.denominator);
	}

	public Rational multiply(final long i) {
		return this.multiply(new Rational(i, 1));
	}

	public Rational divide(final Rational r) throws IllegalArgumentException {
		return this.multiply(r.getReciprocal());
	}

	// -------------------------Utilities------------------------------
	// some utility functions
	private void reduce() {
		if ((this.numerator == 0) && (this.denominator != 0)) {
			this.numerator = 0;
			this.denominator = 1;
		} else if (this.denominator == 0) {
			this.numerator = 1;
			this.denominator = 0;
		} else {
			final long absoluteNumerator = Math.abs(this.numerator);
			final long absoluteDenominator = Math.abs(this.denominator);
			final long sign = (this.numerator * this.denominator) / Math.abs(this.numerator * this.denominator);

			final long gcd = Rational.getGreatestCommonDivisor(absoluteNumerator, absoluteDenominator);

			this.numerator = sign * (absoluteNumerator / gcd);
			this.denominator = (absoluteDenominator / gcd);
		}
	}
	
	/**
	 * Computes the greatest common divisor.
	 * @param a
	 * @param b
	 * @return 
	 */
	public static long getGreatestCommonDivisor(final long a, final long b) {
		if (b == 0) {
			return a;
		} else {
			return Rational.getGreatestCommonDivisor(b, a % b);
		}
	}
	
	/**
	 * 
	 * @param list
	 */
	public Rational(final VectorList<Rational> list) {
		final Rational r = Rational.toRational(list);
		this.numerator = r.getNumerator();
		this.denominator = r.getDenominator();
	}

	public static Rational toRational(final VectorList<Rational> list) {
		if (VectorList.isEmpty(list.tail())) {
			return (Rational) list.head();
		} else {
			final Rational r = Rational.toRational(list.tail()).getReciprocal();
			return ((Rational) list.head()).add(r);
		}
	}

	@Override
	public Rational clone() {
		return new Rational(this.numerator, this.denominator);
	}
	
	public VectorList<Rational> continuedFraction() {
		return this.continuedFraction(this.numerator, this.denominator);
	}

	private VectorList<Rational> continuedFraction(final long a, final long b) {
		if (b == 0) {
			return new VectorList<Rational>(new Rational(0), new VectorList<Rational>(new Rational(0), null));
		}
		if ((b == 1) || (b == -1)) {
			return new VectorList<Rational>(new Rational(a * b), null);
		} else {
			return new VectorList<Rational>(new Rational(a / b), this.continuedFraction(b, a % b));
		}
	}

	/**
	 * determines if the given string is a rational number
	 * @param s
	 * @return
	 */
	public static boolean isRational(String s) {
		return RATIONAL_PATTERN.matcher(s).matches();
	}

	public String getConwayString() {
		return "b(" + this.denominator + "," + this.numerator + ")";
	}

	public String getPair() {
		return "(" + this.numerator + "," + this.denominator + ")";
	}

	public String inversePair() {
		if (this.numerator == 0) {
			return "(0,1)";
		} else if (this.denominator == 0) {
			return "(1,0)";
		} else {
			final long l = MathFunctions.moduloInverse1(this.numerator, this.denominator);
			return "(" + l + "," + this.denominator + ")";
		}
	}

	// Returns the string representation of this rational number
	@Override
	public String toString() {
		String s = "";
		if (this.denominator == 1) {
			s = (new Long(this.numerator)).toString();
		} else if ((this.numerator == 0) && (this.denominator != 0)) {
			s = s + "0";
		} else if (this.denominator == 0) {
			s = s + "inf";
		} else {
			s = s + (new Long(this.numerator).toString()) + "/" + (new Long(this.denominator)).toString();
		}
		return s;
	}
}