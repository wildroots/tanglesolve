package util;

import java.awt.Color;

public class TangleColor1ColorSource extends SimpleColorSource {
	@Override
	public Color getColor() {
		return Preferences.getInstance().getTangleColor1();
	}
}