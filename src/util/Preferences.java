package util;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;


public class Preferences {
	// Singleton
	private static final Preferences INSTANCE = new Preferences();

	public static Preferences getInstance() {
		return INSTANCE;
	}

	// Property Constants
	private static final String PROPERTY_SHOW_BOXES = "SHOW_BOXES";
	private static final boolean PROPERTY_SHOW_BOXES_DEFAULT = false;

	private static final String PROPERTY_FOURPLAT_COLOR_1 = "FOURPLAT_COLOR_1"; // TODO
	private static final Color PROPERTY_FOURPLAT_COLOR_1_DEFAULT = Color.BLACK; // TODO
	
	private static final String PROPERTY_TANGLE_COLOR_1 = "TANGLE_COLOR_1"; // TODO
	private static final Color PROPERTY_TANGLE_COLOR_1_DEFAULT = Color.RED;
	
	private static final String PROPERTY_TANGLE_COLOR_2 = "TANGLE_COLOR_2"; // TODO
	private static final Color PROPERTY_TANGLE_COLOR_2_DEFAULT = Color.BLUE; // TODO



	// Fields
	private Properties properties = new Properties();

	// Private constructor to enfore singleton pattern
	private Preferences() {

	}

	// Save/Load methods
	public void load(final File input) {
		if (input.exists()) {
			try {
				properties.load(new FileInputStream(input));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void save(final File output) {
		try {
			properties.store(new FileOutputStream(output), null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Property methods
	public void setShowBoxes(final boolean show) {
		properties.setProperty(PROPERTY_SHOW_BOXES, Boolean.toString(show));
	}

	public boolean isShowBoxes() {
		final String defaultAsString = Boolean.toString(PROPERTY_SHOW_BOXES_DEFAULT);
		final String property = properties.getProperty(PROPERTY_SHOW_BOXES, defaultAsString);
		return Boolean.parseBoolean(property);
	}

	public void setFourPlatColor1(final Color color) {
		properties.setProperty(PROPERTY_FOURPLAT_COLOR_1, getColorString(color));
	}

	public Color getFourPlatColor1() {
		return getColor(PROPERTY_FOURPLAT_COLOR_1, PROPERTY_FOURPLAT_COLOR_1_DEFAULT);
	}
	
	public void setTangleColor1(final Color color) {
		properties.setProperty(PROPERTY_TANGLE_COLOR_1, getColorString(color));
	}
	
	public Color getTangleColor1() {
		return getColor(PROPERTY_TANGLE_COLOR_1, PROPERTY_TANGLE_COLOR_1_DEFAULT);
	}
	
	public void setTangleColor2(final Color color) {
		properties.setProperty(PROPERTY_TANGLE_COLOR_2, getColorString(color));
	}
	
	public Color getTangleColor2() {
		return getColor(PROPERTY_TANGLE_COLOR_2, PROPERTY_TANGLE_COLOR_2_DEFAULT);
	}

	private Color getColor(String property, Color defaultColor) {
		final String defaultAsString = getColorString(defaultColor);
		final String value = this.properties.getProperty(property, defaultAsString);
		return Color.decode(value);
	}

	private static String getColorString(Color color) {
		final String argb = Integer.toHexString(color.getRGB());
		final String rgb = argb.substring(2, argb.length()); // cuts away the alpha
		return "#" + rgb.toUpperCase();
	}
}
