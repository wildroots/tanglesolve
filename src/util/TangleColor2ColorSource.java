package util;

import java.awt.Color;

public class TangleColor2ColorSource extends SimpleColorSource {
	@Override
	public Color getColor() {
		return Preferences.getInstance().getTangleColor2();
	}
}
