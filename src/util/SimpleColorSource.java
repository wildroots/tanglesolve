package util;

import graphics.ColorSource;

import java.awt.Color;

import javax.vecmath.Color3f;

public abstract class SimpleColorSource implements ColorSource {
	@Override
	public Color3f getColor3f() {
		final Color color = this.getColor();
		return new Color3f(color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f);
	}
}