package run;

/**
 * 1.1 version todo:
 * implement processive theorems
 */

/**
 * Version 2.0 has object to click to see tangle graph or to see theorems. but doesn't have color highlights
 *
 */

import graphics.TangleSolvePanel;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JApplet;
import javax.swing.JFrame;

import util.Preferences;

// This displays a framed area containing one of each shape you can draw.
public class TangleSolve extends JApplet {
	private static final long serialVersionUID = -9007490505613122992L;

	// Executed only when this program is run as an applet.
	@Override
	public void init() {
		final JFrame frame = new JFrame("Tangle Solver");
		final TangleSolvePanel gPanel = new TangleSolvePanel(frame, true);
		frame.getContentPane().add(gPanel);
		frame.setSize(700, 600);
		frame.setVisible(true);
	}

	public static void main(final String arg[]) {
		Preferences.getInstance().load(new File("tanglesolve.preferences"));

		final JFrame frame = new JFrame("Tangle Solver");
		final TangleSolvePanel tangleSolvePanel = new TangleSolvePanel(frame, true);
		frame.getContentPane().add(tangleSolvePanel);

		final WindowAdapter windowAdapter = new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent event) {
				Preferences.getInstance().save(new File("tanglesolve.preferences"));

				System.exit(0);
			}
		};

		frame.addWindowListener(windowAdapter);
		frame.setVisible(true);
		frame.pack();
	}
}