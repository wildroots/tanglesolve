# TangleSovle
> Copyright (C) (2000 -2013) Yuki Saka, Wenjing Zheng, Crista Moreno
> This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Instructions on how to run TangleSolve (on __Mac OS X__) from the ``TangleSolve.zip`` distribution:

1. Unzip the ``TangleSolve.zip`` to the desired location.
2. The new folder will contain a file ``TangleSolve.jar`` and a folder ``TangleSolve_lib``.
3. Double click the ``TangleSolve.jar`` to launch the TangleSolve user interface.



## Troubleshooting:
> The ``Show In 3D…`` functionality doesn't seem to work.

The problem might be that the java version on the Mac is too new (Java ___1.7___ and higher are currently __not__ supported). To fix the problem, the java version has to be explicitly selected through the ``Terminal`` application:

1. Open the ``Terminal`` application:
2. Type:
       ``/System/Library/Frameworks/JavaVM.framework/Versions/``
3. Press the ``Tab`` key a few times for Autocompletion:
  1. See something like: ``1.4/  1.4.2/  1.5/    1.5.0/   1.6/     1.6.0/   1.7/  …``
  2. Otherwise: ``='(``
4. Choose the highest version from __3.i.__ that is numerically below(!) ``1.7``. The path will now look like:
      ``/System/Library/Frameworks/JavaVM.framework/Versions/1.6.0/``
5. Complete the path to:
     ``/System/Library/Frameworks/JavaVM.framework/Versions/1.6.0/Commands/java``
6. Append `` -jar `` to the path. It now looks something like:
     ``/System/Library/Frameworks/JavaVM.framework/Versions/1.6.0/Commands/java -jar ``
7. Append the path to the ``TangleSolve.jar``. It now looks something like:
    ``/System/Library/Frameworks/JavaVM.framework/Versions/1.6.0/Commands/java -jar /Users/crista/Desktop/TangleSolve.jar``
8. Press ``Enter`` and TangleSolve should open and the ``Show In 3D…`` option should function properly.

